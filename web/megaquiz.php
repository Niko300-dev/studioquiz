<?php 
//session cross to sub domain
//ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}
else
{
  //GO LOGIN
  header('Location: login.php');
  exit();
}	

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}


$client_unique_id = uniqid();


$avatar_credential = rand(100,300);

?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
	<base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz - Jouer une partie</title>
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	
	<link rel="stylesheet" href="css/shaker.css">

    <link rel="stylesheet" href="css/style-modern-sq.css">

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/lobibox.css"/>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style>
	
		.annonceClassique 
		{
		  transition: all 1s ease-in-out;
		  border : 2px solid orange;	
		  background-color: #55F;
		  color: white;
		}
	
		.annonceReponse
		{
	      background-color: #F95;
		  border : 2px solid darkblue;		
		  transition: all 1s ease-in-out;
		  color: white;
		  
		}
		
	</style>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.0.5/socket.io.js"></script>

<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://directquiz.niko.ovh/dev/js/jquery.tightgrid.js"></script>
	
	<script src="https://kit.fontawesome.com/e8e9204849.js" crossorigin="anonymous"></script>
	
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<script src="js/lobibox.js"></script>

		
	<script>

		var isStart = false;
		
		// ========== TYPE DE QUIZ ============
		// ===            MEGAQUIZ          ===
		// ====================================
		
		var NAME_QUIZ = 'MEGAQUIZ';
		var ID_QUIZ = 1;
		
		// ====================================
		var isMuted = false;
		var sexeVoix = "French Canadian Male";
		var lecteur = null;
		var applaudissementsRestants = 3;
		var interact = false;
		
		var audioBipButton  = new Audio('https://directquiz.niko.ovh/dev/bip_button.mp3');
		var audioFlap  = new Audio('https://directquiz.niko.ovh/dev/flap.mp3');
		var audioError  = new Audio('https://directquiz.niko.ovh/dev/bip_error.mp3');
			
		var gifFieldOpen = false;
		var gifFieldOpenMobile = false;		
			
		function forcerEjection(data)
		{
			if (data.client_id == "<?= $client_unique_id ?>")
			{
				document.location.href = "index.php";
			}
	
		}
		
		function muteSwitch()
		{
			
			isMuted = !isMuted;
			
			if (isMuted)
			{	
				$(".control-sound, .control-mobile-sound").children("i").removeClass("fa-volume-down").addClass("fa-volume-mute");
				$("#chatbox").append("<div class='message-info'><img src='images/icon-info.png' alt='' class='message-info-img'/> <p class='message-info-label'>Silence activé</p></div>");	
			}			
			else
			{
				$(".control-sound, .control-mobile-sound").children("i").removeClass("fa-volume-mute").addClass("fa-volume-down");
				$("#chatbox").append("<div class='message-info'><img src='images/icon-info.png' alt='' class='message-info-img'/> <p class='message-info-label'>Silence désactivé</p></div>");	
			}

			$( "img" ).load(function() {
				$("#chatbox").scrollTop(99999);	
			});		
		}
		
		function setHomme()
		{
			sexeVoix = "French Canadian Male";
			$("#avatarAnimateur").css("border","3px solid darkblue");
			
		}
		
		function setFemme()
		{
			sexeVoix = "French Canadian Female"
			$("#avatarAnimateur").css("border","3px solid pink");
		}
		
		function redonnerApplaudissements()
		{
			applaudissementsRestants = 3;
		}
		
		function elapsedClap(secondes)
		{

			secondes--;
			if (secondes > 0)
			{	
				$("#clap-elapsed").text((secondes < 10?"0":"") + secondes + "s");
				$("#clap-elapsed-mobile").text((secondes < 10?"0":"") + secondes + "s");
				setTimeout(function(){elapsedClap(secondes);},1000);
			}
			else
			{
				$("#clap-elapsed").text("00s");
				$("#clap-elapsed-mobile").text("00s");
				setTimeout(function(){$("#clap-elapsed, #clap-elapsed-mobile").fadeOut(380);},650);
				return;
			}
		}
		
		var pseudoConnected = "";	
		var IDConnected = "";	
		

		$(function(){
			
			  $.fn.outerHTML = function() {
				return $(this).clone().wrap('<div></div>').parent().html();
			  }

				$(".score-good-answer").hide();
				
				$("#connectFrame").hide();
				$("#sender").show();
				
				$('.easy').click(function(){audioBipButton.play(); lancerMode('easy')});
				
				$('.normal').click(function(){audioBipButton.play(); lancerMode('normal')});
				
				$('.hard').click(function(){audioBipButton.play(); lancerMode('hard')});
				
				$('.new-bot').click(function(){audioBipButton.play(); lancerBot(true)});
				
				$('.del-bot').click(function(){audioBipButton.play(); lancerBot(false)});
				
		$(".control-sound-img, .control-mobile-sound-img").click(function(){
			
			audioBipButton.play();
			muteSwitch();
			
		});	
		
		$(".control-suspect, .control-mobile-suspect").click(function(){
			
			audioBipButton.play();
			suspecterLaQuestionCourante();
			
		});
		
		$(".control-gif-img").click(function(){

			$("#gif-send").val("");
			
			if (!gifFieldOpen)
			{
				audioBipButton.play();
				audioFlap.play();
			
				$("#gif-send").effect( 'slide', {}, 350, function(){$("#gif-send").focus();} );
			}
			else
			{
				audioBipButton.play();
				$("#gif-send").fadeOut(350);
			}
			
			gifFieldOpen = !gifFieldOpen;
			
		});
		
		$(".control-mobile-gif-img").click(function(){
			
			$("#gif-send-mobile").val("");
			
			if (!gifFieldOpenMobile)
			{
				audioBipButton.play();
				audioFlap.play();
			
				$("#gif-send-mobile").effect( 'slide', {}, 350, function(){$("#gif-send-mobile").focus();} );
			}
			else
			{
				audioBipButton.play();
				$("#gif-send-mobile").fadeOut(350);
			}
			
			gifFieldOpenMobile = !gifFieldOpenMobile;
			
		});
		
		function onMediaQueriesChange(x) {
		  if (!x.matches) { // If not mobile

				$("#gif-send-mobile").hide();
				gifFieldOpenMobile = false;
				
		  }
		}

		// Changement du media queries ------
		var x = window.matchMedia("(max-width: 767px)");
		onMediaQueriesChange(x); 
		x.addListener(onMediaQueriesChange); 
		// ----------------------------------
		
		$(window).resize(function(){
			
			$("#gif-send").hide();
			gifFieldOpen = false;	
			
		});

		$("#fond-public").mousemove(function(){
			if (!interact) 
			{
				$( "body" ).off( "mousemove", "#fond-public", function(){interact = true});
			}
		});
		
				
	$(".control-clap-img, .control-mobile-clap-img").click(function(){
		
		if (applaudissementsRestants > 0)
		{
			var dataMember = {
				id: '<?= $_SESSION['userid_dq'] ?>',
				pseudo: '<?= $_SESSION['pseudo_dq'] ?>',
					};			
		
			socket.emit('applaudir',dataMember);
			
			applaudissementsRestants--;
			
			if (applaudissementsRestants == 0)
			{
				$("#clap-elapsed, #clap-elapsed-mobile").show();
				$("#clap-elapsed, #clap-elapsed-mobile").click(function(){
					audioError.play();
				});

				setTimeout(function(){redonnerApplaudissements();},20000);
				setTimeout(function(){elapsedClap(20);},1000);
			}
		}
		else
		{

			audioError.play();

		}
		
	});
	
	
				 lecteur = document.getElementById("animation-presentateur");
				
				
				
				
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
				
				
				$(".um-members-pagi-dropdown").change(function(){
					
					setRecordsMembres();
					
				});
				

				var uuidGlobal = "<?= (isset($_SESSION['userid_dq'])?$_SESSION['userid_dq']:'0'); ?>";			
				var pseudoGlobal = "<?= (isset($_SESSION['pseudo_dq'])?$_SESSION['pseudo_dq']:'0'); ?>";
				
			
			
			
			$("#button-start").click(function(){
				
					if (uuidGlobal != "0" && !isStart)
					{
						$("#button-start").off('click');
						isStart = true;
						$("#button-start").fadeOut(200);
						f5d6e85g6f12d56f2e41e(pseudoGlobal, uuidGlobal, ID_QUIZ, '<?= $client_unique_id ?>');
						$("#salon_name").text("QUIZ "+NAME_QUIZ);
					}
				
			});
		
		});
		
		function searchGIF(terme)
		{
				$("#gif-send").val("");
				$("#resultat").show();
	
				var api = "https://api.giphy.com/v1/gifs/search?";
				var apiKey = "&api_key=4O10kcOpby6A1I8CB6aOtXrWedK70uty";
				var query = "&q="+terme;
				var limit = "&limit=24";

				var url = api + apiKey + query + limit;

				$.get(url).done(function(data){

					//result = JSON.parse(data);
					
					var result = "";
					
					for (i = 0; i < data.data.length ;i++)
					{
							result += "<div class=\'item\'><img class=\'item-gif\' src=\'"+data.data[i].images.fixed_height_small.url+"\' /></div>";
					}
					$("#resultat").html("");
					$("#resultat").html(result);
				
					
					$(".item-gif").click(function(){
						
					audioBipButton.play();

					var data = {
						id: "<?= (isset($_SESSION['userid_dq'])?$_SESSION['userid_dq']:'0'); ?>",
						pseudo: pseudoConnected,
						message: "GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]|" + $(this).attr("src"),
						ia: false
							};
							
						write(data);
						
						$("#resultat").hide();
						
					});
					
					$("#resultat").click(function(){
						$("#resultat").html("");
						$("#resultat").hide();
						
					});
					
				});

		}
		
		function refreshAvatars()
		{
			
			$(".player").each(function(){
					
					$(this).children(".player-avatar").eq(0).attr("src", "images/avatar/"+$(this).attr("uuid")+".jpg?<?= $avatar_credential ?>");
							
					$.post("php/getLevelByUUID.php",{uuid:$(this).attr("uuid")}).done(function(data){

						var result = data.split("#");
						//Level # UUID
						if (result[1]!="")
						$(".avatar-level[uuid="+result[1]+"]").attr("src", "images/avatar/level_"+result[0].trim()+".png");
						
					}).
					fail(function(){
						
						
						
					});


			});

		}

		function gifEnter(e) {

		
			if (e.keyCode == 13 && $("#gif-send").val().trim().replace(/ /g,"") != "") {

				searchGIF($("#gif-send").val().trim());
				$("#gif-send").fadeOut(400);
				gifFieldOpen = !gifFieldOpen;
				return false;	
				
			}
			else if (e.keyCode == 13)
			{
				$("#gif-send").fadeOut(400);
				gifFieldOpen = !gifFieldOpen;
				return false;	
			}

		}
		
		function gifEnterMobile(e) {

			if (e.keyCode == 13 && $("#gif-send-mobile").val().trim().replace(/ /g,"") != "") {

				searchGIF($("#gif-send-mobile").val().trim());
				$("#gif-send-mobile").fadeOut(400);
				gifFieldOpenMobile = !gifFieldOpenMobile;
				return false;	
				
			}
			else if (e.keyCode == 13)
			{
				$("#gif-send-mobile").fadeOut(400);
				gifFieldOpenMobile = !gifFieldOpenMobile;
				return false;	
			}

		}
		
		function lancerMode(mode)
		{
					

					var data = {
						id: "<?= (isset($_SESSION['userid_dq'])?$_SESSION['userid_dq']:'0'); ?>",
						pseudo: pseudoConnected,
						message: '/start '+mode,
						ia: false
							};
					write(data);
	
		}
		
		function lancerBot(ajout)
		{
				
					var data = {
						id: "<?= (isset($_SESSION['userid_dq'])?$_SESSION['userid_dq']:'0'); ?>",
						pseudo: pseudoConnected,
						message: (ajout?'/addbot':'/clearbot'),
						ia: false
							};
					write(data);
	
		}
		
		function f5d6e85g6f12d56f2e41e(pseudo, iduser, sa, clientID)
		{
					
					if (pseudo.trim() != "")
					{						
						console.log(pseudo.trim()+","+ iduser+","+ sa + "," + clientID);						
						connectToServer(pseudo.trim(), iduser, sa, clientID);
						pseudoConnected = pseudo.trim();
						IDConnected = iduser;
					}
					else
					{
						alert("Veuillez vous connecter !")
					}
					
		}
		
		function goHome()
		{
			window.location = 'index.php';
		}
		
	</script>
	


<script src="https://directquiz.niko.ovh/dev/public/quiz-modern.js"></script>
	
	

</head>

<body id="about_us" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
           
						<li><a class="nav-link active" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
<li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					 <li><a class="nav-link" href="discordEndPoint.php"><image src="images/discord_chat.png"></image> Lier</a></li>
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
					
                    </ul>
                </div>
                <div class="search-box">

                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->
	

    <!-- section -->
    <div id="fond-public" class="section layout_padding theme_bg">
        <div class="container">
        
<div id="my-toast-location" style="position: absolute; top: 10px; left: 10px;"></div>
<div id="sender">

<div style="visibility:hidden;" id="audioSpeech"></div>

<div id="button-start">

	CLIQUEZ-ICI POUR REJOINDRE

</div>


<div class="game game-title">
	<h1 id="salon_name"></h1><div id="fermer-quiz"><img onClick="goHome()" style="cursor:pointer;" src="images/fermerJeu.png" alt="Fermer" /></div>
</div>

<div id="resultat" class="pictures">
</div>

<div class="game">
    <div class="left">
        <div class="game-presenter">

			<video id="animation-presentateur" class="game-presenter-video" muted preload="auto" loop width="288" height="216">
					<source src="https://directquiz.niko.ovh/dev/img/animateur.webm" type="video/webm">
					<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4" type="video/mp4">
			</video>
			
            <div class="control">
                <div class="control-sound">
                    <i class="fas fa-volume-down control-sound-img"></i>
                </div>
                <div class="control-suspect" title="Signaler la question">
                    <i class="fas fa-exclamation-circle control-suspect-img"></i>
                </div>				
                <div class="control-clap">
                    <i class="fas fa-sign-language control-clap-img"></i><span id="clap-elapsed">0s</span>
                </div>
				<div class="control-gif">
                    <i class="fas control-gif-img">GIF</i><input id="gif-send" style="width: 135px; height: 50px; margin-top: -3px; padding-left: 24px;" onkeypress="return gifEnter(event)" placeholder="Rechercher" type="text" />
                </div>
            </div>
        </div>
        <div id="score">


        </div>
    </div>
    <div class="right">
        <div id="questionbox" class="messageClassique">
                <p id="questionbox-content">
				</p>				
        </div>
		
        <div id="chatbox">
		
        </div>
        <input type="text" id="message-send" name="sendMessage" onkeypress="return messageEnter(event, '<?= (isset($_SESSION['userid_dq'])?$_SESSION['userid_dq']:'0'); ?>')" autocomplete="off"
               placeholder="Ecrivez votre message ici">
			   <div id="containerChrono" style="top: -52px; position: relative; float:right; width:52px; height:52px; padding:5px;">
			   
					<div class="progress-circle" data-value="30">
						 <div class="progress-masque">
							<div class="progress-barre"></div>
							<div class="progress-sup50"></div>
						 </div>
					</div>
			   
			   </div>
		</input>
		<div id="toPresentator">
			<input type="checkbox" value="IA" id="toIA" /> S'adresser au présentateur
		</div>
		
		<br/>
		
		<span id="writingCurrentContainer"></span>
		
		<br/>
		
		    <div class="control-mobile">
                <span class="mobile-control control-mobile-sound">
                    <i class="fas fa-volume-down control-mobile-sound-img"></i>
                </span>
				
				<span class="mobile-control control-mobile-suspect" title="Signaler la question">
                    <i class="fas fa-exclamation-circle control-mobile-suspect-img"></i>
                </span>	
				
                <span class="mobile-control control-mobile-clap">
                    <i class="fas fa-sign-language control-mobile-clap-img"></i><span id="clap-elapsed-mobile">0s</span>
                </span>
				<br/>
				<span class="mobile-control control-mobile-gif">
                   <i class="fas control-mobile-gif-img">GIF</i> <input id="gif-send-mobile" style="width: 135px; height: 50px; margin-top: -3px; padding-left: 24px;" onkeypress="return gifEnterMobile(event)" placeholder="Rechercher" type="text" />
                </span>
			</div>
			
			

           
		<table><tr><td>
			<div class="new-game">
				<p class="new-game-title">Partie ?</p>
				<input type="button" class="new-game-easy easy" value="FACILE" />
				<input type="button" class="new-game-normal normal" value="NORMALE" />
				<input type="button" class="new-game-hard hard" value="DIFFICILE" />
			</div>
		</td>
		<td>
			<div class="new-game">
				<p class="new-game-title">Bots ?</p>
				<input type="button" class="new-game-easy new-bot" value="➕🤖" />
				<input type="button" class="new-game-hard del-bot" value="❌🤖" />
			</div>
		</td>
		</tr></table>
    </div>
</div>








</div> <!-- sender -->
<div id="connectFrame">

<?php

if (!isset($_SESSION['userid_dq']))
{

	echo '<div id="informationConnexion"><div class="message-logoff">Vous devez être connecté pour pouvoir rejoindre une partie...</div>
	<a class="lien-interne" href="login.php">Se connecter</a>
	</div><br/><script>$(function(){
		
		$("#fond-public").attr("id","noid");
		
	});</script>';
}

?>


	<br/>

</div>
<br/><br/>


               
      
        </div>
    </div>
    <!-- end section -->

	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>