<?php
session_start();

require_once('credentials.php');

use \Eternaltwin\Client\Auth;
use \Eternaltwin\Client\HttpEtwinClient;
use \Eternaltwin\User\UserId;

	$code = $_GET["code"];
	$state = $_GET["state"];
	$accessToken = $oauthClient->getAccessTokenSync($code);

$client = new HttpEtwinClient("https://eternal-twin.net");
$auth = Auth::fromToken($accessToken->getAccessToken());
$self = $client->getSelf($auth);

$user = $self->getUser();
$displayName = $user->getDisplayName()->getCurrent()->getValue()->toString();
$userId = $user->getId()->getInner()->toString();

$_SESSION['userid_dq'] = $userId;
$_SESSION['pseudo_dq'] = $displayName;

setcookie("userid_dq",$_SESSION['userid_dq'],time()+100*24*3600);	
setcookie("pseudo_dq",$_SESSION['pseudo_dq'],time()+100*24*3600);	



// Enregistrement de l'utilisateur à jour
include('config.php');

try
{

	$uuid = $_SESSION['userid_dq'];
	$pseudo = $_SESSION['pseudo_dq'];

	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	if (!$mysqli->query("Call DQ_InsertStatsUsers('".$uuid."','".$pseudo."');")) {
		echo "Echec de la requête : " . $mysqli->error;
	}
	else
	{
		$file = 'images/avatar/0.jpg';
		$newfile = 'images/avatar/'.$uuid.'.jpg';

		if (!file_exists($newfile)) copy($file, $newfile);		
	}
	
	$mysqli = null;

}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

// ----- FIN ENREGISTREMENT ------

header('Location: jouer.php'); 

?>