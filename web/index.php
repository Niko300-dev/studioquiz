<?php 
//session cross to sub domain
//ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
	<base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz</title>
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	
	<link rel="stylesheet" href="css/lobibox.css"/>
    

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src="js/jquery.min.js"></script>
	<script src="js/lobibox.js"></script>
	
	<script>
	
				if (typeof navigator.serviceWorker !== 'undefined') {
				navigator.serviceWorker.register('sw.js')
			  }
			  
			function success(message)
			{
				  Lobibox.alert('success', {
						msg: "<div style='color: darkgreen;'>" + message + "</div>"
				  });
			}	
			
			function error(message)
			{
				Lobibox.alert('error', {
						msg: "<div style='color: red;'>" + message + "</div>"
				});
			}
	
		$(function(){
				
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
						
					}).
					fail(function(){
						
						
						
					});
				
				
			if (get('type') == 'kick')
			{
				alert("Vous avez été kické de la partie !");
			}
			
		});
		
function get(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
	</script>
	
</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
  <!--
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
  -->
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link active" href="index.php">Accueil</a></li>                  
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						
                        <li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
						<li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					    <li><a class="nav-link" href="discordEndPoint.php"><image src="images/discord_chat.png"></image> Lier</a></li>
					   
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
					  				   
						<?php } ?>
					
                    </ul>
                </div>
                <div class="search-box">

                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->

	    <!-- section -->
    <div class="section layout_padding">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="full center margin-bottom_30">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">MEILLEURS</span> JOUEURS</h2>
                            <p class="large">Panthéon</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div id="demo" class="carousel slide" data-ride="carousel">

                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
								
														<?php
						
						
							include('config.php');
							
							
							$COL_PSEUDO = 1;
							$COL_POINT_TOTAUX = 2;
							$COL_BEST_SCORE = 3;
							$COL_GAME_DONE = 4;
							
							$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
							$mysqli->set_charset("utf8mb4");
							
							if ($mysqli->connect_errno) {
								echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
							}

								$query = "CALL DQ_GetCustomStatsUsers('all',".$COL_POINT_TOTAUX.", 'desc')";
								$result = $mysqli->query($query);
								
								$cpt = 0;
								
								
								
								
		
								while (($row = $result->fetch_array(MYSQLI_ASSOC)) && $cpt < 8)
								{
												
									$cpt++;
									
									
									if ($cpt == 5)
									echo '<div class="carousel-item">
                                <div class="row">';
									
									echo '<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="cadre-pantheon">
											<div class="position-pantheon place-pantheon-'.$cpt.'">'.$cpt.'<sup>'.($cpt==1?'er':'ème').'</sup></div>
												<img class="img-responsive" src="images/avatar/'.$row['UUID'].'.jpg?'.time().'" alt="#" /><div class="pseudo-pantheon">'.$row['Pseudo'].'</div>
											</div>
										  </div>';
									
								
									if ($cpt == 4)
									echo '</div>
											</div>';
							
								}
								
								
								/* Libération des résultats */
								$result->free();

								/* Fermeture de la connexion */
								$mysqli->close();
							
							
							$mysqli = null;
						
							if ($cpt != 4)
									echo '	</div>
											</div>';
						
						?>
								
								
						
             
                        </div>

                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>

                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="full center">
                        <a href="classement.php" class="hvr-radial-out button-theme" style="width:300px;">Classement complet ></a>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- end section -->

	    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">DIRECT QUIZ</span> LE QUIZ EN DIRECT</h2>
                            <p class="large">Rejoignez l'aventure !</p>
											
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 text_align_center">
                    <div class="full">
                        <img class="img-responsive" src="images/resume_img.png" alt="#" />
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 white_fonts">
                    <h3 class="small_heading">DES QUESTIONS, DES REPONSES...</h3>
                    <p>PARTICIPEZ A UN QUIZ AVEC DES CANDIDATS COMME SI VOUS ETIEZ DANS UNE EMISSION EN DIRECT</p>
                    <p>Rejoignez une partie et installez-vous avec les autres candidats, lorsque vous êtes au moins 2, un des candidats peut lancer l'émission en choisissant un mode de difficulté, la partie va alors commencer et vous devrez chacun répondre au mieux à 10 questions à difficulté croissante. Que le meilleur gagne !</p>
                    <a href="jouer.php" class="hvr-radial-out button-theme">Jouer ></a>
			   </div>
            </div>
        </div>
    </div>
	
	
						<?php	
						
						
							if (isset($_POST['IDDiscord']))
							{
								try
								{
									include('config.php');
							
									$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
									$mysqli->set_charset("utf8mb4");
							
									if ($mysqli->connect_errno) {
										echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
									}

									$query = "CALL DQ_updateDiscordIdentityByUUID('".$_SESSION["userid_dq"]."','".$_POST['IDDiscord']."')";
									$mysqli->query($query);
									/* Fermeture de la connexion */
									$mysqli->close();
									$mysqli = null;
								
									echo '<script> success("Votre compte Discord \\"'.$_POST['pseudoDiscord'].'\\" a été associé avec succès à votre compte Directquiz !"); </script>';
								}
								catch (Exception $ex)
								{
									echo '<script> error("Une erreur est survenue lors de l\'association de votre compte Discord à votre compte Directquiz 😔"); </script>';
								}
							}
							
						?>
	
    <!-- end section -->


	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

   
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script> 
</body>

</html>