<?php session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
	<base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz</title>
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	<!-- Datatable CSS -->
    <link rel="stylesheet" href="css/data-table.css">
	<!-- Chosen -->
	<link rel="stylesheet" href="css/chosen.min.css">

	<style>
		#questionToValidate_info
		{
			color : white;
		}
		
		.white{
			color: white!important;
		}
	</style>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>



	<script>
	var globalQ = "";
	var globalR = "";
	
	$(function(){
		
		
				
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
						
						
						
					}).
					fail(function(){
						
						
						
					});

		setJqueryDataTable();
		
	});
	
	function refreshDirectDollar()
	{
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}

						
					}).
					fail(function(){
						
						
						
					});
	}
	
	function validerQuestion(moimeme)
	{
		var q = $(moimeme).parent().parent().children("td").eq(1).text();
		var r = $(moimeme).parent().parent().children("td").eq(2).text();
		globalQ = q;
		globalR = r;
		
		if (confirm("Etes-vous sûr de vouloir approuver cette question : \""+q+"\" ?"))
		{
			$("#modale-corr").show();
			$("#repriseQuestion").text(globalQ);
			$("#repriseReponse").text(globalR);
			
			$(".lst").chosen({no_results_text: "Aucune catégorie trouvée...",});
		
			$("#validerFULL").click(function(){
					
					var tags_ = new Array(0);
					
					for (i = 1; i<= 4; i++)
					{
						if ($("#ddlTag"+i+" option:selected").val().trim() != "")
						{
							tags_.push($("#ddlTag"+i+" option:selected").val());
						}
					}

					if ($("#ddlTag5 option:selected").val().trim() != "")
					{
						tags_.push($("#ddlTag5 option:selected").val());
					}

					var categoriesIdentiques = false;
					
					tags_.forEach(
							function (element, index, array) {
									if (!categoriesIdentiques)
									categoriesIdentiques = array.indexOf(element) != array.lastIndexOf(element);
							}
					);
					
				if (!categoriesIdentiques)
				{

					var tagsF_ = tags_.join("|");

					if (tagsF_ != "")
					{
		
						$.post("accepterThis.php",{question:globalQ, tags:tagsF_}).done(function(data){
						
							if (data.trim() == "OK")
							{
								$(moimeme).parent().parent().remove();
								alert("Question approuvée avec succès !");
							}
							else
							{
								alert("Une erreur est survenue !");
							}
							
							$("#modale-corr").hide();
						
						});
						
								
					}
					else
					{
						alert("Veuillez encoder au moins une catégorie..");
					}
					
				}
				else
				{
					alert("Vous avez encodé plusieurs catégories identiques, veuiller rectifier...");
				}

			});
		
		}
		
	}
	
	function refuserQuestion(moimeme)
	{
		var q = $(moimeme).parent().parent().children("td").eq(1).text();
		
		if (confirm("Etes-vous sûr de vouloir refuser la question : \""+q+"\" ?"))
		{
		
			$.post("refuserThis.php",{question:q}).done(function(data){
				
				if (data.trim() == "OK")
				{
					$(moimeme).parent().parent().remove();
					alert("Refusé avec succès !");
				}
				
			});
		
		}
		
	}
	
	function fermerModale()
	{
		$("#liste-corr").html("");
		$("#modale-corr").hide();
	}
	
	
	
	
	function soumettreQuestion()
	{

		var question_ =	$("#txtQuestion").val().trim();

		var reponses_ = new Array(0);


				for (i = 1; i<= 4; i++)
				{
					if ($("#txtReponse"+i).val().trim() != "")
					{
						reponses_.push($("#txtReponse"+i).val().trim().replace(/\|/,""));
					}

				}

				reponsesIdentiques = false;
				
				reponses_.forEach(
						function (element, index, array) {
								if (!reponsesIdentiques)
								reponsesIdentiques = array.indexOf(element) != array.lastIndexOf(element);
						}
				);
				
				
			if (!reponsesIdentiques)
			{
				var reponsesF_ = reponses_.join("|");

				var diff_ =	$("#ddlDifficulte option:selected").val();


				if (question_ != "" && reponsesF_ != "")
				{

						$.post("depense.php", {uuid:'<?= $_SESSION['userid_dq'] ?>', depense:0}).done(function(data){
			
							if (data.trim() == 'OK')
							{
								$.post("insertQuestion.php",{question:question_, reponses:reponsesF_, tags:'', diff:diff_, uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){
						
									alert(data);
									
									$(".fields").val("");	
									
								});
								refreshDirectDollar()
							}
							else
							{
								alert("Vous n'avez pas assez de DirectDollar pour soumettre une question...");
							}
						
						});
							
				}
				else
				{
					alert("Veuillez compléter les informations manquantes...");
				}
				
			}
			else 
			{
				alert("Vous avez encodé plusieurs réponses identiques, veuiller rectifier...");
			}
		
	}
	
	  function setJqueryDataTable() {

            $("#questionToValidate").DataTable({
				 language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/French.json'
        },
                pageLength: 25,
                order: [1, "asc"],
                aoColumnDefs: [
                {
                    "targets": [0],
                    "orderable": true,
                    "searchable": true,
					"width" : 100
                },
			    {
                    "targets": [1],
                    "orderable": true,
                    "searchable": true,
                    "width" : 350
                },
                {
                    "targets": [2],
                    "orderable": true,
                    "searchable": true,
                    "width" : 200
                },
                {
                    "targets": [3],
                    "orderable": true,
                    "searchable": true,
                    "width" : 200
                },
                {
                    "targets": [4],
                    "orderable": true,
                    "searchable": false,
                    "width" : 50
                }
			    
                ],

                dom: 'Brftip'

            });

        }
		
		</script>
	
</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
             
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link active" href="validationQuestion.php">Proposer</a></li>
						
					   <li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					   
					   <li><a class="nav-link" href="discordEndPoint.php"><image src="images/discord_chat.png"></image> Lier</a></li>
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
		
					   </ul>
                </div>
                <div class="search-box">

                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->


    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">PROPOSITIONS </span>DE QUESTIONS</h2>
                            <p class="large">Les futures questions de DirectQuiz</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">


                <div class="col-lg-12 col-md-12 col-sm-12">
             
			 
			 <h2 class="white">Proposer une question</h2>
			 <table id="table-proposer">
				<tr><td class="celulle">Question</td><td><input type="text" placeholder="Intitulé de la question" class="fields" style="width:600px;" id="txtQuestion" /></td></tr>
				<tr><td class="celulle">Réponse(s) possible(s)</td><td><input type="text" placeholder="Réponse 1" class="fields" id="txtReponse1" /><br/><input type="text" placeholder="Réponse 2" class="fields" id="txtReponse2" /> <span class="white"><i>(facultatif)</i></span> <br/><input type="text" placeholder="Réponse 3" class="fields" id="txtReponse3" /> <span class="white"><i>(facultatif)</i></span> <br/><input type="text" placeholder="Réponse 4" class="fields" id="txtReponse4" /> <span class="white"><i>(facultatif)</i></span> </td></tr>

				<tr><td class="celulle">Difficulté</td><td>
				
				<select id="ddlDifficulte">
					<option value="0" style="background-color: cyan;">Ultra facile</option>
					<option value="8" style="background-color: #0F9;">Très facile</option>
					<option value="15" style="background-color: #3F0;">Facile</option>
					<option value="28" style="background-color: #FC0;">Moyenne</option>
					<option value="55" style="background-color: #F90;">Pas facile</option>
					<option value="65" style="background-color: #F30;">Difficile</option>
					<option value="75" style="background-color: #F00;">Très difficile</option>
					<option value="80" style="background-color: #900;">Compliquée</option>
					<option value="90" style="background-color: #300; color: white;">Très compliquée</option>
					<option value="100" style="background-color: #000; color: white;">Presqu'impossible</option>
				</select>
				</td></tr>
				<tr><td colspan="2"><input type="button" value="Soumettre" onClick="soumettreQuestion()" /> <span style="color:white;">(Elle sera soumise à validation)</span></td></tr>
			 </table>

						<?php
						include('config.php');
							$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
							$mysqli->set_charset("utf8mb4");
							if ($mysqli->connect_errno) {
								echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
							}
							
								$query = "CALL DQ_GetRoleByUUID('".$_SESSION['userid_dq']."')";
								$result0 = $mysqli->query($query);			
						
								$role = $result0->fetch_array(MYSQLI_ASSOC);
								
								$roleFinal = $role['Role'];
						
								/* Libération des résultats */
								$result0->free();
						
														/* Fermeture de la connexion */
								$mysqli->close();
							
							
							$mysqli = null;
						
						if ($roleFinal == 5)
						{
							
							$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
							$mysqli->set_charset("utf8mb4");
							if ($mysqli->connect_errno) {
								echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
							}
							
							echo '<p>
									<h2 class="white">Propositions de question</h2>';

								$query = "CALL DQ_GetQuestionsAValider()";
								$result = $mysqli->query($query);
								
								echo '<table id="questionToValidate" class="cell-border white">';
								echo '<thead>';
								echo '<tr><th>Auteur</th><th>Question</th><th>Réponse(s)</th><th>Tags</th><th>% Difficulté</th><th></th></tr>';
								echo '</thead>';
								echo '<tbody>';
								/* Tableau associatif */
								while ($row = $result->fetch_array(MYSQLI_ASSOC))
								{
									echo '<tr><td><img class="avatar-classement" src="images/avatar/'.$row['AuteurUUID'].'.jpg?'.time().'" /> '.$row['Pseudo'].'</td><td>'.$row['question'].'</td><td>'.$row['answers'].'</td><td>'.$row['tags'].'</td><td>'.$row['difficulty'].'</td><td><input type="button" value="V" class="acceptation" onClick="validerQuestion(this)" /> <input type="button" class="refus" value="X" onClick="refuserQuestion(this)" /></td></tr>';
								}

								echo '</tbody>';
								echo '</table>';

								/* Libération des résultats */
								$result->free();

								/* Fermeture de la connexion */
								$mysqli->close();
							
							
							$mysqli = null;
							
							echo '</p>';
							
						}
						?>
					
					
            
			<?php
	
			?>
			
                </div>
            </div>
        </div>
    </div>
    <!-- end section -->

	<div id="modale-corr">
	<span style="cursor:pointer; text-decoration: underline; float:right;" onClick="fermerModale()">FERMER [X]</span>
	<br/>
	<h2>Validation d'une question</h2>
	<br/>
	<span>Question : </span><span id="repriseQuestion"></span><br/>
	<span>Réponse(s) : </span><span id="repriseReponse"></span>
	<br/>
	<table id="table-validation">
				<tr><td>Choisissez entre 1 et 5 catégories</td><td>
				
				<select class="lst" id="ddlTag1" data-placeholder="Catégorie 1"><option value=""> </option><option value="Acronyme">001 : Acronyme </option><option value="Action">002 : Action </option><option value="Affaires criminelles">003 : Affaires criminelles </option><option value="Afrique">004 : Afrique </option><option value="Agriculture">005 : Agriculture </option><option value="Alcool">006 : Alcool </option><option value="Allemagne">007 : Allemagne </option><option value="Alpinisme">008 : Alpinisme </option><option value="Amérique">009 : Amérique </option><option value="Amérique latine">010 : Amerique latine </option><option value="Amour">011 : Amour </option><option value="Anatomie">012 : Anatomie </option><option value="Animaux">013 : Animaux </option><option value="Anime">014 : Anime </option><option value="Années 1900">015 : Années 1900 </option><option value="Années 1910">016 : Années 1910 </option><option value="Années 1920">017 : Années 1920 </option><option value="Années 1930">018 : Années 1930 </option><option value="Années 1940">019 : Années 1940 </option><option value="Années 1950">020 : Années 1950 </option><option value="Années 1960">021 : Années 1960 </option><option value="Années 1970">022 : Années 1970 </option><option value="Années 1980">023 : Années 1980 </option><option value="Années 1990">024 : Années 1990 </option><option value="Années 2000">025 : Années 2000 </option><option value="Années 2010">026 : Années 2010 </option><option value="Années 2020">027 : Années 2020 </option><option value="Anthropologie">028 : Anthropologie </option><option value="Antiquité">029 : Antiquité </option><option value="Apple">030 : Apple </option><option value="Application">031 : Application </option><option value="Archéologie">032 : Archéologie </option><option value="Architecture">033 : Architecture </option><option value="Armes">034 : Armes </option><option value="Armée">035 : Armée </option><option value="Art contemporain">036 : Art contemporain </option><option value="Art moderne">037 : Art moderne </option><option value="Arts">038 : Arts </option><option value="Asie">039 : Asie </option><option value="Astronomie">040 : Astronomie </option><option value="Athlétisme">041 : Athlétisme </option><option value="Australie">042 : Australie </option><option value="Auto">043 : Auto </option><option value="Autriche">044 : Autriche </option><option value="Aventure">045 : Aventure </option><option value="Aviation">046 : Aviation </option><option value="Bande dessinée">047 : Bande dessinée </option><option value="Base ball">048 : Base ball </option><option value="Basket">049 : Basket </option><option value="Bataille">050 : Bataille </option><option value="Belgique">051 : Belgique </option><option value="Bien-être">052 : Bien-être </option><option value="Biographie">053 : Biographie </option><option value="Biologie">054 : Biologie </option><option value="Blues">055 : Blues </option><option value="Bonheur">056 : Bonheur </option><option value="Botanique">057 : Botanique </option><option value="Boxe">058 : Boxe </option><option value="Boys band">059 : Boys band </option><option value="Bretagne">060 : Bretagne </option><option value="Brésil">061 : Brésil </option><option value="Calcul mental">062 : Calcul mental </option><option value="Canada">063 : Canada </option><option value="Capitales">064 : Capitales </option><option value="Catastrophe">065 : Catastrophe </option><option value="Catholicisme">066 : Catholicisme </option><option value="Chanson">067 : Chanson </option><option value="Chanson francophone">068 : Chanson francophone </option><option value="Chefs d'état">069 : Chefs d'état </option><option value="Chimie">070 : Chimie </option><option value="Chine">071 : Chine </option><option value="Christianisme">072 : Christianisme </option><option value="Cinéma">073 : Cinéma </option><option value="Citations">074 : Citations </option><option value="Classique">075 : Classique </option><option value="Comédie">076 : Comédie </option><option value="Comédie dramatique">077 : Comédie dramatique </option><option value="Comédie musicale">078 : Comédie musicale </option><option value="Comédie romantique">079: Comédie romantique </option><option value="Comics">080 : Comics </option><option value="Conte">081 : Conte </option><option value="Couleurs">082 : Couleurs </option><option value="Country">083 : Country </option><option value="Cuba">084 : Cuba </option><option value="Cuisine">085 : Cuisine </option><option value="Culture">086 : Culture </option><option value="Culture geek">087 : Culture geek </option><option value="Culture générale">088 : Culture générale </option><option value="Cyclisme">089 : Cyclisme </option><option value="Danse">090 : Danse </option><option value="Déserts">091 : Déserts </option><option value="Dessin animé">092 : Dessin animé </option><option value="Disco">093 : Disco </option><option value="Disney">094 : Disney </option><option value="Drame">095 : Drame </option><option value="Drapeaux">096 : Drapeaux </option><option value="Droit">097 : Droit </option><option value="Droits de l'homme">098 : Droits de l'homme </option><option value="Ecologie">099 : Ecologie </option><option value="Economie">100 : Economie </option><option value="Education">101 : Education </option><option value="Egypte">102 : Egypte </option><option value="Electricité">103 : Electricité </option><option value="Electro">104 : Electro </option><option value="Energie">105 : Energie </option><option value="Enfance">106 : Enfance </option><option value="Epoque contemporaine">107 : Epoque contemporaine </option><option value="Epoque moderne">108 : Epoque moderne </option><option value="Espace">109 : Espace </option><option value="Espagne">110 : Espagne </option><option value="Etats-unis">111 : Etats-unis </option><option value="Etymologie">112 : Etymologie </option><option value="Eurodance">113 : Eurodance </option><option value="Europe">114 : Europe </option><option value="Europe de l'est">115 : Europe de l'est </option><option value="Europe du nord">116 : Europe du nord </option><option value="Explorateur">117 : Explorateur </option><option value="Expressions">118 : Expressions </option><option value="Fantastique">119 : Fantastique </option><option value="Femmes">120 : Femmes </option><option value="Fête">121 : Fête </option><option value="Film">122 : Film </option><option value="Film d'animation">123 : Film d'animation </option><option value="Film historique">124 : Film historique </option><option value="Finances">125 : Finances </option><option value="Folk">126 : Folk </option><option value="Football">127 : Football </option><option value="France">128 : France </option><option value="Fruit">129 : Fruit </option><option value="Funk">130 : Funk </option><option value="Gangsters">131 : Gangsters </option><option value="Géographie">132 : Géographie </option><option value="Geologie">133 : Geologie </option><option value="Grandes premières">134 : Grandes premières </option><option value="Grêce">135 : Grêce </option><option value="Grunge">136 : Grunge </option><option value="Guerre">137 : Guerre </option><option value="Guitare">138 : Guitare </option><option value="Gymnastique">139 : Gymnastique </option><option value="Handball">140 : Handball </option><option value="Hard rock">141 : Hard rock </option><option value="Harry potter">142 : Harry potter </option><option value="Heavy metal">143 : Heavy metal </option><option value="Hip hop">144 : Hip hop </option><option value="Histoire">145 : Histoire </option><option value="Hockey">146 : Hockey </option><option value="Horreur">147 : Horreur </option><option value="Humour">148 : Humour </option><option value="Hymnes">149 : Hymnes </option><option value="Iles">150 : Iles </option><option value="Inde">151 : Inde </option><option value="Industrie">152 : Industrie </option><option value="Informatique">153 : Informatique </option><option value="Insectes">154 : Insectes </option><option value="International">155 : International </option><option value="Internet">156 : Internet </option><option value="Inventions">157 : Inventions </option><option value="Irlande">158 : Irlande </option><option value="Israël">159 : Israël </option><option value="Italie">160 : Italie </option><option value="Japon">161 : Japon </option><option value="Jazz">162 : Jazz </option><option value="Jeu">163 : Jeu </option><option value="Jeunesse">164 : Jeunesse </option><option value="Jeux de rôle">166 : Jeux de rôle </option><option value="Jeux de société">167 : Jeux de société </option><option value="Jeux olympiques">168 : Jeux olympiques </option><option value="Jeux tv">169 : Jeux tv </option><option value="Jeux vidéo">170 : Jeux vidéo </option><option value="Journalisme">171 : Journalisme </option><option value="Judaïsme">172 : Judaïsme </option><option value="Judo">173 : Judo </option><option value="Langue française">174 : Langue française </option><option value="Langues">175 : Langues </option><option value="Légendes">176 : Légendes </option><option value="Light novel">177 : Light novel </option><option value="Linguistique">178 : Linguistique </option><option value="Littérature">179 : Littérature </option><option value="Logiciel">180 : Logiciel </option><option value="Loisirs">181 : Loisirs </option><option value="Maghreb">182 : Maghreb </option><option value="Manga">183 : Manga </option><option value="Marques">184 : Marques </option><option value="Mathematiques">185 : Mathematiques </option><option value="Médecine">186 : Médecine </option><option value="Mers et océans">187 : Mers et océans </option><option value="Metal">188 : Métal </option><option value="Météo">189 : Météo </option><option value="Militaire">190 : Militaire </option><option value="Mobile">191 : Mobile </option><option value="Mode">192 : Mode </option><option value="Monde arabo musulman">193 : Monde arabo musulman </option><option value="Monde celtique">194 : Monde celtique </option><option value="Monde équestre">195 : Monde équestre </option><option value="Monde turc">196 : Monde turc </option><option value="Monnaie">197 : Monnaie </option><option value="Montagne">198 : Montagne </option><option value="Motion twin">199 : Motion twin </option><option value="Moto">200 : Moto </option><option value="Moyen-age">201 : Moyen-age </option><option value="Muet">202 : Muet </option><option value="Musée">203 : Musée </option><option value="Musique">204 : Musique </option><option value="Musique électronique">205 : Musique électronique </option><option value="Musique française">206 : Musique française </option><option value="Musique francophone">207 : Musique francophone </option><option value="Mythologie">208 : Mythologie </option><option value="Mythologie grecque">209 : Mythologie grecque </option><option value="Mythologie nordique">210 : Mythologie nordique </option><option value="Mythologie romaine">211 : Mythologie romaine </option><option value="Mythologie égyptienne">212 : Mythologie égyptienne </option><option value="Médias">213 : Médias </option><option value="Métiers">214 : Métiers </option><option value="Nanar">215 : Nanar </option><option value="Napoleon">216 : Napoleon </option><option value="Natation">217 : Natation </option><option value="Nature">218 : Nature </option><option value="Navigation">219 : Navigation </option><option value="Neo metal">220 : Neo metal </option><option value="New wave">221 : New wave </option><option value="Nintendo">222 : Nintendo </option><option value="Noël">223 : Noël </option><option value="Noir et blanc">224 : Noir et blanc </option><option value="Nordique">225 : Nordique </option><option value="Nu metal">226 : Nu metal </option><option value="Océanie">227 : Océanie </option><option value="Oenologie">228 : Oenologie </option><option value="Oiseaux">229 : Oiseaux </option><option value="Opera">230 : Opera </option><option value="Optique">231 : Optique </option><option value="Outre mer">232 : Outre mer </option><option value="Paranormal">233 : Paranormal </option><option value="Paris">234 : Paris </option><option value="Patisserie">235 : Patisserie </option><option value="Pays-bas">236 : Pays-bas </option><option value="Peinture">237 : Peinture </option><option value="People">238 : People </option><option value="Peplum">239 : Peplum </option><option value="Peuples premiers">240 : Peuples premiers </option><option value="Philosophie">241 : Philosophie </option><option value="Photographie">242 : Photographie </option><option value="Physique">243 : Physique </option><option value="Pirates">244 : Pirates </option><option value="Playstation">245 : Playstation </option><option value="Poésie">246 : Poésie </option><option value="Polar">247 : Polar </option><option value="Policier">248 : Policier </option><option value="Politique">249 : Politique </option><option value="Pologne">250 : Pologne </option><option value="Pop">251 : Pop </option><option value="Pop rock">252 : Pop rock </option><option value="Portugal">253 : Portugal </option><option value="Préhistoire">254 : Préhistoire </option><option value="Président">255 : Président </option><option value="Presse">256 : Presse </option><option value="Prix">257 : Prix </option><option value="Provence">258 : Provence </option><option value="Proverbes">259 : Proverbes </option><option value="Publicités">260 : Publicités </option><option value="Punk">261 : Punk </option><option value="Quebec">262 : Quebec </option><option value="Radio">263 : Radio </option><option value="Rai">264 : Rai </option><option value="Rap">265 : Rap </option><option value="Reggae">266 : Reggae </option><option value="Régions">267 : Régions </option><option value="Religion">268 : Religion </option><option value="Renaissance">269 : Renaissance </option><option value="Révolution">270 : Révolution </option><option value="Rnb">271 : Rnb </option><option value="Rock">272 : Rock </option><option value="Rock alternatif">273 : Rock alternatif </option><option value="Rois et reines">274 : Rois et reines </option><option value="Romance">275 : Romance </option><option value="Roman historique">276 : Roman historique </option><option value="Royaumes-unis">277 : Royaumes-unis </option><option value="Rugby">278 : Rugby </option><option value="Russie">279 : Russie </option><option value="Russie 2018">280 : Russie 2018 </option><option value="Saga audio">281 : Saga audio </option><option value="Santé">282 : Santé </option><option value="Scandales">283 : Scandales </option><option value="Scandinavie">284 : Scandinavie </option><option value="Science-fiction">285 : Science-fiction </option><option value="Sciences">286 : Sciences </option><option value="Sculpture">287 : Sculpture </option><option value="Serbie">288 : Serbie </option><option value="Série dramatique">289 : Série dramatique </option><option value="Séries tv">290 : Séries tv </option><option value="Shakespeare">291 : Shakespeare </option><option value="Sigles">292 : Sigles </option><option value="Sitcom">293 : Sitcom </option><option value="Snapchat">294 : Snapchat </option><option value="Soap opera">295 : Soap opera </option><option value="Sociologie">296 : Sociologie </option><option value="Société">297 : Société </option><option value="Solfege">298 : Solfege </option><option value="Soul">299 : Soul </option><option value="Spectacle">300 : Spectacle </option><option value="Sport">301 : Sport </option><option value="Suisse">302 : Suisse </option><option value="Surnoms">303 : Surnoms </option><option value="Surréalisme">304 : Surréalisme </option><option value="Synthpop">305 : Synthpop </option><option value="Technique musicale">306 : Technique musicale </option><option value="Technologie">307 : Technologie </option><option value="Télévision">308 : Télévision </option><option value="Tennis">309 : Tennis </option><option value="Tennis de table">310 : Tennis de table </option><option value="Terroir">311 : Terroir </option><option value="TF1">312 : TF1 </option><option value="Thriller">313 : Thriller </option><option value="Théâtre">314 : Théâtre </option><option value="Tintin">315 : Tintin </option><option value="Train">316 : Train </option><option value="Transports">317 : Transports </option><option value="Turquie">318 : Turquie </option><option value="Urbanisme">319 : Urbanisme </option><option value="USA">320 : USA </option><option value="Variété">321 : Variété </option><option value="Ville">322 : Ville </option><option value="Volley-ball">323 : Volley-ball </option><option value="Western">324 : Western </option><option value="World music">325 : World music </option><option value="Xbox">326 : Xbox </option><option value="Zoologie">327 : Zoologie </option></select>
				
				<br/>
				
				<select class="lst" id="ddlTag2" data-placeholder="Catégorie 2"><option value=""> </option><option value="Acronyme">001 : Acronyme </option><option value="Action">002 : Action </option><option value="Affaires criminelles">003 : Affaires criminelles </option><option value="Afrique">004 : Afrique </option><option value="Agriculture">005 : Agriculture </option><option value="Alcool">006 : Alcool </option><option value="Allemagne">007 : Allemagne </option><option value="Alpinisme">008 : Alpinisme </option><option value="Amérique">009 : Amérique </option><option value="Amérique latine">010 : Amerique latine </option><option value="Amour">011 : Amour </option><option value="Anatomie">012 : Anatomie </option><option value="Animaux">013 : Animaux </option><option value="Anime">014 : Anime </option><option value="Années 1900">015 : Années 1900 </option><option value="Années 1910">016 : Années 1910 </option><option value="Années 1920">017 : Années 1920 </option><option value="Années 1930">018 : Années 1930 </option><option value="Années 1940">019 : Années 1940 </option><option value="Années 1950">020 : Années 1950 </option><option value="Années 1960">021 : Années 1960 </option><option value="Années 1970">022 : Années 1970 </option><option value="Années 1980">023 : Années 1980 </option><option value="Années 1990">024 : Années 1990 </option><option value="Années 2000">025 : Années 2000 </option><option value="Années 2010">026 : Années 2010 </option><option value="Années 2020">027 : Années 2020 </option><option value="Anthropologie">028 : Anthropologie </option><option value="Antiquité">029 : Antiquité </option><option value="Apple">030 : Apple </option><option value="Application">031 : Application </option><option value="Archéologie">032 : Archéologie </option><option value="Architecture">033 : Architecture </option><option value="Armes">034 : Armes </option><option value="Armée">035 : Armée </option><option value="Art contemporain">036 : Art contemporain </option><option value="Art moderne">037 : Art moderne </option><option value="Arts">038 : Arts </option><option value="Asie">039 : Asie </option><option value="Astronomie">040 : Astronomie </option><option value="Athlétisme">041 : Athlétisme </option><option value="Australie">042 : Australie </option><option value="Auto">043 : Auto </option><option value="Autriche">044 : Autriche </option><option value="Aventure">045 : Aventure </option><option value="Aviation">046 : Aviation </option><option value="Bande dessinée">047 : Bande dessinée </option><option value="Base ball">048 : Base ball </option><option value="Basket">049 : Basket </option><option value="Bataille">050 : Bataille </option><option value="Belgique">051 : Belgique </option><option value="Bien-être">052 : Bien-être </option><option value="Biographie">053 : Biographie </option><option value="Biologie">054 : Biologie </option><option value="Blues">055 : Blues </option><option value="Bonheur">056 : Bonheur </option><option value="Botanique">057 : Botanique </option><option value="Boxe">058 : Boxe </option><option value="Boys band">059 : Boys band </option><option value="Bretagne">060 : Bretagne </option><option value="Brésil">061 : Brésil </option><option value="Calcul mental">062 : Calcul mental </option><option value="Canada">063 : Canada </option><option value="Capitales">064 : Capitales </option><option value="Catastrophe">065 : Catastrophe </option><option value="Catholicisme">066 : Catholicisme </option><option value="Chanson">067 : Chanson </option><option value="Chanson francophone">068 : Chanson francophone </option><option value="Chefs d'état">069 : Chefs d'état </option><option value="Chimie">070 : Chimie </option><option value="Chine">071 : Chine </option><option value="Christianisme">072 : Christianisme </option><option value="Cinéma">073 : Cinéma </option><option value="Citations">074 : Citations </option><option value="Classique">075 : Classique </option><option value="Comédie">076 : Comédie </option><option value="Comédie dramatique">077 : Comédie dramatique </option><option value="Comédie musicale">078 : Comédie musicale </option><option value="Comédie romantique">079: Comédie romantique </option><option value="Comics">080 : Comics </option><option value="Conte">081 : Conte </option><option value="Couleurs">082 : Couleurs </option><option value="Country">083 : Country </option><option value="Cuba">084 : Cuba </option><option value="Cuisine">085 : Cuisine </option><option value="Culture">086 : Culture </option><option value="Culture geek">087 : Culture geek </option><option value="Culture générale">088 : Culture générale </option><option value="Cyclisme">089 : Cyclisme </option><option value="Danse">090 : Danse </option><option value="Déserts">091 : Déserts </option><option value="Dessin animé">092 : Dessin animé </option><option value="Disco">093 : Disco </option><option value="Disney">094 : Disney </option><option value="Drame">095 : Drame </option><option value="Drapeaux">096 : Drapeaux </option><option value="Droit">097 : Droit </option><option value="Droits de l'homme">098 : Droits de l'homme </option><option value="Ecologie">099 : Ecologie </option><option value="Economie">100 : Economie </option><option value="Education">101 : Education </option><option value="Egypte">102 : Egypte </option><option value="Electricité">103 : Electricité </option><option value="Electro">104 : Electro </option><option value="Energie">105 : Energie </option><option value="Enfance">106 : Enfance </option><option value="Epoque contemporaine">107 : Epoque contemporaine </option><option value="Epoque moderne">108 : Epoque moderne </option><option value="Espace">109 : Espace </option><option value="Espagne">110 : Espagne </option><option value="Etats-unis">111 : Etats-unis </option><option value="Etymologie">112 : Etymologie </option><option value="Eurodance">113 : Eurodance </option><option value="Europe">114 : Europe </option><option value="Europe de l'est">115 : Europe de l'est </option><option value="Europe du nord">116 : Europe du nord </option><option value="Explorateur">117 : Explorateur </option><option value="Expressions">118 : Expressions </option><option value="Fantastique">119 : Fantastique </option><option value="Femmes">120 : Femmes </option><option value="Fête">121 : Fête </option><option value="Film">122 : Film </option><option value="Film d'animation">123 : Film d'animation </option><option value="Film historique">124 : Film historique </option><option value="Finances">125 : Finances </option><option value="Folk">126 : Folk </option><option value="Football">127 : Football </option><option value="France">128 : France </option><option value="Fruit">129 : Fruit </option><option value="Funk">130 : Funk </option><option value="Gangsters">131 : Gangsters </option><option value="Géographie">132 : Géographie </option><option value="Geologie">133 : Geologie </option><option value="Grandes premières">134 : Grandes premières </option><option value="Grêce">135 : Grêce </option><option value="Grunge">136 : Grunge </option><option value="Guerre">137 : Guerre </option><option value="Guitare">138 : Guitare </option><option value="Gymnastique">139 : Gymnastique </option><option value="Handball">140 : Handball </option><option value="Hard rock">141 : Hard rock </option><option value="Harry potter">142 : Harry potter </option><option value="Heavy metal">143 : Heavy metal </option><option value="Hip hop">144 : Hip hop </option><option value="Histoire">145 : Histoire </option><option value="Hockey">146 : Hockey </option><option value="Horreur">147 : Horreur </option><option value="Humour">148 : Humour </option><option value="Hymnes">149 : Hymnes </option><option value="Iles">150 : Iles </option><option value="Inde">151 : Inde </option><option value="Industrie">152 : Industrie </option><option value="Informatique">153 : Informatique </option><option value="Insectes">154 : Insectes </option><option value="International">155 : International </option><option value="Internet">156 : Internet </option><option value="Inventions">157 : Inventions </option><option value="Irlande">158 : Irlande </option><option value="Israël">159 : Israël </option><option value="Italie">160 : Italie </option><option value="Japon">161 : Japon </option><option value="Jazz">162 : Jazz </option><option value="Jeu">163 : Jeu </option><option value="Jeunesse">164 : Jeunesse </option><option value="Jeux de rôle">166 : Jeux de rôle </option><option value="Jeux de société">167 : Jeux de société </option><option value="Jeux olympiques">168 : Jeux olympiques </option><option value="Jeux tv">169 : Jeux tv </option><option value="Jeux vidéo">170 : Jeux vidéo </option><option value="Journalisme">171 : Journalisme </option><option value="Judaïsme">172 : Judaïsme </option><option value="Judo">173 : Judo </option><option value="Langue française">174 : Langue française </option><option value="Langues">175 : Langues </option><option value="Légendes">176 : Légendes </option><option value="Light novel">177 : Light novel </option><option value="Linguistique">178 : Linguistique </option><option value="Littérature">179 : Littérature </option><option value="Logiciel">180 : Logiciel </option><option value="Loisirs">181 : Loisirs </option><option value="Maghreb">182 : Maghreb </option><option value="Manga">183 : Manga </option><option value="Marques">184 : Marques </option><option value="Mathematiques">185 : Mathematiques </option><option value="Médecine">186 : Médecine </option><option value="Mers et océans">187 : Mers et océans </option><option value="Metal">188 : Métal </option><option value="Météo">189 : Météo </option><option value="Militaire">190 : Militaire </option><option value="Mobile">191 : Mobile </option><option value="Mode">192 : Mode </option><option value="Monde arabo musulman">193 : Monde arabo musulman </option><option value="Monde celtique">194 : Monde celtique </option><option value="Monde équestre">195 : Monde équestre </option><option value="Monde turc">196 : Monde turc </option><option value="Monnaie">197 : Monnaie </option><option value="Montagne">198 : Montagne </option><option value="Motion twin">199 : Motion twin </option><option value="Moto">200 : Moto </option><option value="Moyen-age">201 : Moyen-age </option><option value="Muet">202 : Muet </option><option value="Musée">203 : Musée </option><option value="Musique">204 : Musique </option><option value="Musique électronique">205 : Musique électronique </option><option value="Musique française">206 : Musique française </option><option value="Musique francophone">207 : Musique francophone </option><option value="Mythologie">208 : Mythologie </option><option value="Mythologie grecque">209 : Mythologie grecque </option><option value="Mythologie nordique">210 : Mythologie nordique </option><option value="Mythologie romaine">211 : Mythologie romaine </option><option value="Mythologie égyptienne">212 : Mythologie égyptienne </option><option value="Médias">213 : Médias </option><option value="Métiers">214 : Métiers </option><option value="Nanar">215 : Nanar </option><option value="Napoleon">216 : Napoleon </option><option value="Natation">217 : Natation </option><option value="Nature">218 : Nature </option><option value="Navigation">219 : Navigation </option><option value="Neo metal">220 : Neo metal </option><option value="New wave">221 : New wave </option><option value="Nintendo">222 : Nintendo </option><option value="Noël">223 : Noël </option><option value="Noir et blanc">224 : Noir et blanc </option><option value="Nordique">225 : Nordique </option><option value="Nu metal">226 : Nu metal </option><option value="Océanie">227 : Océanie </option><option value="Oenologie">228 : Oenologie </option><option value="Oiseaux">229 : Oiseaux </option><option value="Opera">230 : Opera </option><option value="Optique">231 : Optique </option><option value="Outre mer">232 : Outre mer </option><option value="Paranormal">233 : Paranormal </option><option value="Paris">234 : Paris </option><option value="Patisserie">235 : Patisserie </option><option value="Pays-bas">236 : Pays-bas </option><option value="Peinture">237 : Peinture </option><option value="People">238 : People </option><option value="Peplum">239 : Peplum </option><option value="Peuples premiers">240 : Peuples premiers </option><option value="Philosophie">241 : Philosophie </option><option value="Photographie">242 : Photographie </option><option value="Physique">243 : Physique </option><option value="Pirates">244 : Pirates </option><option value="Playstation">245 : Playstation </option><option value="Poésie">246 : Poésie </option><option value="Polar">247 : Polar </option><option value="Policier">248 : Policier </option><option value="Politique">249 : Politique </option><option value="Pologne">250 : Pologne </option><option value="Pop">251 : Pop </option><option value="Pop rock">252 : Pop rock </option><option value="Portugal">253 : Portugal </option><option value="Préhistoire">254 : Préhistoire </option><option value="Président">255 : Président </option><option value="Presse">256 : Presse </option><option value="Prix">257 : Prix </option><option value="Provence">258 : Provence </option><option value="Proverbes">259 : Proverbes </option><option value="Publicités">260 : Publicités </option><option value="Punk">261 : Punk </option><option value="Quebec">262 : Quebec </option><option value="Radio">263 : Radio </option><option value="Rai">264 : Rai </option><option value="Rap">265 : Rap </option><option value="Reggae">266 : Reggae </option><option value="Régions">267 : Régions </option><option value="Religion">268 : Religion </option><option value="Renaissance">269 : Renaissance </option><option value="Révolution">270 : Révolution </option><option value="Rnb">271 : Rnb </option><option value="Rock">272 : Rock </option><option value="Rock alternatif">273 : Rock alternatif </option><option value="Rois et reines">274 : Rois et reines </option><option value="Romance">275 : Romance </option><option value="Roman historique">276 : Roman historique </option><option value="Royaumes-unis">277 : Royaumes-unis </option><option value="Rugby">278 : Rugby </option><option value="Russie">279 : Russie </option><option value="Russie 2018">280 : Russie 2018 </option><option value="Saga audio">281 : Saga audio </option><option value="Santé">282 : Santé </option><option value="Scandales">283 : Scandales </option><option value="Scandinavie">284 : Scandinavie </option><option value="Science-fiction">285 : Science-fiction </option><option value="Sciences">286 : Sciences </option><option value="Sculpture">287 : Sculpture </option><option value="Serbie">288 : Serbie </option><option value="Série dramatique">289 : Série dramatique </option><option value="Séries tv">290 : Séries tv </option><option value="Shakespeare">291 : Shakespeare </option><option value="Sigles">292 : Sigles </option><option value="Sitcom">293 : Sitcom </option><option value="Snapchat">294 : Snapchat </option><option value="Soap opera">295 : Soap opera </option><option value="Sociologie">296 : Sociologie </option><option value="Société">297 : Société </option><option value="Solfege">298 : Solfege </option><option value="Soul">299 : Soul </option><option value="Spectacle">300 : Spectacle </option><option value="Sport">301 : Sport </option><option value="Suisse">302 : Suisse </option><option value="Surnoms">303 : Surnoms </option><option value="Surréalisme">304 : Surréalisme </option><option value="Synthpop">305 : Synthpop </option><option value="Technique musicale">306 : Technique musicale </option><option value="Technologie">307 : Technologie </option><option value="Télévision">308 : Télévision </option><option value="Tennis">309 : Tennis </option><option value="Tennis de table">310 : Tennis de table </option><option value="Terroir">311 : Terroir </option><option value="TF1">312 : TF1 </option><option value="Thriller">313 : Thriller </option><option value="Théâtre">314 : Théâtre </option><option value="Tintin">315 : Tintin </option><option value="Train">316 : Train </option><option value="Transports">317 : Transports </option><option value="Turquie">318 : Turquie </option><option value="Urbanisme">319 : Urbanisme </option><option value="USA">320 : USA </option><option value="Variété">321 : Variété </option><option value="Ville">322 : Ville </option><option value="Volley-ball">323 : Volley-ball </option><option value="Western">324 : Western </option><option value="World music">325 : World music </option><option value="Xbox">326 : Xbox </option><option value="Zoologie">327 : Zoologie </option></select>
				
				<br/>
				
				<select class="lst" id="ddlTag3" data-placeholder="Catégorie 3"><option value=""> </option><option value="Acronyme">001 : Acronyme </option><option value="Action">002 : Action </option><option value="Affaires criminelles">003 : Affaires criminelles </option><option value="Afrique">004 : Afrique </option><option value="Agriculture">005 : Agriculture </option><option value="Alcool">006 : Alcool </option><option value="Allemagne">007 : Allemagne </option><option value="Alpinisme">008 : Alpinisme </option><option value="Amérique">009 : Amérique </option><option value="Amérique latine">010 : Amerique latine </option><option value="Amour">011 : Amour </option><option value="Anatomie">012 : Anatomie </option><option value="Animaux">013 : Animaux </option><option value="Anime">014 : Anime </option><option value="Années 1900">015 : Années 1900 </option><option value="Années 1910">016 : Années 1910 </option><option value="Années 1920">017 : Années 1920 </option><option value="Années 1930">018 : Années 1930 </option><option value="Années 1940">019 : Années 1940 </option><option value="Années 1950">020 : Années 1950 </option><option value="Années 1960">021 : Années 1960 </option><option value="Années 1970">022 : Années 1970 </option><option value="Années 1980">023 : Années 1980 </option><option value="Années 1990">024 : Années 1990 </option><option value="Années 2000">025 : Années 2000 </option><option value="Années 2010">026 : Années 2010 </option><option value="Années 2020">027 : Années 2020 </option><option value="Anthropologie">028 : Anthropologie </option><option value="Antiquité">029 : Antiquité </option><option value="Apple">030 : Apple </option><option value="Application">031 : Application </option><option value="Archéologie">032 : Archéologie </option><option value="Architecture">033 : Architecture </option><option value="Armes">034 : Armes </option><option value="Armée">035 : Armée </option><option value="Art contemporain">036 : Art contemporain </option><option value="Art moderne">037 : Art moderne </option><option value="Arts">038 : Arts </option><option value="Asie">039 : Asie </option><option value="Astronomie">040 : Astronomie </option><option value="Athlétisme">041 : Athlétisme </option><option value="Australie">042 : Australie </option><option value="Auto">043 : Auto </option><option value="Autriche">044 : Autriche </option><option value="Aventure">045 : Aventure </option><option value="Aviation">046 : Aviation </option><option value="Bande dessinée">047 : Bande dessinée </option><option value="Base ball">048 : Base ball </option><option value="Basket">049 : Basket </option><option value="Bataille">050 : Bataille </option><option value="Belgique">051 : Belgique </option><option value="Bien-être">052 : Bien-être </option><option value="Biographie">053 : Biographie </option><option value="Biologie">054 : Biologie </option><option value="Blues">055 : Blues </option><option value="Bonheur">056 : Bonheur </option><option value="Botanique">057 : Botanique </option><option value="Boxe">058 : Boxe </option><option value="Boys band">059 : Boys band </option><option value="Bretagne">060 : Bretagne </option><option value="Brésil">061 : Brésil </option><option value="Calcul mental">062 : Calcul mental </option><option value="Canada">063 : Canada </option><option value="Capitales">064 : Capitales </option><option value="Catastrophe">065 : Catastrophe </option><option value="Catholicisme">066 : Catholicisme </option><option value="Chanson">067 : Chanson </option><option value="Chanson francophone">068 : Chanson francophone </option><option value="Chefs d'état">069 : Chefs d'état </option><option value="Chimie">070 : Chimie </option><option value="Chine">071 : Chine </option><option value="Christianisme">072 : Christianisme </option><option value="Cinéma">073 : Cinéma </option><option value="Citations">074 : Citations </option><option value="Classique">075 : Classique </option><option value="Comédie">076 : Comédie </option><option value="Comédie dramatique">077 : Comédie dramatique </option><option value="Comédie musicale">078 : Comédie musicale </option><option value="Comédie romantique">079: Comédie romantique </option><option value="Comics">080 : Comics </option><option value="Conte">081 : Conte </option><option value="Couleurs">082 : Couleurs </option><option value="Country">083 : Country </option><option value="Cuba">084 : Cuba </option><option value="Cuisine">085 : Cuisine </option><option value="Culture">086 : Culture </option><option value="Culture geek">087 : Culture geek </option><option value="Culture générale">088 : Culture générale </option><option value="Cyclisme">089 : Cyclisme </option><option value="Danse">090 : Danse </option><option value="Déserts">091 : Déserts </option><option value="Dessin animé">092 : Dessin animé </option><option value="Disco">093 : Disco </option><option value="Disney">094 : Disney </option><option value="Drame">095 : Drame </option><option value="Drapeaux">096 : Drapeaux </option><option value="Droit">097 : Droit </option><option value="Droits de l'homme">098 : Droits de l'homme </option><option value="Ecologie">099 : Ecologie </option><option value="Economie">100 : Economie </option><option value="Education">101 : Education </option><option value="Egypte">102 : Egypte </option><option value="Electricité">103 : Electricité </option><option value="Electro">104 : Electro </option><option value="Energie">105 : Energie </option><option value="Enfance">106 : Enfance </option><option value="Epoque contemporaine">107 : Epoque contemporaine </option><option value="Epoque moderne">108 : Epoque moderne </option><option value="Espace">109 : Espace </option><option value="Espagne">110 : Espagne </option><option value="Etats-unis">111 : Etats-unis </option><option value="Etymologie">112 : Etymologie </option><option value="Eurodance">113 : Eurodance </option><option value="Europe">114 : Europe </option><option value="Europe de l'est">115 : Europe de l'est </option><option value="Europe du nord">116 : Europe du nord </option><option value="Explorateur">117 : Explorateur </option><option value="Expressions">118 : Expressions </option><option value="Fantastique">119 : Fantastique </option><option value="Femmes">120 : Femmes </option><option value="Fête">121 : Fête </option><option value="Film">122 : Film </option><option value="Film d'animation">123 : Film d'animation </option><option value="Film historique">124 : Film historique </option><option value="Finances">125 : Finances </option><option value="Folk">126 : Folk </option><option value="Football">127 : Football </option><option value="France">128 : France </option><option value="Fruit">129 : Fruit </option><option value="Funk">130 : Funk </option><option value="Gangsters">131 : Gangsters </option><option value="Géographie">132 : Géographie </option><option value="Geologie">133 : Geologie </option><option value="Grandes premières">134 : Grandes premières </option><option value="Grêce">135 : Grêce </option><option value="Grunge">136 : Grunge </option><option value="Guerre">137 : Guerre </option><option value="Guitare">138 : Guitare </option><option value="Gymnastique">139 : Gymnastique </option><option value="Handball">140 : Handball </option><option value="Hard rock">141 : Hard rock </option><option value="Harry potter">142 : Harry potter </option><option value="Heavy metal">143 : Heavy metal </option><option value="Hip hop">144 : Hip hop </option><option value="Histoire">145 : Histoire </option><option value="Hockey">146 : Hockey </option><option value="Horreur">147 : Horreur </option><option value="Humour">148 : Humour </option><option value="Hymnes">149 : Hymnes </option><option value="Iles">150 : Iles </option><option value="Inde">151 : Inde </option><option value="Industrie">152 : Industrie </option><option value="Informatique">153 : Informatique </option><option value="Insectes">154 : Insectes </option><option value="International">155 : International </option><option value="Internet">156 : Internet </option><option value="Inventions">157 : Inventions </option><option value="Irlande">158 : Irlande </option><option value="Israël">159 : Israël </option><option value="Italie">160 : Italie </option><option value="Japon">161 : Japon </option><option value="Jazz">162 : Jazz </option><option value="Jeu">163 : Jeu </option><option value="Jeunesse">164 : Jeunesse </option><option value="Jeux de rôle">166 : Jeux de rôle </option><option value="Jeux de société">167 : Jeux de société </option><option value="Jeux olympiques">168 : Jeux olympiques </option><option value="Jeux tv">169 : Jeux tv </option><option value="Jeux vidéo">170 : Jeux vidéo </option><option value="Journalisme">171 : Journalisme </option><option value="Judaïsme">172 : Judaïsme </option><option value="Judo">173 : Judo </option><option value="Langue française">174 : Langue française </option><option value="Langues">175 : Langues </option><option value="Légendes">176 : Légendes </option><option value="Light novel">177 : Light novel </option><option value="Linguistique">178 : Linguistique </option><option value="Littérature">179 : Littérature </option><option value="Logiciel">180 : Logiciel </option><option value="Loisirs">181 : Loisirs </option><option value="Maghreb">182 : Maghreb </option><option value="Manga">183 : Manga </option><option value="Marques">184 : Marques </option><option value="Mathematiques">185 : Mathematiques </option><option value="Médecine">186 : Médecine </option><option value="Mers et océans">187 : Mers et océans </option><option value="Metal">188 : Métal </option><option value="Météo">189 : Météo </option><option value="Militaire">190 : Militaire </option><option value="Mobile">191 : Mobile </option><option value="Mode">192 : Mode </option><option value="Monde arabo musulman">193 : Monde arabo musulman </option><option value="Monde celtique">194 : Monde celtique </option><option value="Monde équestre">195 : Monde équestre </option><option value="Monde turc">196 : Monde turc </option><option value="Monnaie">197 : Monnaie </option><option value="Montagne">198 : Montagne </option><option value="Motion twin">199 : Motion twin </option><option value="Moto">200 : Moto </option><option value="Moyen-age">201 : Moyen-age </option><option value="Muet">202 : Muet </option><option value="Musée">203 : Musée </option><option value="Musique">204 : Musique </option><option value="Musique électronique">205 : Musique électronique </option><option value="Musique française">206 : Musique française </option><option value="Musique francophone">207 : Musique francophone </option><option value="Mythologie">208 : Mythologie </option><option value="Mythologie grecque">209 : Mythologie grecque </option><option value="Mythologie nordique">210 : Mythologie nordique </option><option value="Mythologie romaine">211 : Mythologie romaine </option><option value="Mythologie égyptienne">212 : Mythologie égyptienne </option><option value="Médias">213 : Médias </option><option value="Métiers">214 : Métiers </option><option value="Nanar">215 : Nanar </option><option value="Napoleon">216 : Napoleon </option><option value="Natation">217 : Natation </option><option value="Nature">218 : Nature </option><option value="Navigation">219 : Navigation </option><option value="Neo metal">220 : Neo metal </option><option value="New wave">221 : New wave </option><option value="Nintendo">222 : Nintendo </option><option value="Noël">223 : Noël </option><option value="Noir et blanc">224 : Noir et blanc </option><option value="Nordique">225 : Nordique </option><option value="Nu metal">226 : Nu metal </option><option value="Océanie">227 : Océanie </option><option value="Oenologie">228 : Oenologie </option><option value="Oiseaux">229 : Oiseaux </option><option value="Opera">230 : Opera </option><option value="Optique">231 : Optique </option><option value="Outre mer">232 : Outre mer </option><option value="Paranormal">233 : Paranormal </option><option value="Paris">234 : Paris </option><option value="Patisserie">235 : Patisserie </option><option value="Pays-bas">236 : Pays-bas </option><option value="Peinture">237 : Peinture </option><option value="People">238 : People </option><option value="Peplum">239 : Peplum </option><option value="Peuples premiers">240 : Peuples premiers </option><option value="Philosophie">241 : Philosophie </option><option value="Photographie">242 : Photographie </option><option value="Physique">243 : Physique </option><option value="Pirates">244 : Pirates </option><option value="Playstation">245 : Playstation </option><option value="Poésie">246 : Poésie </option><option value="Polar">247 : Polar </option><option value="Policier">248 : Policier </option><option value="Politique">249 : Politique </option><option value="Pologne">250 : Pologne </option><option value="Pop">251 : Pop </option><option value="Pop rock">252 : Pop rock </option><option value="Portugal">253 : Portugal </option><option value="Préhistoire">254 : Préhistoire </option><option value="Président">255 : Président </option><option value="Presse">256 : Presse </option><option value="Prix">257 : Prix </option><option value="Provence">258 : Provence </option><option value="Proverbes">259 : Proverbes </option><option value="Publicités">260 : Publicités </option><option value="Punk">261 : Punk </option><option value="Quebec">262 : Quebec </option><option value="Radio">263 : Radio </option><option value="Rai">264 : Rai </option><option value="Rap">265 : Rap </option><option value="Reggae">266 : Reggae </option><option value="Régions">267 : Régions </option><option value="Religion">268 : Religion </option><option value="Renaissance">269 : Renaissance </option><option value="Révolution">270 : Révolution </option><option value="Rnb">271 : Rnb </option><option value="Rock">272 : Rock </option><option value="Rock alternatif">273 : Rock alternatif </option><option value="Rois et reines">274 : Rois et reines </option><option value="Romance">275 : Romance </option><option value="Roman historique">276 : Roman historique </option><option value="Royaumes-unis">277 : Royaumes-unis </option><option value="Rugby">278 : Rugby </option><option value="Russie">279 : Russie </option><option value="Russie 2018">280 : Russie 2018 </option><option value="Saga audio">281 : Saga audio </option><option value="Santé">282 : Santé </option><option value="Scandales">283 : Scandales </option><option value="Scandinavie">284 : Scandinavie </option><option value="Science-fiction">285 : Science-fiction </option><option value="Sciences">286 : Sciences </option><option value="Sculpture">287 : Sculpture </option><option value="Serbie">288 : Serbie </option><option value="Série dramatique">289 : Série dramatique </option><option value="Séries tv">290 : Séries tv </option><option value="Shakespeare">291 : Shakespeare </option><option value="Sigles">292 : Sigles </option><option value="Sitcom">293 : Sitcom </option><option value="Snapchat">294 : Snapchat </option><option value="Soap opera">295 : Soap opera </option><option value="Sociologie">296 : Sociologie </option><option value="Société">297 : Société </option><option value="Solfege">298 : Solfege </option><option value="Soul">299 : Soul </option><option value="Spectacle">300 : Spectacle </option><option value="Sport">301 : Sport </option><option value="Suisse">302 : Suisse </option><option value="Surnoms">303 : Surnoms </option><option value="Surréalisme">304 : Surréalisme </option><option value="Synthpop">305 : Synthpop </option><option value="Technique musicale">306 : Technique musicale </option><option value="Technologie">307 : Technologie </option><option value="Télévision">308 : Télévision </option><option value="Tennis">309 : Tennis </option><option value="Tennis de table">310 : Tennis de table </option><option value="Terroir">311 : Terroir </option><option value="TF1">312 : TF1 </option><option value="Thriller">313 : Thriller </option><option value="Théâtre">314 : Théâtre </option><option value="Tintin">315 : Tintin </option><option value="Train">316 : Train </option><option value="Transports">317 : Transports </option><option value="Turquie">318 : Turquie </option><option value="Urbanisme">319 : Urbanisme </option><option value="USA">320 : USA </option><option value="Variété">321 : Variété </option><option value="Ville">322 : Ville </option><option value="Volley-ball">323 : Volley-ball </option><option value="Western">324 : Western </option><option value="World music">325 : World music </option><option value="Xbox">326 : Xbox </option><option value="Zoologie">327 : Zoologie </option></select>
				
				<br/>
				
				<select class="lst" id="ddlTag4" data-placeholder="Catégorie 4"><option value=""> </option><option value="Acronyme">001 : Acronyme </option><option value="Action">002 : Action </option><option value="Affaires criminelles">003 : Affaires criminelles </option><option value="Afrique">004 : Afrique </option><option value="Agriculture">005 : Agriculture </option><option value="Alcool">006 : Alcool </option><option value="Allemagne">007 : Allemagne </option><option value="Alpinisme">008 : Alpinisme </option><option value="Amérique">009 : Amérique </option><option value="Amérique latine">010 : Amerique latine </option><option value="Amour">011 : Amour </option><option value="Anatomie">012 : Anatomie </option><option value="Animaux">013 : Animaux </option><option value="Anime">014 : Anime </option><option value="Années 1900">015 : Années 1900 </option><option value="Années 1910">016 : Années 1910 </option><option value="Années 1920">017 : Années 1920 </option><option value="Années 1930">018 : Années 1930 </option><option value="Années 1940">019 : Années 1940 </option><option value="Années 1950">020 : Années 1950 </option><option value="Années 1960">021 : Années 1960 </option><option value="Années 1970">022 : Années 1970 </option><option value="Années 1980">023 : Années 1980 </option><option value="Années 1990">024 : Années 1990 </option><option value="Années 2000">025 : Années 2000 </option><option value="Années 2010">026 : Années 2010 </option><option value="Années 2020">027 : Années 2020 </option><option value="Anthropologie">028 : Anthropologie </option><option value="Antiquité">029 : Antiquité </option><option value="Apple">030 : Apple </option><option value="Application">031 : Application </option><option value="Archéologie">032 : Archéologie </option><option value="Architecture">033 : Architecture </option><option value="Armes">034 : Armes </option><option value="Armée">035 : Armée </option><option value="Art contemporain">036 : Art contemporain </option><option value="Art moderne">037 : Art moderne </option><option value="Arts">038 : Arts </option><option value="Asie">039 : Asie </option><option value="Astronomie">040 : Astronomie </option><option value="Athlétisme">041 : Athlétisme </option><option value="Australie">042 : Australie </option><option value="Auto">043 : Auto </option><option value="Autriche">044 : Autriche </option><option value="Aventure">045 : Aventure </option><option value="Aviation">046 : Aviation </option><option value="Bande dessinée">047 : Bande dessinée </option><option value="Base ball">048 : Base ball </option><option value="Basket">049 : Basket </option><option value="Bataille">050 : Bataille </option><option value="Belgique">051 : Belgique </option><option value="Bien-être">052 : Bien-être </option><option value="Biographie">053 : Biographie </option><option value="Biologie">054 : Biologie </option><option value="Blues">055 : Blues </option><option value="Bonheur">056 : Bonheur </option><option value="Botanique">057 : Botanique </option><option value="Boxe">058 : Boxe </option><option value="Boys band">059 : Boys band </option><option value="Bretagne">060 : Bretagne </option><option value="Brésil">061 : Brésil </option><option value="Calcul mental">062 : Calcul mental </option><option value="Canada">063 : Canada </option><option value="Capitales">064 : Capitales </option><option value="Catastrophe">065 : Catastrophe </option><option value="Catholicisme">066 : Catholicisme </option><option value="Chanson">067 : Chanson </option><option value="Chanson francophone">068 : Chanson francophone </option><option value="Chefs d'état">069 : Chefs d'état </option><option value="Chimie">070 : Chimie </option><option value="Chine">071 : Chine </option><option value="Christianisme">072 : Christianisme </option><option value="Cinéma">073 : Cinéma </option><option value="Citations">074 : Citations </option><option value="Classique">075 : Classique </option><option value="Comédie">076 : Comédie </option><option value="Comédie dramatique">077 : Comédie dramatique </option><option value="Comédie musicale">078 : Comédie musicale </option><option value="Comédie romantique">079: Comédie romantique </option><option value="Comics">080 : Comics </option><option value="Conte">081 : Conte </option><option value="Couleurs">082 : Couleurs </option><option value="Country">083 : Country </option><option value="Cuba">084 : Cuba </option><option value="Cuisine">085 : Cuisine </option><option value="Culture">086 : Culture </option><option value="Culture geek">087 : Culture geek </option><option value="Culture générale">088 : Culture générale </option><option value="Cyclisme">089 : Cyclisme </option><option value="Danse">090 : Danse </option><option value="Déserts">091 : Déserts </option><option value="Dessin animé">092 : Dessin animé </option><option value="Disco">093 : Disco </option><option value="Disney">094 : Disney </option><option value="Drame">095 : Drame </option><option value="Drapeaux">096 : Drapeaux </option><option value="Droit">097 : Droit </option><option value="Droits de l'homme">098 : Droits de l'homme </option><option value="Ecologie">099 : Ecologie </option><option value="Economie">100 : Economie </option><option value="Education">101 : Education </option><option value="Egypte">102 : Egypte </option><option value="Electricité">103 : Electricité </option><option value="Electro">104 : Electro </option><option value="Energie">105 : Energie </option><option value="Enfance">106 : Enfance </option><option value="Epoque contemporaine">107 : Epoque contemporaine </option><option value="Epoque moderne">108 : Epoque moderne </option><option value="Espace">109 : Espace </option><option value="Espagne">110 : Espagne </option><option value="Etats-unis">111 : Etats-unis </option><option value="Etymologie">112 : Etymologie </option><option value="Eurodance">113 : Eurodance </option><option value="Europe">114 : Europe </option><option value="Europe de l'est">115 : Europe de l'est </option><option value="Europe du nord">116 : Europe du nord </option><option value="Explorateur">117 : Explorateur </option><option value="Expressions">118 : Expressions </option><option value="Fantastique">119 : Fantastique </option><option value="Femmes">120 : Femmes </option><option value="Fête">121 : Fête </option><option value="Film">122 : Film </option><option value="Film d'animation">123 : Film d'animation </option><option value="Film historique">124 : Film historique </option><option value="Finances">125 : Finances </option><option value="Folk">126 : Folk </option><option value="Football">127 : Football </option><option value="France">128 : France </option><option value="Fruit">129 : Fruit </option><option value="Funk">130 : Funk </option><option value="Gangsters">131 : Gangsters </option><option value="Géographie">132 : Géographie </option><option value="Geologie">133 : Geologie </option><option value="Grandes premières">134 : Grandes premières </option><option value="Grêce">135 : Grêce </option><option value="Grunge">136 : Grunge </option><option value="Guerre">137 : Guerre </option><option value="Guitare">138 : Guitare </option><option value="Gymnastique">139 : Gymnastique </option><option value="Handball">140 : Handball </option><option value="Hard rock">141 : Hard rock </option><option value="Harry potter">142 : Harry potter </option><option value="Heavy metal">143 : Heavy metal </option><option value="Hip hop">144 : Hip hop </option><option value="Histoire">145 : Histoire </option><option value="Hockey">146 : Hockey </option><option value="Horreur">147 : Horreur </option><option value="Humour">148 : Humour </option><option value="Hymnes">149 : Hymnes </option><option value="Iles">150 : Iles </option><option value="Inde">151 : Inde </option><option value="Industrie">152 : Industrie </option><option value="Informatique">153 : Informatique </option><option value="Insectes">154 : Insectes </option><option value="International">155 : International </option><option value="Internet">156 : Internet </option><option value="Inventions">157 : Inventions </option><option value="Irlande">158 : Irlande </option><option value="Israël">159 : Israël </option><option value="Italie">160 : Italie </option><option value="Japon">161 : Japon </option><option value="Jazz">162 : Jazz </option><option value="Jeu">163 : Jeu </option><option value="Jeunesse">164 : Jeunesse </option><option value="Jeux de rôle">166 : Jeux de rôle </option><option value="Jeux de société">167 : Jeux de société </option><option value="Jeux olympiques">168 : Jeux olympiques </option><option value="Jeux tv">169 : Jeux tv </option><option value="Jeux vidéo">170 : Jeux vidéo </option><option value="Journalisme">171 : Journalisme </option><option value="Judaïsme">172 : Judaïsme </option><option value="Judo">173 : Judo </option><option value="Langue française">174 : Langue française </option><option value="Langues">175 : Langues </option><option value="Légendes">176 : Légendes </option><option value="Light novel">177 : Light novel </option><option value="Linguistique">178 : Linguistique </option><option value="Littérature">179 : Littérature </option><option value="Logiciel">180 : Logiciel </option><option value="Loisirs">181 : Loisirs </option><option value="Maghreb">182 : Maghreb </option><option value="Manga">183 : Manga </option><option value="Marques">184 : Marques </option><option value="Mathematiques">185 : Mathematiques </option><option value="Médecine">186 : Médecine </option><option value="Mers et océans">187 : Mers et océans </option><option value="Metal">188 : Métal </option><option value="Météo">189 : Météo </option><option value="Militaire">190 : Militaire </option><option value="Mobile">191 : Mobile </option><option value="Mode">192 : Mode </option><option value="Monde arabo musulman">193 : Monde arabo musulman </option><option value="Monde celtique">194 : Monde celtique </option><option value="Monde équestre">195 : Monde équestre </option><option value="Monde turc">196 : Monde turc </option><option value="Monnaie">197 : Monnaie </option><option value="Montagne">198 : Montagne </option><option value="Motion twin">199 : Motion twin </option><option value="Moto">200 : Moto </option><option value="Moyen-age">201 : Moyen-age </option><option value="Muet">202 : Muet </option><option value="Musée">203 : Musée </option><option value="Musique">204 : Musique </option><option value="Musique électronique">205 : Musique électronique </option><option value="Musique française">206 : Musique française </option><option value="Musique francophone">207 : Musique francophone </option><option value="Mythologie">208 : Mythologie </option><option value="Mythologie grecque">209 : Mythologie grecque </option><option value="Mythologie nordique">210 : Mythologie nordique </option><option value="Mythologie romaine">211 : Mythologie romaine </option><option value="Mythologie égyptienne">212 : Mythologie égyptienne </option><option value="Médias">213 : Médias </option><option value="Métiers">214 : Métiers </option><option value="Nanar">215 : Nanar </option><option value="Napoleon">216 : Napoleon </option><option value="Natation">217 : Natation </option><option value="Nature">218 : Nature </option><option value="Navigation">219 : Navigation </option><option value="Neo metal">220 : Neo metal </option><option value="New wave">221 : New wave </option><option value="Nintendo">222 : Nintendo </option><option value="Noël">223 : Noël </option><option value="Noir et blanc">224 : Noir et blanc </option><option value="Nordique">225 : Nordique </option><option value="Nu metal">226 : Nu metal </option><option value="Océanie">227 : Océanie </option><option value="Oenologie">228 : Oenologie </option><option value="Oiseaux">229 : Oiseaux </option><option value="Opera">230 : Opera </option><option value="Optique">231 : Optique </option><option value="Outre mer">232 : Outre mer </option><option value="Paranormal">233 : Paranormal </option><option value="Paris">234 : Paris </option><option value="Patisserie">235 : Patisserie </option><option value="Pays-bas">236 : Pays-bas </option><option value="Peinture">237 : Peinture </option><option value="People">238 : People </option><option value="Peplum">239 : Peplum </option><option value="Peuples premiers">240 : Peuples premiers </option><option value="Philosophie">241 : Philosophie </option><option value="Photographie">242 : Photographie </option><option value="Physique">243 : Physique </option><option value="Pirates">244 : Pirates </option><option value="Playstation">245 : Playstation </option><option value="Poésie">246 : Poésie </option><option value="Polar">247 : Polar </option><option value="Policier">248 : Policier </option><option value="Politique">249 : Politique </option><option value="Pologne">250 : Pologne </option><option value="Pop">251 : Pop </option><option value="Pop rock">252 : Pop rock </option><option value="Portugal">253 : Portugal </option><option value="Préhistoire">254 : Préhistoire </option><option value="Président">255 : Président </option><option value="Presse">256 : Presse </option><option value="Prix">257 : Prix </option><option value="Provence">258 : Provence </option><option value="Proverbes">259 : Proverbes </option><option value="Publicités">260 : Publicités </option><option value="Punk">261 : Punk </option><option value="Quebec">262 : Quebec </option><option value="Radio">263 : Radio </option><option value="Rai">264 : Rai </option><option value="Rap">265 : Rap </option><option value="Reggae">266 : Reggae </option><option value="Régions">267 : Régions </option><option value="Religion">268 : Religion </option><option value="Renaissance">269 : Renaissance </option><option value="Révolution">270 : Révolution </option><option value="Rnb">271 : Rnb </option><option value="Rock">272 : Rock </option><option value="Rock alternatif">273 : Rock alternatif </option><option value="Rois et reines">274 : Rois et reines </option><option value="Romance">275 : Romance </option><option value="Roman historique">276 : Roman historique </option><option value="Royaumes-unis">277 : Royaumes-unis </option><option value="Rugby">278 : Rugby </option><option value="Russie">279 : Russie </option><option value="Russie 2018">280 : Russie 2018 </option><option value="Saga audio">281 : Saga audio </option><option value="Santé">282 : Santé </option><option value="Scandales">283 : Scandales </option><option value="Scandinavie">284 : Scandinavie </option><option value="Science-fiction">285 : Science-fiction </option><option value="Sciences">286 : Sciences </option><option value="Sculpture">287 : Sculpture </option><option value="Serbie">288 : Serbie </option><option value="Série dramatique">289 : Série dramatique </option><option value="Séries tv">290 : Séries tv </option><option value="Shakespeare">291 : Shakespeare </option><option value="Sigles">292 : Sigles </option><option value="Sitcom">293 : Sitcom </option><option value="Snapchat">294 : Snapchat </option><option value="Soap opera">295 : Soap opera </option><option value="Sociologie">296 : Sociologie </option><option value="Société">297 : Société </option><option value="Solfege">298 : Solfege </option><option value="Soul">299 : Soul </option><option value="Spectacle">300 : Spectacle </option><option value="Sport">301 : Sport </option><option value="Suisse">302 : Suisse </option><option value="Surnoms">303 : Surnoms </option><option value="Surréalisme">304 : Surréalisme </option><option value="Synthpop">305 : Synthpop </option><option value="Technique musicale">306 : Technique musicale </option><option value="Technologie">307 : Technologie </option><option value="Télévision">308 : Télévision </option><option value="Tennis">309 : Tennis </option><option value="Tennis de table">310 : Tennis de table </option><option value="Terroir">311 : Terroir </option><option value="TF1">312 : TF1 </option><option value="Thriller">313 : Thriller </option><option value="Théâtre">314 : Théâtre </option><option value="Tintin">315 : Tintin </option><option value="Train">316 : Train </option><option value="Transports">317 : Transports </option><option value="Turquie">318 : Turquie </option><option value="Urbanisme">319 : Urbanisme </option><option value="USA">320 : USA </option><option value="Variété">321 : Variété </option><option value="Ville">322 : Ville </option><option value="Volley-ball">323 : Volley-ball </option><option value="Western">324 : Western </option><option value="World music">325 : World music </option><option value="Xbox">326 : Xbox </option><option value="Zoologie">327 : Zoologie </option></select>
				
				<br/>
				
				<select class="lst" id="ddlTag5" data-placeholder="Catégorie 5"><option value=""> </option><option value="Acronyme">001 : Acronyme </option><option value="Action">002 : Action </option><option value="Affaires criminelles">003 : Affaires criminelles </option><option value="Afrique">004 : Afrique </option><option value="Agriculture">005 : Agriculture </option><option value="Alcool">006 : Alcool </option><option value="Allemagne">007 : Allemagne </option><option value="Alpinisme">008 : Alpinisme </option><option value="Amérique">009 : Amérique </option><option value="Amérique latine">010 : Amerique latine </option><option value="Amour">011 : Amour </option><option value="Anatomie">012 : Anatomie </option><option value="Animaux">013 : Animaux </option><option value="Anime">014 : Anime </option><option value="Années 1900">015 : Années 1900 </option><option value="Années 1910">016 : Années 1910 </option><option value="Années 1920">017 : Années 1920 </option><option value="Années 1930">018 : Années 1930 </option><option value="Années 1940">019 : Années 1940 </option><option value="Années 1950">020 : Années 1950 </option><option value="Années 1960">021 : Années 1960 </option><option value="Années 1970">022 : Années 1970 </option><option value="Années 1980">023 : Années 1980 </option><option value="Années 1990">024 : Années 1990 </option><option value="Années 2000">025 : Années 2000 </option><option value="Années 2010">026 : Années 2010 </option><option value="Années 2020">027 : Années 2020 </option><option value="Anthropologie">028 : Anthropologie </option><option value="Antiquité">029 : Antiquité </option><option value="Apple">030 : Apple </option><option value="Application">031 : Application </option><option value="Archéologie">032 : Archéologie </option><option value="Architecture">033 : Architecture </option><option value="Armes">034 : Armes </option><option value="Armée">035 : Armée </option><option value="Art contemporain">036 : Art contemporain </option><option value="Art moderne">037 : Art moderne </option><option value="Arts">038 : Arts </option><option value="Asie">039 : Asie </option><option value="Astronomie">040 : Astronomie </option><option value="Athlétisme">041 : Athlétisme </option><option value="Australie">042 : Australie </option><option value="Auto">043 : Auto </option><option value="Autriche">044 : Autriche </option><option value="Aventure">045 : Aventure </option><option value="Aviation">046 : Aviation </option><option value="Bande dessinée">047 : Bande dessinée </option><option value="Base ball">048 : Base ball </option><option value="Basket">049 : Basket </option><option value="Bataille">050 : Bataille </option><option value="Belgique">051 : Belgique </option><option value="Bien-être">052 : Bien-être </option><option value="Biographie">053 : Biographie </option><option value="Biologie">054 : Biologie </option><option value="Blues">055 : Blues </option><option value="Bonheur">056 : Bonheur </option><option value="Botanique">057 : Botanique </option><option value="Boxe">058 : Boxe </option><option value="Boys band">059 : Boys band </option><option value="Bretagne">060 : Bretagne </option><option value="Brésil">061 : Brésil </option><option value="Calcul mental">062 : Calcul mental </option><option value="Canada">063 : Canada </option><option value="Capitales">064 : Capitales </option><option value="Catastrophe">065 : Catastrophe </option><option value="Catholicisme">066 : Catholicisme </option><option value="Chanson">067 : Chanson </option><option value="Chanson francophone">068 : Chanson francophone </option><option value="Chefs d'état">069 : Chefs d'état </option><option value="Chimie">070 : Chimie </option><option value="Chine">071 : Chine </option><option value="Christianisme">072 : Christianisme </option><option value="Cinéma">073 : Cinéma </option><option value="Citations">074 : Citations </option><option value="Classique">075 : Classique </option><option value="Comédie">076 : Comédie </option><option value="Comédie dramatique">077 : Comédie dramatique </option><option value="Comédie musicale">078 : Comédie musicale </option><option value="Comédie romantique">079: Comédie romantique </option><option value="Comics">080 : Comics </option><option value="Conte">081 : Conte </option><option value="Couleurs">082 : Couleurs </option><option value="Country">083 : Country </option><option value="Cuba">084 : Cuba </option><option value="Cuisine">085 : Cuisine </option><option value="Culture">086 : Culture </option><option value="Culture geek">087 : Culture geek </option><option value="Culture générale">088 : Culture générale </option><option value="Cyclisme">089 : Cyclisme </option><option value="Danse">090 : Danse </option><option value="Déserts">091 : Déserts </option><option value="Dessin animé">092 : Dessin animé </option><option value="Disco">093 : Disco </option><option value="Disney">094 : Disney </option><option value="Drame">095 : Drame </option><option value="Drapeaux">096 : Drapeaux </option><option value="Droit">097 : Droit </option><option value="Droits de l'homme">098 : Droits de l'homme </option><option value="Ecologie">099 : Ecologie </option><option value="Economie">100 : Economie </option><option value="Education">101 : Education </option><option value="Egypte">102 : Egypte </option><option value="Electricité">103 : Electricité </option><option value="Electro">104 : Electro </option><option value="Energie">105 : Energie </option><option value="Enfance">106 : Enfance </option><option value="Epoque contemporaine">107 : Epoque contemporaine </option><option value="Epoque moderne">108 : Epoque moderne </option><option value="Espace">109 : Espace </option><option value="Espagne">110 : Espagne </option><option value="Etats-unis">111 : Etats-unis </option><option value="Etymologie">112 : Etymologie </option><option value="Eurodance">113 : Eurodance </option><option value="Europe">114 : Europe </option><option value="Europe de l'est">115 : Europe de l'est </option><option value="Europe du nord">116 : Europe du nord </option><option value="Explorateur">117 : Explorateur </option><option value="Expressions">118 : Expressions </option><option value="Fantastique">119 : Fantastique </option><option value="Femmes">120 : Femmes </option><option value="Fête">121 : Fête </option><option value="Film">122 : Film </option><option value="Film d'animation">123 : Film d'animation </option><option value="Film historique">124 : Film historique </option><option value="Finances">125 : Finances </option><option value="Folk">126 : Folk </option><option value="Football">127 : Football </option><option value="France">128 : France </option><option value="Fruit">129 : Fruit </option><option value="Funk">130 : Funk </option><option value="Gangsters">131 : Gangsters </option><option value="Géographie">132 : Géographie </option><option value="Geologie">133 : Geologie </option><option value="Grandes premières">134 : Grandes premières </option><option value="Grêce">135 : Grêce </option><option value="Grunge">136 : Grunge </option><option value="Guerre">137 : Guerre </option><option value="Guitare">138 : Guitare </option><option value="Gymnastique">139 : Gymnastique </option><option value="Handball">140 : Handball </option><option value="Hard rock">141 : Hard rock </option><option value="Harry potter">142 : Harry potter </option><option value="Heavy metal">143 : Heavy metal </option><option value="Hip hop">144 : Hip hop </option><option value="Histoire">145 : Histoire </option><option value="Hockey">146 : Hockey </option><option value="Horreur">147 : Horreur </option><option value="Humour">148 : Humour </option><option value="Hymnes">149 : Hymnes </option><option value="Iles">150 : Iles </option><option value="Inde">151 : Inde </option><option value="Industrie">152 : Industrie </option><option value="Informatique">153 : Informatique </option><option value="Insectes">154 : Insectes </option><option value="International">155 : International </option><option value="Internet">156 : Internet </option><option value="Inventions">157 : Inventions </option><option value="Irlande">158 : Irlande </option><option value="Israël">159 : Israël </option><option value="Italie">160 : Italie </option><option value="Japon">161 : Japon </option><option value="Jazz">162 : Jazz </option><option value="Jeu">163 : Jeu </option><option value="Jeunesse">164 : Jeunesse </option><option value="Jeux de rôle">166 : Jeux de rôle </option><option value="Jeux de société">167 : Jeux de société </option><option value="Jeux olympiques">168 : Jeux olympiques </option><option value="Jeux tv">169 : Jeux tv </option><option value="Jeux vidéo">170 : Jeux vidéo </option><option value="Journalisme">171 : Journalisme </option><option value="Judaïsme">172 : Judaïsme </option><option value="Judo">173 : Judo </option><option value="Langue française">174 : Langue française </option><option value="Langues">175 : Langues </option><option value="Légendes">176 : Légendes </option><option value="Light novel">177 : Light novel </option><option value="Linguistique">178 : Linguistique </option><option value="Littérature">179 : Littérature </option><option value="Logiciel">180 : Logiciel </option><option value="Loisirs">181 : Loisirs </option><option value="Maghreb">182 : Maghreb </option><option value="Manga">183 : Manga </option><option value="Marques">184 : Marques </option><option value="Mathematiques">185 : Mathematiques </option><option value="Médecine">186 : Médecine </option><option value="Mers et océans">187 : Mers et océans </option><option value="Metal">188 : Métal </option><option value="Météo">189 : Météo </option><option value="Militaire">190 : Militaire </option><option value="Mobile">191 : Mobile </option><option value="Mode">192 : Mode </option><option value="Monde arabo musulman">193 : Monde arabo musulman </option><option value="Monde celtique">194 : Monde celtique </option><option value="Monde équestre">195 : Monde équestre </option><option value="Monde turc">196 : Monde turc </option><option value="Monnaie">197 : Monnaie </option><option value="Montagne">198 : Montagne </option><option value="Motion twin">199 : Motion twin </option><option value="Moto">200 : Moto </option><option value="Moyen-age">201 : Moyen-age </option><option value="Muet">202 : Muet </option><option value="Musée">203 : Musée </option><option value="Musique">204 : Musique </option><option value="Musique électronique">205 : Musique électronique </option><option value="Musique française">206 : Musique française </option><option value="Musique francophone">207 : Musique francophone </option><option value="Mythologie">208 : Mythologie </option><option value="Mythologie grecque">209 : Mythologie grecque </option><option value="Mythologie nordique">210 : Mythologie nordique </option><option value="Mythologie romaine">211 : Mythologie romaine </option><option value="Mythologie égyptienne">212 : Mythologie égyptienne </option><option value="Médias">213 : Médias </option><option value="Métiers">214 : Métiers </option><option value="Nanar">215 : Nanar </option><option value="Napoleon">216 : Napoleon </option><option value="Natation">217 : Natation </option><option value="Nature">218 : Nature </option><option value="Navigation">219 : Navigation </option><option value="Neo metal">220 : Neo metal </option><option value="New wave">221 : New wave </option><option value="Nintendo">222 : Nintendo </option><option value="Noël">223 : Noël </option><option value="Noir et blanc">224 : Noir et blanc </option><option value="Nordique">225 : Nordique </option><option value="Nu metal">226 : Nu metal </option><option value="Océanie">227 : Océanie </option><option value="Oenologie">228 : Oenologie </option><option value="Oiseaux">229 : Oiseaux </option><option value="Opera">230 : Opera </option><option value="Optique">231 : Optique </option><option value="Outre mer">232 : Outre mer </option><option value="Paranormal">233 : Paranormal </option><option value="Paris">234 : Paris </option><option value="Patisserie">235 : Patisserie </option><option value="Pays-bas">236 : Pays-bas </option><option value="Peinture">237 : Peinture </option><option value="People">238 : People </option><option value="Peplum">239 : Peplum </option><option value="Peuples premiers">240 : Peuples premiers </option><option value="Philosophie">241 : Philosophie </option><option value="Photographie">242 : Photographie </option><option value="Physique">243 : Physique </option><option value="Pirates">244 : Pirates </option><option value="Playstation">245 : Playstation </option><option value="Poésie">246 : Poésie </option><option value="Polar">247 : Polar </option><option value="Policier">248 : Policier </option><option value="Politique">249 : Politique </option><option value="Pologne">250 : Pologne </option><option value="Pop">251 : Pop </option><option value="Pop rock">252 : Pop rock </option><option value="Portugal">253 : Portugal </option><option value="Préhistoire">254 : Préhistoire </option><option value="Président">255 : Président </option><option value="Presse">256 : Presse </option><option value="Prix">257 : Prix </option><option value="Provence">258 : Provence </option><option value="Proverbes">259 : Proverbes </option><option value="Publicités">260 : Publicités </option><option value="Punk">261 : Punk </option><option value="Quebec">262 : Quebec </option><option value="Radio">263 : Radio </option><option value="Rai">264 : Rai </option><option value="Rap">265 : Rap </option><option value="Reggae">266 : Reggae </option><option value="Régions">267 : Régions </option><option value="Religion">268 : Religion </option><option value="Renaissance">269 : Renaissance </option><option value="Révolution">270 : Révolution </option><option value="Rnb">271 : Rnb </option><option value="Rock">272 : Rock </option><option value="Rock alternatif">273 : Rock alternatif </option><option value="Rois et reines">274 : Rois et reines </option><option value="Romance">275 : Romance </option><option value="Roman historique">276 : Roman historique </option><option value="Royaumes-unis">277 : Royaumes-unis </option><option value="Rugby">278 : Rugby </option><option value="Russie">279 : Russie </option><option value="Russie 2018">280 : Russie 2018 </option><option value="Saga audio">281 : Saga audio </option><option value="Santé">282 : Santé </option><option value="Scandales">283 : Scandales </option><option value="Scandinavie">284 : Scandinavie </option><option value="Science-fiction">285 : Science-fiction </option><option value="Sciences">286 : Sciences </option><option value="Sculpture">287 : Sculpture </option><option value="Serbie">288 : Serbie </option><option value="Série dramatique">289 : Série dramatique </option><option value="Séries tv">290 : Séries tv </option><option value="Shakespeare">291 : Shakespeare </option><option value="Sigles">292 : Sigles </option><option value="Sitcom">293 : Sitcom </option><option value="Snapchat">294 : Snapchat </option><option value="Soap opera">295 : Soap opera </option><option value="Sociologie">296 : Sociologie </option><option value="Société">297 : Société </option><option value="Solfege">298 : Solfege </option><option value="Soul">299 : Soul </option><option value="Spectacle">300 : Spectacle </option><option value="Sport">301 : Sport </option><option value="Suisse">302 : Suisse </option><option value="Surnoms">303 : Surnoms </option><option value="Surréalisme">304 : Surréalisme </option><option value="Synthpop">305 : Synthpop </option><option value="Technique musicale">306 : Technique musicale </option><option value="Technologie">307 : Technologie </option><option value="Télévision">308 : Télévision </option><option value="Tennis">309 : Tennis </option><option value="Tennis de table">310 : Tennis de table </option><option value="Terroir">311 : Terroir </option><option value="TF1">312 : TF1 </option><option value="Thriller">313 : Thriller </option><option value="Théâtre">314 : Théâtre </option><option value="Tintin">315 : Tintin </option><option value="Train">316 : Train </option><option value="Transports">317 : Transports </option><option value="Turquie">318 : Turquie </option><option value="Urbanisme">319 : Urbanisme </option><option value="USA">320 : USA </option><option value="Variété">321 : Variété </option><option value="Ville">322 : Ville </option><option value="Volley-ball">323 : Volley-ball </option><option value="Western">324 : Western </option><option value="World music">325 : World music </option><option value="Xbox">326 : Xbox </option><option value="Zoologie">327 : Zoologie </option></select>
	
				</td></tr>
	</table>
	<br/>
	<input type="button" value="Envoyer la question dans la base officielle" id="validerFULL" />
				
	</div>
	
	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>