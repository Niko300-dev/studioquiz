<?php 
//session cross to sub domain
ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

?>
<!DOCTYPE html>
<html lang="fr"><!-- Basic -->
<head>
	<base href="/">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
     <!-- Site Metas -->
    <title>Directquiz - Faire un don</title>
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />	
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

		<!-- ALL JS FILES -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	
	
	
	<script>
	
	$(function(){
		
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture").html("<img class='ceinture-profil' src='images/ceinture_"+result[0].trim()+".png' />");
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
							$("#XP").text(result[2].trim());
							
																				
							$("#profil-thune-cadre").html("Fortune : " + result[3].trim()+" <img class='piecetteG' title='DirectDollar' src='images/dd.png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
		
	});
		
	</script>
	
</head>
<body id="contact_page" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

	<!-- LOADER -->
    <div id="preloader">
		<div class="loader">
			<img src="images/loader.gif" alt="#"/>
		</div>
    </div><!-- end loader -->
    <!-- END LOADER -->
	
	<!-- Start header -->
	<header class="top-header">
		<nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
				<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
					<span></span>
					<span></span>
					<span></span>
				</button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
                        <li><a class="nav-link" href="about.php">A propos</a></li>
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
						<li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					  
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
						 <li><a class="nav-link active" href="dons.php">Faire un don</a></li>
                    </ul>
                </div>
				<div class="search-box">

	             </div>
            </div>
        </nav>
	</header>
	<!-- End header -->

	    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">FAIRE</span> UN DON</h2>
                            <p class="large">Pour soutenir le projet</p>
											
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 white_fonts">

<iframe id="donFrame"
    title="Faire un don"
    width="1100"
    height="1450"
    src="https://fr.tipeee.com/directquiz">
</iframe>
				
                </div>
            </div>
        </div>
    </div>
    <!-- end section -->
	
	
	<?php 
	
		include('footer.php');
	
	?>


	
	<a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

	


	

    <!-- ALL PLUGINS -->
	<script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script> 
	<script src="js/slider-index.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
	<script src="js/isotope.min.js"></script>	
	<script src="js/images-loded.min.js"></script>	
    <script src="js/custom.js"></script>
</body>
</html>