
	

	<script>

		function fermerMentions()
		{
			
			$("#mention").hide();
			
		}
		
		function ouvrirMentions()
		{
			
			$("#mention").show();
			
		}
		
		if (window.location.href.indexOf("https://www.") == -1)
		{
			window.location = window.location.href.replace("https://","https://www.");
		}
		
		
	</script>
	
	<script src="https://unpkg.com/magic-snowflakes/dist/snowflakes.min.js"></script>
    <script>
        
		/*** POUR L'HIVER SEULEMENT ***/
			
		/*	
			
			var snowflakes = new Snowflakes({
				color: '#5ECDEF', // Default: "#5ECDEF"
			});

			
			$(function(){
				
				$('head').append('<link rel="stylesheet" href="css/hiver.css">');
				if (location.href.indexOf("jouer.php") > -1 ||
					location.href.indexOf("megaquiz.php") > -1 || 
					location.href.indexOf("spectacle.php") > -1 || 
					location.href.indexOf("jeuxvideo.php") > -1 || 
					location.href.indexOf("histoire.php") > -1 || 
					location.href.indexOf("sport.php") > -1 || 
					location.href.indexOf("geo.php") > -1 || 
					location.href.indexOf("informatique.php") > -1 || 
					location.href.indexOf("noel.php") > -1)
					{
						$('.footer-box').css("background-image","url('images/surfaceNeigeBis.png')");
						$('.footer-box').css("background-repeat","repeat-x");
					}
				
			});
		
		*/
			
		/*** ====================== ***/
		
    </script>
	
    <!-- Start Footer -->
    <footer class="footer-box">
	
        <div class="container footerContainer">
            <div class="row">
                <div class="col-lg-12">
                    <div class="logo">
                        <a href="index.php"><img src="images/footer_logo.png" alt="#" /></a>
                    </div>
                </div>
                <div class="col-lg-12 white_fonts">
                    <h4 class="text-align">D'après une idée de <img alt="Motion Twin" src="images/mt.png"></h4>
                </div>

            </div>
            <div class="row white_fonts margin-top_30">
                <div class="col-lg-12">
                    <a target="_BLANK" href="https://discord.com/invite/ERc3svy"><img width="32" height="32" src="images/discord.png" alt="discord"></a>                 
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="crp">© 2022 DIRECTQUIZ. Tous droits réservés.</p>
                    <ul class="bottom_menu">
                        <li><a onclick="ouvrirMentions()" href="#">Mentions légales</a></li>
						 <li><a href="https://eternal-twin.net/" target="_BLANK">Eternal Twin</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	
	<div id="mention" style="display:none;">
				<?php include('mention.php'); ?>
	</div>
	
	