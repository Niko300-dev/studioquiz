const {getLocalConfigSync} = require("./config.js");
const http = require('http');
const https = require('https');
const fs = require('fs');
const Discord = require('discord.js');
const linkify = require('linkifyjs');
const linkifyString = require('linkify-string');
const { Configuration, OpenAIApi } = require("openai");

const textToSpeech = require('@google-cloud/text-to-speech');
const {Translate} = require('@google-cloud/translate').v2;
const util = require('util');
const ftp = require("basic-ftp");
const { v4: uuidv4 } = require('uuid');

const Client = new Discord.Client;
const CONFIG = getLocalConfigSync();

// Creates a client to speech
const clientSpeech = new textToSpeech.TextToSpeechClient({credentials: CONFIG.googleapis});
const translate = new Translate({credentials:CONFIG.googleapis2});
	
const EASY = 1;
const MEDIUM = 2;
const HARD = 3;

/* ############   ( PROPRE AU SALON )   ############ */

const CATEGORY_PAGE = "informatique";
const CATEGORY = "INFORMATIQUE";
const CATEGORY_ID = 6;
const PORT_ECOUTE = 3005;

/* ############ ___ PROPRE AU SALON __ ############ */

var lastEmprunteWritingCurrent = "";

var echellePoints = [10,5,2,2];
var niveauDifficulte = MEDIUM;

// Les TIMEOUT
var timeoutDebutPartie = null;
var timeoutFinQuestion = null;
var timeoutQuizFinish = null;
var timeoutQuizContinue = null;
var timeoutAnnonceCandidats = null;
var timeoutAnnonceQuestion = null;
var timeoutNextQuestion = null;
var timeoutRefreshTopMessage = null;

// Last Socket Globale
var lastSocket = null;

// Les personnes en train d'écrire
var currentWritingList = new Array(0);

// Bots
var conversationIABots = [];

conversationIABots.push([]);
conversationIABots.push([]);
conversationIABots.push([]);


Client.on("ready", () => {
	
	console.log("bot opérationnel");
	//Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send("Bot Directquiz initialisé sur le salon "+ CATEGORY +" :white_check_mark:");
	console.log("Bot Directquiz initialisé sur le salon "+ CATEGORY);
	// Thread Discord
	// Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).threads.cache.get("872963458382053418").send("Bot Directquiz initialisé sur le salon "+ CATEGORY +" :white_check_mark:");	
	
	
	
});

function deleteIfBot(message, channelCurrent)
{
	if (message.author.id == "845315680411582493") channelCurrent.messages.delete(message);				
}

function deleteMessage(message, channelCurrent)
{
	channelCurrent.messages.delete(message);
}

Client.login(CONFIG.discord.token);

function createServer(app) {
	const keyPath = CONFIG?.https?.key;
	const certPath = CONFIG?.https?.cert;
	if (typeof keyPath === "string" && typeof certPath === "string") {
		const key = fs.readFileSync(keyPath);
		const cert = fs.readFileSync(certPath);
		return https.createServer({ key, cert }, app);
	} else {
		return http.createServer(app);
	}
}


var express = require('express');
const axios = require('axios');
var mp3Duration = require('mp3-duration');
const MINUTES_TIMEOUT = 160;
var app = express();
const server1 = createServer(app);
var server = server1.listen(PORT_ECOUTE);

var socket = require('socket.io');
const credentials = require("./config");

var io = socket(server);

function refreshListeSlotsInitial()
{
		var listeSlots = new Array(0);
		for (i = 0; i < listeDesMembres.length; i++)
		{
			listeSlots.push(listeDesMembresAvecDetail[i]); 
		}
			
		io.sockets.emit('slots', listeSlots);	
}

Client.on("message", messageCommande => {
	
	const channelCurrent = messageCommande.channel; //Client.channels.cache.get(messageCommande.channel.id);
	
	if (messageCommande.content == "!cleardqn")
	{	
		deleteMessage(messageCommande, channelCurrent);
		channelCurrent.messages.fetch({ limit: 100 }).then(messages => {
		messages.forEach(message => deleteIfBot(message, channelCurrent));

		});
	}
	else if(messageCommande.content.indexOf("!sendDirectquiz ") == 0)
	{
		var messageIsole = messageCommande.content.replace("!sendDirectquiz ","");
		deleteMessage(messageCommande, channelCurrent);
		
			if (CATEGORY_ID == 8)
				io.sockets.emit('annonce',{type:111,annonce: "<i><u><strong>[Message send from DISCORD] :</strong></u></i> <br/> <img class='microavatar' src='"+messageCommande.author.avatarURL()+"' /> <strong>"+messageCommande.author.username+"</strong> : " + messageIsole.replace(regexHTML,"")  });	
			else
				io.sockets.emit('annonce',{type:111,annonce: "<i><u><strong>[Message envoyé depuis DISCORD] :</strong></u></i> <br/> <img class='microavatar' src='"+messageCommande.author.avatarURL()+"' /> <strong>"+messageCommande.author.username+"</strong> : " + messageIsole.replace(regexHTML,"")  });	
	}

});


const regexHTML = /(<([^>]+)>)/ig;

app.use(express.static('public'));

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://www.directquiz.org');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

console.log("Le serveur est effectif !");

var cptQuestion = -1;
var cptReponseDonnee = 0;

var messageTitre = "";

io.sockets.on('connection', newConnection);

io.use(function(socket, next) {
  var handshakeData = socket.request;
  console.log("TOKEN : ", handshakeData._query['token']);
  next();
});

var identifiantPartie = "";

var lastAnnonce = "";
var lastBlague = "";

var listeDesMembres = new Array(0);
var listeDesMembresOffline = new Array(0);
var listeDesMembresAvecDetailOffline = new Array(0);
var listeDesMembresAvecDetail = new Array(0);
var listeDesMembresRestant = new Array(0);
var countMember = 0;
var allClients = new Array(0);
var listeRepondantCorrect = new Array(0);
var listeQuestions = new Array(0);
var listeIDQuestions = new Array(0);
var listeIDReelQuestions = new Array(0);

var bot1OK = false;
var bot2OK = false;
var bot3OK = false;

var chrono = null;
var chronoPhHasard = null;
var chronoPhSortieHasard = null;

var chronoDelaiQuestion = null;
var chronoDelairPhraseHasard = null;

var isStart = false;
var isFinish = false;
var isFirstFinish = false;
var listeBlagues = new Array(0);
var listePhrasesInutiles = new Array(0);
var listePhrasesSortie = new Array(0);
var timeout = false;
var globalChronoQuestion = 40;
var timerSecondes = null;

var phrases5secondesRestantes = new Array(0);
var phrasesAttente = new Array(0);

var insultes = new Array(0);

var listePointsWin = new Array(0);

var conversationIA = [];
var positionAttenteGPT = 1;

var enPleinQuiz = false;

var currentGPTProcess = false;
var currentPhraseHasardProcess = false;

var isBlague = false;

var currentNumberOfBot = 0;

const NOTIFY_CONNECT = 1;
const NOTIFY_DISCONNECT = 2;

 insultes.push("CUL");
 insultes.push("BITE");
 insultes.push("CHIER");
 insultes.push("PUTAIN");
 insultes.push("MERDE"); 
 insultes.push("MERDEUX");
 insultes.push("CONNARD");
 insultes.push("CONNASSE");
 insultes.push("CON");
 insultes.push("CONS");
 insultes.push("FILS DE PUTE");
 insultes.push("SALOPE");
 insultes.push("ENCULES");
 insultes.push("ENCULE");
 insultes.push("ENCULER");
 insultes.push("TA MERE LA PUTE");
 insultes.push("FILS DE CHIEN");
 insultes.push("FOUTRE");
 insultes.push("PUTE");
 insultes.push("PUTTE");
 insultes.push("SALOPE");
 insultes.push("ABRUTI");
 insultes.push("SAPERLIPOPETTE");
 insultes.push("MORVEUX");
 insultes.push("CRETIN");
 insultes.push("IMBECILE");
 insultes.push("FUCK");
 insultes.push("MOTHER FUCKER");
 insultes.push("COUILLES");
 insultes.push("COUILLE");
 insultes.push("ZIZI");
 insultes.push("ZEZETTE");
 insultes.push("KIKINE");

var fileName = "noname.mp3";

		const configuration = new Configuration({
		  apiKey: CONFIG.openai.api_key
		});

		const openai = new OpenAIApi(configuration);

init(true);

function gptMessage(role, content) {
    this.role = role;
    this.content = content;
}

async function processLang(str)
{
	
	if (CATEGORY_ID == 8 || CATEGORY_ID == 9)
	{
		/*
			const completion = await openai.createChatCompletion({
				  model: "gpt-3.5-turbo",
				  messages: [new gptMessage("user", "traduis-moi cette phrase en anglais en ne donnant que la traduction et rien d'autre : \"" + str + "\"")]
			});
		
			let traduction = completion.data.choices[0].message.content.replace(/\."/g, '').replace(/"/g, '');
			
			return traduction;
		*/
		return await traduire(str,"");
	}
	else
	{
		return str;
		
		// return traduction;
	}	
	
}

async function requestDalle(requestPseudo, descriptionFR)
{
	
	if (!enPleinQuiz)
	{	

		let descriptionClean = descriptionFR.replace(/Montre-moi /,"").replace(/montre-moi /,"").replace(/Show me /,"").replace(/show me /,"").replace(/Laat me /,"").replace(/laat me /,"");
				
		let generatedIAResponse = await traduire(descriptionClean,"en");
		
		const response = await openai.createImage({
		  prompt: generatedIAResponse,
		  n: 1,
		  size: "256x256",
		});
		
		let image_url = response.data.data[0].url;
		
		if (CATEGORY_ID != 8)
		{
			conversationIA.push(new gptMessage("assistant","Voici une image qui représente "+descriptionClean));
		}
		else if (CATEGORY_ID == 9)
		{
			conversationIA.push(new gptMessage("assistant","Dit is een afbeelding die "+descriptionClean+" voorstelt"));
		}
		else if (CATEGORY_ID == 8)
		{
			conversationIA.push(new gptMessage("assistant","Look at this picture, it's "+descriptionClean));
		}
		
		if (CATEGORY_ID != 8)
		{
			speech("D'accord "+requestPseudo+" ! Voici ce que je peux vous montrer : ");
			io.sockets.emit('annonce',{type:98,annonce:"D'accord "+requestPseudo+" ! Voici ce que je peux vous montrer : "});
			io.sockets.emit('annonce',{type:982,annonce:"<img src='"+image_url+"' alt='image générée' />"});
		}
		else if (CATEGORY_ID == 9)
		{	
			speech("Oké "+requestPseudo+" ! Dit is wat ik je kan laten zien : ");
			io.sockets.emit('annonce',{type:98,annonce:"Oké "+requestPseudo+" ! Dit is wat ik je kan laten zien : "});
			io.sockets.emit('annonce',{type:982,annonce:"<img src='"+image_url+"' alt='picture generated' />"});
		}	
		else if (CATEGORY_ID == 8)
		{	
			speech("Right "+requestPseudo+" ! That is I can show you : ");
			io.sockets.emit('annonce',{type:98,annonce:"Right "+requestPseudo+" ! That is I can show you : "});
			io.sockets.emit('annonce',{type:982,annonce:"<img src='"+image_url+"' alt='picture generated' />"});
		}	




	}
	else
	{
		var excuseMontrerImage3 = ((CATEGORY_ID != 8 && CATEGORY_ID != 9)?requestPseudo+" ,les représentation visuelle ne peuvent être montrée qu'en dehors d'un quiz !":requestPseudo+" ,the visual representations can be show only out of a quiz !");
						
		io.sockets.emit('annonce',{type:7, annonce:excuseMontrerImage3 });
	}
	
}

async function requestGPTByBOT(speaker, requestToBot, idBot)
{
	
			var baseConversationIABot = [];
			baseConversationIABot.push(new gptMessage("system","Tu es un candidat à l'émission Directquiz et tu te nommes BOT_"+idBot+", tu dois répondre les nombres avec des chiffres, les candidats présents sur le plateau sont : "+listeCandidatsLangageNaturel()+" et le dernier classement est le suivant : "+classementEnFR()+(enPleinQuiz?", un quiz a commencé et est actuellement en cours.":", il n'y a aucun quiz commencé ou en cours.")));
				
			if (CATEGORY_ID == 8) //EN
			{
				baseConversationIABot.push(new gptMessage("system","Tu dois obligatoirement répondre en anglais et pas dans une autre langue !"));
			}
			else if (CATEGORY_ID == 9) //NL
			{
				baseConversationIABot.push(new gptMessage("system","Tu dois obligatoirement répondre en néerlandais et pas dans une autre langue !"));
			}
			else //FR
			{
				baseConversationIABot.push(new gptMessage("system","Tu dois obligatoirement répondre en français et pas dans une autre langue !"));
			}
			
			var strConstitue = "";
			
			if (!enPleinQuiz)
			{
				if (CATEGORY_ID != 8)
					strConstitue = speaker + " a dit \""+requestToBot+"\", réponds-lui en citant son nom et ce que tu dis ne doit pas dépasser une vingtaine de mots.";
				else
					strConstitue = speaker + " said \""+requestToBot+"\", respond to him by mentioning his name and your statement should not exceed twenty words.";
			}
			else
			{

				if (speaker == "###PRESENTATOR###")
				{
					if (botDonneBonneReponse())
					{
						if (CATEGORY_ID != 8)
							strConstitue = "réponds à la question suivante : \""+requestToBot+"\" en disant juste le texte suivant et rien d'autre : "+listeQuestions[cptQuestion].reponse.split("|")[0];
						else
							strConstitue = "respond to the following question : \""+requestToBot+"\" saying just the following text and nothing else : "+listeQuestions[cptQuestion].reponse.split("|")[0];
					}
					else
					{
						if (CATEGORY_ID != 8)
							strConstitue = "réponds une mauvaise réponse à la question suivante : \""+requestToBot+"\" et ta réponse doit être la plus courte possible !";
						else
							strConstitue = "respond a wrong answer to the following question : \""+requestToBot+"\" and your response have to be the shorter available !";
					}

				}
				else
				{
					if (CATEGORY_ID != 8)
						strConstitue = speaker + " a dit \"" + requestToBot + "\", réponds-lui en citant son nom mais ta réponse ne peut pas contenir un de ces mots : \""+listeQuestions[cptQuestion].reponse+"\" et ce que tu dis ne doit pas dépasser une dizaine de mots.";
					else
						strConstitue = speaker + " said \"" + requestToBot + "\", respond to him by mentioning his name, but you cannot respond to this words : \""+listeQuestions[cptQuestion].reponse+"\" and your statement should not exceed ten words.";
				}
	
			}
			
			if (speaker == "###PRESENTATOR###" && bot1OK && idBot == 1) strConstitue = "ne rien dire";
			else if (speaker == "###PRESENTATOR###" && bot2OK && idBot == 2) strConstitue = "ne rien dire";
			else if (speaker == "###PRESENTATOR###" && bot3OK && idBot == 3) strConstitue = "ne rien dire";
		
		
			if (strConstitue != "ne rien dire")
			{
				conversationIABots[idBot-1].push(new gptMessage("user",strConstitue));
				baseConversationIABot.push(...conversationIABots[idBot-1]);
				const completion = await openai.createChatCompletion({
				  model: "gpt-3.5-turbo-0613",
				  messages: baseConversationIABot
				});		
				let generatedIAResponse = completion.data.choices[0].message.content.replace(/"/g, '').replace(/\(e\)/g,'');
				
				if (CATEGORY_ID != 8)
				{			
						generatedIAResponse = generatedIAResponse.replace(/ qu'IA/g, " que candidat").replace(/ une IA/g, " un candidat").replace(/ modèle de langage/g, " candidat");
				}

				if (speaker == "###PRESENTATOR###") generatedIAResponse = generatedIAResponse.replace(/\./g, "");

				messageGetByBot({
							id: "bot"+idBot,
							pseudo: "<span class='pseudoBot bot"+idBot+"'>BOT_"+idBot+"</span>",
							message: generatedIAResponse,
							ia: false
						});

				conversationIABots[idBot-1].push(new gptMessage("assistant",generatedIAResponse));
			}

			await new Promise(resolve => setTimeout(resolve, 6500));

		
			


}

async function requestGPT(requestPseudo, requestContent)
{
	if (!currentPhraseHasardProcess && !currentGPTProcess)
	{
		positionAttenteGPT++;
			
			var baseConversationIA = [];
			
				if (CATEGORY_ID != 2)
					baseConversationIA.push(new gptMessage("system","Tu es un animateur d'une émission de quiz télévisée qui parle d'une façon à animer une émission, tu t'appelles Marc Mazzoti, tu as une veste orange et une cravate verte ainsi que des cheveux jaunes et des lunettes de soleil et tu présentes l'émission \"Direct Quiz\" mais il n'est pas nécessaire de le rappeler à chaque fois. Pour lancer une partie, il faut choisir un mode de difficulté"+(enPleinQuiz?", un quiz a commencé et est actuellement en cours":", il n'y a aucun quiz commencé ou en cours")+" et les candidats présents sur le plateau sont : "+listeCandidatsLangageNaturel()+" avec le classement suivant : "+classementEnFR()));
				else
					baseConversationIA.push(new gptMessage("system","Tu es une animatrice d'une émission de quiz télévisée qui parle d'une façon à animer une émission, tu t'appelles Jessica, tu as une veste rose et un collier vert ainsi que des longs cheveux jaunes et des lunettes de soleil et tu présentes l'émission \"Direct Quiz\" mais il n'est pas nécessaire de le rappeler à chaque fois. Pour lancer une partie, il faut choisir un mode de difficulté"+(enPleinQuiz?", un quiz a commencé et est actuellement en cours":", il n'y a aucun quiz commencé ou en cours")+" et les candidats présents sur le plateau sont : "+listeCandidatsLangageNaturel()+" avec le classement suivant : "+classementEnFR()));
			
			if (CATEGORY_ID == 8) //EN
			{
				baseConversationIA.push(new gptMessage("system","Tu dois obligatoirement répondre en anglais et pas dans une autre langue !"));
			}
			else if (CATEGORY_ID == 9) //NL
			{
				baseConversationIA.push(new gptMessage("system","Tu dois obligatoirement répondre en néerlandais et pas dans une autre langue !"));
			}
			else //FR
			{
				baseConversationIA.push(new gptMessage("system","Tu dois obligatoirement répondre en français et pas dans une autre langue !"));
			}
			
			currentGPTProcess=true;

			var strConstitue = "";
			
			if (!enPleinQuiz)
			{
				if (CATEGORY_ID != 8)
					strConstitue = requestPseudo + " a dit \""+requestContent+"\", réponds-lui en citant son nom et ce que tu dis ne doit pas dépasser une vingtaine de mots.";
				else
					strConstitue = requestPseudo + " said \""+requestContent+"\", respond to him by mentioning his name and your statement should not exceed twenty words.";
			}
			else
			{
				if (CATEGORY_ID != 8)
					strConstitue = requestPseudo + " a dit \"" + requestContent + "\", réponds-lui en citant son nom mais tu ne peux pas dire les mots suivants : \"" + listeQuestions[cptQuestion].reponse.split("|").join(", ") + "\". ce que tu dis ne doit pas dépasser une dizaine de mots.";
				else
					strConstitue = requestPseudo + " said \"" + requestContent + "\", respond to him by mentioning his name, but you cannot use the words : \"" + await processLang(listeQuestions[cptQuestion].reponse.split("|").join(", ")) + "\". Your statement should not exceed ten words."
			
			}
			
			conversationIA.push(new gptMessage("user",strConstitue));
			
			if (conversationIA.length > 16)
			{
				conversationIA.shift();
			}
			
			baseConversationIA.push(...conversationIA);

			const completion = await openai.createChatCompletion({
			  model: "gpt-3.5-turbo-0613",
			  messages: baseConversationIA
			});
			
			let generatedIAResponse = completion.data.choices[0].message.content.replace(/"/g, '').replace(/\(e\)/g,'');
			
			if (CATEGORY_ID != 8)
			{
				if (CATEGORY_ID != 2)
					generatedIAResponse = generatedIAResponse.replace(/ qu'IA/g, " qu'animateur").replace(/ une IA/g, " un animateur").replace(/ modèle de langage/g, " animateur");
				else
					generatedIAResponse = generatedIAResponse.replace(/ qu'IA/g, " qu'animatrice").replace(/ une IA/g, " une animatrice").replace(/ un modèle de langage/g, " une animatrice").replace(/ modèle de langage/g, " animatrice");
			}
			
			conversationIA.push(new gptMessage("assistant",generatedIAResponse));
			
			speech(generatedIAResponse);
			io.sockets.emit('annonce',{type:98,annonce:generatedIAResponse});	 	
			
			await new Promise(resolve => setTimeout(resolve, 6500));

			currentGPTProcess=false;
			
		

	}

}

function updateOnlinePlayer(theme, nombre)
{
		const params = new URLSearchParams();
		params.append('theme', theme);
		params.append('nombre', nombre);

		axios({
		  method: 'post',
		  url: 'https://www.directquiz.org/niko.ovh/directquiz89/updateJoueursOnline.php',
		  data: params
		}).then(function (response) {					
			console.log("Nombre de joueurs online mis à jour");
		  })
		  .catch(function (error) {
			console.log(error);
		});
}

///// TRANSLATE /////
async function traduire(texteATraduire, lang)
{
	
	var target = 'fr';
	
	if (lang == "")
	{
		  // The target language
		switch (CATEGORY_ID)
		{
			case 8:   target = 'en'; break;
			case 9:   target = 'nl'; break;
			default:  target = 'fr'; break;
		}
	}
	else
	{
		target = lang;
	}

  // Translates some text into Russian
  const [translation] = await translate.translate(texteATraduire, target);

  return translation;

}

///// TEXT-TO-SPEECH /////
async function speech(texteAParler)
{

	  // The text to synthesize
	  var textSpeak = texteAParler.replace("お茶が好き","Oka ga souki").replace(/_/g," ");

	  // Construct the request
	  
	  var request = null;
	  
	  
	  if (CATEGORY_ID == 8)
	  {
				request = {
					input: {
					  text: textSpeak
					},
					voice: {
					  languageCode: 'en-GB',		  
					  ssmlGender: 'MALE',
					  name: "en-GB-Neural2-D"
					},
					audioConfig: {
					  audioEncoding: "MP3",
					  pitch: 4,
					  speakingRate: 1.3,
					  volumeGainDb: -0.95
					}
				  };
	  }
	  else if (CATEGORY_ID == 9)
	  {
				request = {
					input: {
					  text: textSpeak
					},
					voice: {
					  languageCode: 'nl-NL',		  
					  ssmlGender: 'MALE',
					  name: "nl-NL-Wavenet-B"
					},
					audioConfig: {
					  audioEncoding: "MP3",
					  pitch: 1.6,
					  speakingRate: 1.15,
					  volumeGainDb: -0.95
					}
				  };		  
	  }  
	  else
	  {
		  	  if (CATEGORY_ID != 2)
			  {
					request = {
					input: {
					  text: textSpeak
					},
					voice: {
					  languageCode: 'fr-FR',		  
					  ssmlGender: 'MALE',
					  name: "fr-FR-Neural2-D"
					},
					audioConfig: {
					  audioEncoding: "MP3",
					  pitch: 2.8,
					  speakingRate: 1.23,
					  volumeGainDb: -0.95
					}
				  };
			  }
			  else
			  {
					request = {
					input: {
					  text: textSpeak
					},
					voice: {
					  languageCode: 'fr-FR',		  
					  ssmlGender: 'FEMALE',
					  name: "fr-FR-Neural2-C"
					},
					audioConfig: {
					  audioEncoding: "MP3",
					  pitch: 2.8,
					  speakingRate: 1.23,
					  volumeGainDb: -0.95
					}
				  };
			  }
	  }


	  // Performs the text-to-speech request
	  const [response] = await clientSpeech.synthesizeSpeech(request);

	  const writeFile = util.promisify(fs.writeFile);
	   
	   const pathToDelete = '/var/www/html/speech/'+fileName;

		fs.unlink(pathToDelete, (err) => {
		  if (err) {
			console.error(err);
		  }
		});
	   
	   
      fileName = uuidv4()+'.mp3';
	  
      await writeFile('/var/www/html/speech/'+fileName, response.audioContent, 'binary');


mp3Duration('/var/www/html/speech/'+fileName, function (err, duration) {
  if (err) return console.log(err.message);
  io.sockets.emit('annonce_vocale',{type:50, annonce: fileName, secondes: duration});	
});	

}



async function messageGetByBot(data)
	{
		console.log(data.pseudo + " : " + data.message);
		if (data.message.indexOf('giphy.com/media/') > -1)
		{
			var message = data.message;
		}
		else
		{
			var message = data.message.replace(regexHTML,"");
		}
		
		/*
		versionPseudo = getMembreDetailByID(data.id).version;
		
		if (versionPseudo > 1)
		{
			data.pseudo = data.pseudo + "#" + versionPseudo;
		}
		*/
		
		reponseAttendue = "3598bde5-abe2-4703-a481-384c5276f2b7";
		
		var replace255 = String.fromCharCode(160);
		var regex255 = new RegExp(replace255,"gi");
		
		if (message.toUpperCase().indexOf("P0RN") > -1 || message.toUpperCase().indexOf("PORN") > -1 || message.toUpperCase().indexOf("XHAMSTER") > -1 || message.toUpperCase().indexOf("X-HAMSTER") > -1 || message.toUpperCase().indexOf("XNXX") > -1 || message.toUpperCase().indexOf("REDTUBE") > -1 || message.toUpperCase().indexOf("XXX") > -1)
		{
			if (CATEGORY_ID != 8)
				io.sockets.emit('annonce',{type:7,annonce: "Le message saisi par "+ data.pseudo + " a été bloqué, celui-ci ne respecte pas la charte du site !"});
			else
				io.sockets.emit('annonce',{type:7,annonce: "The message entered by "+ data.pseudo + " has been blocked, it does not comply with the site's rules!"});
		}
		else
		{
		
			if (cptQuestion >= 0 && cptQuestion < listeQuestions.length) reponseAttendue = listeQuestions[cptQuestion].reponse;
				
			if (!timeout && (!isFinish && isStart && (await isMatch(message.normalize("NFD").replace(/[\u0300-\u036f]/g, ""), reponseAttendue.normalize("NFD").replace(/[\u0300-\u036f]/g, "")) && cptQuestion >= 0)))
			{
				
					var pts = 0;

					if (listeRepondantCorrect.lastIndexOf(data.pseudo) == -1)
					{
						cptReponseDonnee++;
						
						switch (cptReponseDonnee)
						{
							case 1 : pts = echellePoints[0]; break;
							case 2 : pts = echellePoints[1]; break;
							case 3 : pts = echellePoints[2]; break;
							case 4 : pts = echellePoints[3]; break;
							default : pts = 0;					
						}

						if (pts > 0)
						{

							try
							{
								var indexToAddPoints = listeDesMembresAvecDetail.map(function(e) { return e.id; }).indexOf(data.id); //listeDesMembres.indexOf(data.pseudo);
								listeDesMembresAvecDetail[indexToAddPoints].points += pts;
								listeDesMembresAvecDetail[indexToAddPoints].lastGame = identifiantPartie;
								
								notifyPointsWin(new itemClassement(data.id, pts));
								notifyOrdreBonneReponse({uuid:data.id, ordre:cptReponseDonnee});
								
								var correctToMe = ((CATEGORY_ID != 8)?"Bonne réponse ! "+pts+" points gagnés !":"Good answer ! "+pts+" points win !");
								var correctToYou = ((CATEGORY_ID != 8)?data.pseudo + " a trouvé la bonne réponse ! "+pts+" points gagnés !":data.pseudo + " found the good answer ! "+pts+" points win !");
								
								switch (data.id)
								{
									case "bot1" : bot1OK = true; break;
									case "bot2" : bot2OK = true; break;
									case "bot3" : bot3OK = true; break;
								}
								
								
								
								//io.sockets.emit('annonce_user',{type:3, user:data.id, annonce:correctToMe });	
								io.sockets.emit('annonce',{type:3, annonce:correctToYou });
								listeRepondantCorrect.push(data.pseudo);
								refreshListeSlots();
								
								// Stoppage si tout le monde ou 4 personnes ont trouvé
									
									if (listeRepondantCorrect.length == 4 || listeRepondantCorrect.length == listeDesMembres.length)
									{
										clearTimeout(chronoDelaiQuestion);
										clearTimeout(chronoDelairPhraseHasard);

										timeout = true;
										clearInterval(timerSecondes);
										notifyChrono(0,40);
									
										var allReponseOK = ((CATEGORY_ID != 8)?"Toutes les réponses ont été trouvées...":"The answers have all been found...");
									
										speech(allReponseOK);
										io.sockets.emit('annonce',{type:4,annonce:allReponseOK});
		
										timeoutNextQuestion = setTimeout(nextQuestion, 6500);
									}
								
								
								
							}
							catch(error)
							{
								console.error(error);
								//socket.broadcast.emit('annonce',{type:3,annonce: ((CATEGORY_ID != 8)?"erreur détectée : ":"detected error : ") + error});
							}

						}
					}
					else // Cas réponse déjà trouvée par l'utilisateur qui écris un message
					{
						io.sockets.emit('annonce_user',{type:3, user:data.id, annonce: ((CATEGORY_ID != 8)?"Vous avez déjà trouvé la bonne réponse !":"You already found the good answer !")});
					}
			}
			else
			{

					if (message.indexOf("/speak") == 0)
					{
						speech(message.replace("/speak",""));			
					}				
					else if (message.indexOf("/kick ") == 0)
					{
						var commande = message.split(" ");
						kick(commande[1]);
					}
					else
					{
						
						var tempMessage = message;				
						var motsMessages = tempMessage.split(" ");
						var indexInsulte = -1;
						var isCensured = false;
						
						for (i = 0; i < motsMessages.length; i++)
						{
							indexInsulte = insultes.indexOf(motsMessages[i].normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase());
							
							if (indexInsulte > -1)
							{
								isCensured = true;
								var censure = "";
								for (j = 0; j < insultes[indexInsulte].length; j++)
								{
									censure += "*";
								}
								motsMessages[i] = censure;	
							}
						}
						
						data.message = motsMessages.join(" ");									
						
						io.sockets.emit('message', data);
						
						
						
						/*
					if (data.message.toLowerCase().indexOf("bot_1") > -1 || data.message.toLowerCase().indexOf("bot1") > -1 || data.message.toLowerCase().indexOf("bot 1") > -1)
					{
						requestGPTByBOT(data.pseudo, data.message, 1);
					}
					else if (data.message.toLowerCase().indexOf("bot_2") > -1 || data.message.toLowerCase().indexOf("bot2") > -1 || data.message.toLowerCase().indexOf("bot 2") > -1)
					{
						requestGPTByBOT(data.pseudo, data.message, 2);
					}
					else if (data.message.toLowerCase().indexOf("bot_3") > -1 || data.message.toLowerCase().indexOf("bot3") > -1 || data.message.toLowerCase().indexOf("bot 3") > -1)
					{
						requestGPTByBOT(data.pseudo, data.message, 3);
					}
					*/

						let volonteVoirImage = (data.message.indexOf("Montre-moi") == 0 || data.message.indexOf("montre-moi") == 0 || data.message.indexOf("Show me") == 0 || data.message.indexOf("show me") == 0 || data.message.indexOf("Laat me") == 0 || data.message.indexOf("laat me") == 0);
						
						if ((data.message.length >= 3 && data.ia) && !volonteVoirImage)
						{
							requestGPT(data.pseudo, data.message);
						}
						
						if (data.message.length >= 25 && volonteVoirImage && data.ia)
						{
							requestDalle(data.pseudo, data.message);
						}
						else if (volonteVoirImage && data.ia && !enPleinQuiz)
						{
							var excuseMontrerImage1 = ((CATEGORY_ID != 8)?"La demande de "+data.pseudo+" est trop courte !":"The request of "+data.pseudo+" is too short !");
							
							io.sockets.emit('annonce',{type:7, annonce:excuseMontrerImage1 });						
							
						}
						else if (volonteVoirImage && data.ia && enPleinQuiz)
						{
							var excuseMontrerImage2 = ((CATEGORY_ID != 8)?data.pseudo+", les représentation visuelle ne peuvent être montrée qu'en dehors d'un quiz !":data.pseudo+", the visual representations can be show only out of a quiz !");
							
							io.sockets.emit('annonce',{type:7, annonce:excuseMontrerImage2 });
						}
						
					}				
					
				
				
			}
		
	    }
		
	}

//////////////////////////






function getUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function timerSec()
{
	globalChronoQuestion--;
	notifyChrono(globalChronoQuestion, 40);
	if (globalChronoQuestion == 0) clearInterval(timerSecondes);
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

async function phrase5secondesAuHasard()
{
	
	return await processLang(phrases5secondesRestantes[getRandomInt(0,phrases5secondesRestantes.length)]);
	
}

async function phraseAttenteAuHasard()
{
	return await processLang(phrasesAttente[getRandomInt(0,phrasesAttente.length)]);
	
}


// CARAMBAR + DISCORD //////
async function lancerUneBlagueAuHasard()
{
	var blagueRandom = null;
	var discordRandom = null;
	
	do
	{
		blagueRandom = listeBlagues[getRandomInt(0,listeBlagues.length)];
	} while(lastBlague == blagueRandom);
	
	do
	{
		discordRandom = listePhrasesInutiles[getRandomInt(0,13)];
	} while(lastAnnonce == discordRandom);
	
	 
	
	// Traitement du remplacement par les noms des joueurs
		var membre1 = (CATEGORY_ID == 8?"a people":"une personne");
		var sizeM = listeDesMembres.filter(membre => membre.length > 2).length
		var randomNumber = 0;
		
		do { 
		randomNumber = getRandomInt(0,listeDesMembres.length);
		membre1 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
		} while (membre1 === undefined);

		var membre2 = (CATEGORY_ID == 8?"someone":"quelqu'un");

			if ((discordRandom.indexOf("ggg") > -1) && sizeM  > 1)
			{
				

				do { 
							randomNumber = getRandomInt(0,listeDesMembres.length);
							membre1 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
				} while (membre1 === undefined);		
			
					do {
					

						do { 
						randomNumber = getRandomInt(0,listeDesMembres.length);
						membre2 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
						} while (membre2 === undefined);


					}
					while (membre1 == membre2);
				
				
				 discordRandom = discordRandom.replace(/mmm/g,membre1).replace(/ggg/g,membre2);
			}
			else
			{
				
				do {
				discordRandom =  listePhrasesInutiles[getRandomInt(0,13)];
				} while (discordRandom.indexOf("ggg") > -1);

				discordRandom = discordRandom.replace(/mmm/g,membre1);
			}

	var phraseToSend = "";
	
	if (cptQuestion >=4 && cptQuestion <= 8)
	{
		phraseToSend = await processLang(discordRandom);
		speech(phraseToSend);	
	}
	else
	{
		phraseToSend = await processLang(blagueRandom);
		speech(phraseToSend);
		lastBlague = blagueRandom;		
	}


	io.sockets.emit('annonce',{type:98,annonce:phraseToSend});
}
//////////////////

async function lancerUnePhraseAuHasard()
{
	if (!currentGPTProcess)
	{
		
		currentPhraseHasardProcess = true;
	
		if (cptQuestion%2 == 0)
		{
			lancerUneBlagueAuHasard();
		}
		else
		{	
			var phraseRandom = null;
			
				do
				{
					phraseRandom = listePhrasesInutiles[getRandomInt(12,listePhrasesInutiles.length)];
				} while(lastAnnonce == phraseRandom);		
			
			var membre1 = (CATEGORY_ID == 8?"a people":"une personne");

			var sizeM = listeDesMembres.filter(membre => membre.length > 2).length

			var randomNumber = 0;
			
			do { 
			randomNumber = getRandomInt(0,listeDesMembres.length);
			membre1 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
			} while (membre1 === undefined);

			var membre2 = (CATEGORY_ID == 8?"someone":"quelqu'un");
			
			if (isFinish)
			{
						if (isFirstFinish)
						{
							phraseRandom = listePhrasesSortie[0];
							isFirstFinish = false;
						}
						else
						{
								do
								{
									phraseRandom = listePhrasesSortie[getRandomInt(0,listePhrasesSortie.length)];
								} while(lastAnnonce == phraseRandom);
								
								lastAnnonce = phraseRandom;
						}			
			}
			else
			{	
				if ((phraseRandom.indexOf("ggg") > -1) && sizeM  > 1)
				{
					

				do { 
							randomNumber = getRandomInt(0,listeDesMembres.length);
							membre1 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
				} while (membre1 === undefined);		
				
						do {
						

							do { 
							randomNumber = getRandomInt(0,listeDesMembres.length);
							membre2 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
							} while (membre2 === undefined);


						}
						while (membre1 == membre2);
					
					 lastAnnonce = phraseRandom;
					 phraseRandom = phraseRandom.replace(/mmm/g,membre1).replace(/ggg/g,membre2);
					 
					 
				}
				else
				{
					
					do {
					phraseRandom =  listePhrasesInutiles[getRandomInt(12,listePhrasesInutiles.length)];
					} while (phraseRandom.indexOf("ggg") > -1);
					
					lastAnnonce = phraseRandom;
					phraseRandom = phraseRandom.replace(/mmm/g,membre1);
				}
			}

			var phraseTraduite = await processLang(phraseRandom);

			speech(phraseTraduite);	
			io.sockets.emit('annonce',{type:98,annonce:phraseTraduite});
		}
		
		 setTimeout(() => {
					currentPhraseHasardProcess = false;
			}, 8200);

	}	
}

function init(resetMember)
{

	isStart = false;
	isFinish = false;
	isBlague = false;
	enPleinQuiz = false;

	globalChronoQuestion = 40;		
	
	messageTitre = "";

	clearInterval(chronoPhSortieHasard);
	clearInterval(timerSecondes);

	clearTimeout(timeoutDebutPartie);
	clearTimeout(timeoutFinQuestion);
	clearTimeout(timeoutQuizFinish);
	clearTimeout(timeoutQuizContinue);
	clearTimeout(timeoutAnnonceCandidats);
	clearTimeout(timeoutAnnonceQuestion);
	clearTimeout(timeoutNextQuestion);
	clearTimeout(timeoutRefreshTopMessage);
	
	listeRepondantCorrect = new Array(0);
	countMember = 0;
	
	if (resetMember)
	{
		currentNumberOfBot = 0;
		listeDesMembres = new Array(0);
		listeDesMembresAvecDetail = new Array(0);
		listeDesMembresRestant = new Array(0);
		listeDesMembresOffline = new Array(0);
		listeDesMembresAvecDetailOffline = new Array(0);
		conversationIA = [];	
		processClearBot();	
	}
	else
	{
		isFinish = true;
	}	
	
	cptQuestion = -1;
	cptReponseDonnee = 0;
	chrono = null;
	// var maxQuestionID = 1;
	
	listeQuestions = new Array(0);
	listeIDQuestions = new Array(0);
	
	var IDgot = 0;

				const params10 = new URLSearchParams();
				params10.append('niveau', niveauDifficulte);
				params10.append('theme', CATEGORY_ID);

				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/niko.ovh/directquiz89/get10QuestionsID.php',
				  data: params10
				}).then(function (response10) {					
					listeIDQuestions = response10.data.split("###");
					
				for (i = 0; i < 10;i++)
				{
					//console.log(">>>"+listeIDQuestions[i]+"<<<");
					
				const params = new URLSearchParams();
				params.append('id', listeIDQuestions[i]);
				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/niko.ovh/directquiz89/getQuestionV2.php',
				  data: params
				}).then(function (response) {
					  //console.log(response.data);
					var resultQuestion = response.data.split("###");
					listeQuestions.push(new question(resultQuestion[0],resultQuestion[1].toUpperCase(), parseInt(resultQuestion[2], 10), resultQuestion[3]));
				  })
				  .catch(function (error) {
					console.log(error);
				  });
					
				}

					
				  })
				  .catch(function (error) {
					console.log(error);
				});

phrasesAttente = new Array(0);

phrasesAttente.push("Bienvenue cher candidat, d'autres joueurs pourraient rejoindre d'un moment à l'autre !");
phrasesAttente.push("Patience ###PSD40###, d'autres candidats devraient arriver...");
phrasesAttente.push("Bonjour ###PSD40###, installe-toi en attendant que les autres candidats rejoignent ce quiz \""+CATEGORY+"\"...");
phrasesAttente.push("Salutation ###PSD40###, certains joueurs ont été notifiés de ta venue, prends patience...");
phrasesAttente.push("Bienvenue pour participer à ce quiz \""+ CATEGORY +"\", ###PSD40###, qui s'annonce excellent et plein de rebondissements !");
phrasesAttente.push("Un tonnerre d'applaudissements pour ###PSD40### qui vient de rejoindre ce quiz \""+ CATEGORY +"\" ! Ah mais je vois qu'il n'y a pas encore d'autres candidats...");


phrases5secondesRestantes = new Array(0);

phrases5secondesRestantes.push("Il ne reste plus que 5 secondes...");
phrases5secondesRestantes.push("Attention, seulement 5 secondes restantes !");
phrases5secondesRestantes.push("Dépéchez-vous, le temps est bientôt écoulé !");
phrases5secondesRestantes.push("5 secondes et le temps sera écoulé...");
phrases5secondesRestantes.push("Encore 5 secondes.");
phrases5secondesRestantes.push("Prudence, plus que 5 secondes");
phrases5secondesRestantes.push("Plus que 5 secondes au chrono.");
phrases5secondesRestantes.push("Le temps est limité, 5 secondes restantes");
phrases5secondesRestantes.push("Faites vite car il ne reste que 5 secondes");
phrases5secondesRestantes.push("Le chrono s'arrète dans 5 secondes");
phrases5secondesRestantes.push("5 secondes");
phrases5secondesRestantes.push("Vite, vite, vite, plus que 5 secondes !");
phrases5secondesRestantes.push("5 secondes, c'est le temps qu'il vous reste !");
phrases5secondesRestantes.push("Ce sont les dernières secondes !");
phrases5secondesRestantes.push("5 secondes restantes !");
phrases5secondesRestantes.push("Il reste peu de temps, faites vite !");
	
listePhrasesInutiles = new Array(0);

listePhrasesInutiles.push("Va faire un tour sur le Discord Officiel, tu pourras t'inscrire aux notifications des joueurs qui rejoignent une partie en t'attribuant le rôle Mention Player DirectQuiz");
listePhrasesInutiles.push("Je pense que mmm devrait aller jeter un oeil sur le Discord Officiel pour s'attribuer le rôle Mention Player DirectQuiz et être notifié des joueurs qui rejoignent une partie...");
listePhrasesInutiles.push("Ecoute-moi mmm, si tu souhaites être notifié des parties sur Direct Quiz, rejoins le Discord Officiel et attribue-toi le rôle dans #rôle-game");
listePhrasesInutiles.push("Un seul endroit pour être notifié des parties et discuter avec la communauté : Le Discord Officiel");
listePhrasesInutiles.push("Hey mmm, ça te dit d'être notifié lorsque quelqu'un rejoint une partie ? Va donc t'attribuer le rôle Mention Player DirectQuiz sur le Discord Officiel");
listePhrasesInutiles.push("Il semblerait que mmm et ggg souhaitent être notifiés des nouvelles parties sur le site, il suffit pour cela qu'ils rejoignent le Discord Officiel et s'attribuent le rôle de mention \"Direct Quiz\"");
listePhrasesInutiles.push("Nikofly, l'admin, aimerait bien que le plus de joueurs possible soient notifiés lorsqu'un joueur rejoint un salon de quiz, un seul endroit pour avoir les notifs : le Discord Officiel");
listePhrasesInutiles.push("Il semblerait que mmm ne soit pas encore abonné aux notifications du jeu sur le Discord Officiel, à moins que ce ne soit ggg qui n'en a pas pris connaissance...");
listePhrasesInutiles.push("mmm, sais-tu que tu peux recevoir des notifications quand quelqu'un rejoint une partie ? Rends-toi donc sur le Discord Officiel et attribues-toi le rôle Mention Player DirectQuiz");
listePhrasesInutiles.push("Inscrivez-vous sur le Discord Officiel !");
listePhrasesInutiles.push("Le Discord Officiel n'attend que vous et vous permettra d'être notifié des nouvelles parties sur le site !");
listePhrasesInutiles.push("N'hésitez pas à lier votre compte Directquiz à votre compte Discord pour être mentionné sur le Discord Officiel quand vous venez ou quittez une partie !");
listePhrasesInutiles.push("Je me demande si mmm a lié son compte Directquiz à son compte Discord, cela lui permettrait d'être mentionné dans les notifications du Discord Officiel !");
listePhrasesInutiles.push("Regardez en haut à droite du site, vous avez une option pour lier votre compte Discord à Directquiz !");

listePhrasesInutiles.push("mmm devrait arrêter de se tourner les pouces et se concentrer sur la partie.");
listePhrasesInutiles.push("mmm est certainement décidé à gagner cette partie !");
listePhrasesInutiles.push("mmm mise tout sur sa victoire...");
listePhrasesInutiles.push("Inutile de préciser que mmm est déterminé à l'emporter sur cette partie...");

listePhrasesInutiles.push("N'oubliez pas de vous laver régulièrement les mains, surtout toi mmm !");
listePhrasesInutiles.push("Prenez exemple sur mmm en toussant dans votre coude...");
listePhrasesInutiles.push("Prière de garder une distance d'un mètre 50 au minimum entre chaque candidats");
listePhrasesInutiles.push("Mes pensées vont aux réfugiés ukrainiens...");
listePhrasesInutiles.push("Directquiz est un jeu qui demande de l'intuition et de la mémoire");
listePhrasesInutiles.push("mmm, merci de ne pas copier sur votre voisin.");

listePhrasesInutiles.push("mmm et ggg ont l'air déterminés plus que jamais à gagner cette partie !");
listePhrasesInutiles.push("Je pense que mmm versera un petit quelque chose en fin d'émission dans notre tirelire réservée aux victimes de la guerre en Ukraine...");

listePhrasesInutiles.push("Je rappelle que des repas seront servis aux candidats après l'émission");
listePhrasesInutiles.push("Des toilettes seront disponibles à la pause, je pense que mmm en a bien besoin");

listePhrasesInutiles.push("Espérons que la variole du singe n'atteigne pas nos candidats...");

listePhrasesInutiles.push("Grâce au vaccin mmm et ggg ont retrouvé une vie normale...");
listePhrasesInutiles.push("Il semblerait que mmm ai bien révisé ses classiques.");
listePhrasesInutiles.push("mmm pense surement qu'il va gagner la partie");
listePhrasesInutiles.push("mmm est un redoutable adversaire, il cache bien son jeu...");
listePhrasesInutiles.push("mmm devrait se méfier de ggg...");
listePhrasesInutiles.push("mmm apprécie bien la performance de ggg...");
listePhrasesInutiles.push("mmm veut absolument battre ggg, car il se trouve meilleur que lui...");
listePhrasesInutiles.push("mmm a reçu sa troisième dose du vaccin, il ne peut plus rien lui arriver");
listePhrasesInutiles.push("mmm est certainement en train de jouer au bureau au lieu de travailler...");
listePhrasesInutiles.push("mmm ne va peut être pas changer le monde, mais brillera certainement lors de cette partie endiablée en compagnie de ggg");
listePhrasesInutiles.push("Serez-vous les coudes, et répondez pour un mieux à la question qui vous est posée.");
listePhrasesInutiles.push("Le savoir forme la sagesse, et la sagesse dessinne un avenir radieux, mmm est bien placé pour le savoir !");
listePhrasesInutiles.push("Je me demande qui de mmm ou ggg va le plus briller lors de cette émission !");
listePhrasesInutiles.push("Il vous reste 20 secondes pour répondre à la question...");
listePhrasesInutiles.push("Les réfugiés ukrainiens, n'ont plus que leurs yeux pour pleurer, aidons-les grâce à la cagnotte mise en place.");
listePhrasesInutiles.push("J'espère que mmm va bien mais en tout cas ggg à l'air de bien veiller sur lui. Remercions ggg de bien veiller sur mmm...");
listePhrasesInutiles.push("La Covid19 n'est plus que de l'histoire ancienne.");
listePhrasesInutiles.push("OK Google, raconte-moi une blague s'il te plait !");


listeBlagues = new Array(0);

listeBlagues.push("Pourquoi les indiens mettent-ils leur argent en bourse ? Parce que ça rapporte beaucoup de sioux.");
listeBlagues.push("Pourquoi les poules ne lèvent-elles qu’une patte en dormant ? Parce que si elles levaient les deux pattes, elles se casseraient la figure !");
listeBlagues.push("Monsieur, savez-vous que votre chien aboie toute la nuit ? « Oh, ça ne fait rien, il dort toute la journée ! »");
listeBlagues.push("Pourquoi Michaël ouvre t-il la porte ? Parce que Jack sonne .");
listeBlagues.push("Comment les abeilles font-elles l’amour ? Dare-dare !");
listeBlagues.push("Quelles sont les lettres que l’on boit au petit déjeuner ? K.K.O.");
listeBlagues.push("Quel est le futur de « je bâille » ? Je dors.");
listeBlagues.push("J’ai cru que Mozart était mort mais mozzarella !");
listeBlagues.push("Comment appelle-t-on un squelette bavard ? Un os-parleur !");
listeBlagues.push("Chauffeur, soyez prudent, à chaque virage j’ai peur de tomber dans le ravin ! – Madame n’a qu’à faire comme moi, fermer les yeux !");
listeBlagues.push("Deux volcans discutent : « Mais dis-moi, tu n’aurais pas arrêté de fumer toi ? »");
listeBlagues.push("Que disent les plongeurs au nouvel an ? Bonne Apnée !");
listeBlagues.push("Quelle est la ressemblance entre un parachute et l’humour ? Quand on n’en a pas, on s’écrase !");
listeBlagues.push("C’est une jolie antenne de TV qui est tombée amoureuse d’un paratonnerre. Elle murmure : « Dis, tu y crois toi, au coup de foudre ? »");
listeBlagues.push("Quelle est la femelle du Hamster ? Hamster-Dame ");
listeBlagues.push("Quel est l’animal le plus malheureux ? Le taureau, parce que sa femme est vache !");
listeBlagues.push("Pourquoi faut-il se méfier des sirènes au volant ? Parce qu’elles font des queues de poisson !");
listeBlagues.push("C’est une feuille de papier qui avance dans l’eau. Et qui dit au secours j’ai pas pieds ! .");
listeBlagues.push("La maîtresse dit à Kevin : – « Une grande rue s’appelle une artère. » – « Ouais, et la traverser sans se faire écraser, c’est une veine ! »");
listeBlagues.push("Quel est comble pour une couturière ? De perdre le fil de la conversation !");
listeBlagues.push("Quel est le comble du chauve ? C’est d’avoir un cheveu sur la langue.");
listeBlagues.push("Quel animal est sourd ? Le crapaud, parce qu’il fait coâ-coâ !");
listeBlagues.push("Quel est le comble pour un professeur de musique ? Mettre des mauvaises notes !");
listeBlagues.push("Comment reconnaît-on un idiot dans un magasin de chaussures ? C’est celui qui essaye les boites.");
listeBlagues.push("Avez-vous amené au zoo le pingouin que vous avez trouvé dans la rue ? – « Oui, il a bien aimé, mais aujourd’hui on va au cinéma. »");
listeBlagues.push("Une star de cinéma est en train de prendre son bain. Le robinet d’eau froide dit au robinet d’eau chaude : « Alors, toi aussi, elle te fait tourner la tête ? »");
listeBlagues.push("Un petit nuage se promène avec sa maman dans le ciel. Tout à coup, il s’arrête en se tortillant : – « J’ai envie de faire pluie-pluie, maman ! »");
listeBlagues.push("Quel est le sport préféré des insectes ? Le criquet.");
listeBlagues.push("Une jeune femme demande à une autre : – « Alors, toujours amoureuse de ton parachutiste ? » – « Non, je l’ai laissé tomber. »");
listeBlagues.push("Quelles sont les lettres qui bougent tout le temps ? AJT! .");
listeBlagues.push("Qu’est-ce qui fait « aaaatchoum » quand je veux le manger en dessert ? Un baba au rhum  !");
listeBlagues.push("Quel avis peut-on donner à un marin ? Un avis rond ! .");
listeBlagues.push("C’est un bol et une tasse qui sont devant un évier : Je n’ai pas envie de plonger, dit le bol, à chaque fois je bois la tasse. C’est vrai, répond la tasse, tu n’as jamais eu de bol !");
listeBlagues.push("Toto demande à son père : – « Papa, c’est quoi un synonyme ? » – « C’est un mot qu’on emploie quand on ne sait pas comment l’autre s’écrit. »");
listeBlagues.push("Que dit-on d’un garçon qui ramène le pain à la maison ? C’est le petit calepin !");
listeBlagues.push("Quel est le comble pour un professeur de géographie ? C’est de perdre le Nord !");
listeBlagues.push("Qu’est-ce qui fait 999 fois « Tic » et 1 fois « Toc » ? Un mille pattes avec une jambe de bois !");
listeBlagues.push("Si j’écris « 192 poule », est-ce que je fais une faute d’orthographe ? Non, car c’est un œuf de poule.");
listeBlagues.push("Que dit-on d’un garçon qui se promène un transistor à la main ? C’est le petit caleçon !");
listeBlagues.push("Quel est le comble pour un électricien ? C’est d’avoir des ampoules aux pieds !");
listeBlagues.push("Qui du marin ou de l’aviateur écrit le moins ? Le marin car il a jeté l’ancre !");
listeBlagues.push("Quelle est l’étoile la plus sale ? L’étoile d’araignée !");
listeBlagues.push("Comment appelle t-on le père et la mère de l’homme invisible ? Ses transparents !");
listeBlagues.push("Quel est le plat préféré des opticiens ? Les lentilles !");
listeBlagues.push("A l’entrée d’un pont, il y a une pancarte : « Ne pas passer à deux, sinon le pont casse. » Un homme lit la pancarte et passe. Crac, le pont cède. Pourquoi ? Parce qu’un homme averti en vaut deux !");
listeBlagues.push("Quel est le plus beau compliment qu’un cannibale puisse faire à une fille ? « Vous êtes belle à croquer ! »");
listeBlagues.push("Pourquoi les femmes de ménage préfèrent-elles faire le ménage chez les musiciens ? Parce qu’elles ont un do-mi-si-la-sol-fa-si-la-si-ré !");
listeBlagues.push("Le lion est le roi de la jungle. Il n’a peur que d’un animal. Lequel ? La lionne.");
listeBlagues.push("Quel est le cri des fourmis ? Cro-ondes !");
listeBlagues.push("Le roi souffre des dents. Son dentiste lui dit : – « Sir, il faudrait changer de couronne. » – « Ah ça jamais ! Répond le roi. »");
listeBlagues.push("Quelle différence y a-t-il entre une femme et le lait ? La femme, elle aime acheter, le lait on l’aime U.H.T.");
listeBlagues.push("Un Essuie-tout dit a un papier cul: L’Essuie-tout dit: « Ca va bien toi aujourd’hui ? » Le papier cul lui répond: « Non, toujours dans la merde ! »");
listeBlagues.push("Que portons-nous qui pourtant nous supportent ? Les chaussures.");
listeBlagues.push("Quelle est la ressemblance entre un ascenseur et une cigarette ? Ils font tous les deux des cendres .");
listeBlagues.push("Qu’est-ce qui pleure quand on lui tourne la tête ? Un robinet.");
listeBlagues.push("Au restaurant, Monsieur Leplatre s’écrie : – « Serveur, il y a une mouche qui nage dans mon assiette. » – « Oh, c’est encore le chef qui a mis trop de potage. D’habitude, elles ont pied ! »");
listeBlagues.push("Il siffle sans bouche, court sans jambes, frappe sans mains, passe sans paraître. Qui est-ce ? Le vent.");
listeBlagues.push("Comment appelle t-on une fleur qui prend sa graine à moitié ? Une migraine.");
listeBlagues.push("Un gendarme fait stopper une automobile : – « Vous n’aviez pas vu le feu rouge ? » – « Si si. C’est vous que je n’avais pas vu ! »");
listeBlagues.push("Quelles sont les lettres les plus vieilles de l’alphabet ? A et G");
listeBlagues.push("Que dit un vitrier à son fils pour qu’il soit sage ? Tiens-toi à carreaux si tu veux une glace !");
listeBlagues.push("Comment appelle-t-on un chien sans patte ? On ne l’appelle pas, on va le chercher !");
listeBlagues.push("Quel est l’animal qui mange avec sa queue ? Tous. Aucun n’enlève sa queue pour manger.");
listeBlagues.push("D’où vient le son de la trompette ? De l’Asie parce qu’il est perçant.");
listeBlagues.push("Pourquoi les pécheurs sont-ils maigres ? Parce qu’il surveille leur lignes.");
listeBlagues.push("Dans la rue, un homme demande à Madame Leplatre : – « Vous n’auriez pas vu un policier ? » – « Non. » – « Alors donnez-moi votre sac à main. »");
listeBlagues.push("Que disent deux grains de sables dans le désert ? Je crois qu’on est suivi.");
listeBlagues.push("Quel est le point commun entre un pâtissier et un ciel orageux ? Tous les deux font des éclairs.");
listeBlagues.push("Quelle est la ressemblance entre un facteur et un jongleur ? Tous les deux ont besoin de beaucoup d’adresse.");
listeBlagues.push("Pourquoi les Anglais n’aiment-ils pas les grenouilles ? Parce qu’elles font le thé tard !");
listeBlagues.push("Charlotte, j’espère que tu prêtes ta luge à ton petit frère ? – « Oh oui, maman ! Lui, il l’a pour monter, et moi pour descendre! »");
listeBlagues.push("J’ai un chapeau et pas de tête, un pied et pas de souliers. Qui suis-je ? Le champignon.");
listeBlagues.push("Monsieur et Madame Dupont ont cinq enfants, la moitié sont des filles. Comment est-ce possible ? L’autre moitié ce sont des filles aussi !");
listeBlagues.push("De quelle couleur sont les petits pois ? Les petits pois sont rouges.");
listeBlagues.push("Ce matin, j’ai voulu faire une blague sur le Super U, mais elle n’a pas Supermarché !");
listeBlagues.push("Où les supers héros vont-ils faire leurs courses ? Au Super Marché !");
listeBlagues.push("Pourquoi les maisons des montagnards sont-elles faites en bois ? Parce qu’on a eu besoin des pierres pour construire la montagne !");
listeBlagues.push("Quel est le comble pour un avion ? D’avoir un anti-vol.");
listeBlagues.push("Deux enfants passent devant un panneau « Ralentir, école ». Tu te rends compte, dit l’un, ils ne croient tout de même pas qu’on va y aller en courant !");
listeBlagues.push("Qu’est-ce qui tombe sans faire de bruit ? La nuit.");
listeBlagues.push("Quel est le comble de la clé ? C’est d’être mise à la porte.");
listeBlagues.push("Pourquoi les Pokemons ne peuvent pas conduire? Parce qu’ils ont trop de PV.");
listeBlagues.push("Quelle était la déesse qui énervait le plus Jupiter ? C’est Minerve… (qu’est-ce qu’elle minerve celle-là !)");
listeBlagues.push("La maîtresse demande à Kevin : – « Conjugue-moi s’il te plait le verbe savoir à tous les temps. » – « Je sais qu’il pleut, je sais qu’il fera beau, je sais qu’il neige. »");
listeBlagues.push("Quel super héros donne le plus vite l’heure ? Speed heure man! ");
listeBlagues.push("Qu’est-ce qu’un squelette dans une armoire ? C’est quelqu’un qui a gagné à cache-cache.");
listeBlagues.push("Quel est le fruit que les poissons n’aiment pas ? La pêche !");
listeBlagues.push("Un sucre tombe amoureux d’une cuillère. Le sucre lui dit alors : « Nous pourrions peut-être nous rencontrer dans un café ? »");
listeBlagues.push("Deux œufs se rencontrent : – « T’as l’air brouillé toi aujourd’hui. » – « Je suis crevé, totalement à plat ! »");
listeBlagues.push("La maîtresse demande de construire une phrase avec l’adjectif épithète. Cyril lève le doigt et dit : « Aujourd’hui il pleut, épithète demain, il fera beau ! »");
listeBlagues.push("Quelles sont les lettres qui sont toujours aux toilettes : Ce sont les lettres WC et PQ.");
listeBlagues.push("Quelle différence y a-t-il entre un horloger et une girouette ? L’horloger vend des montres et la girouette montre le vent.");
listeBlagues.push("Un homme tombe de la Tour Eiffel. Ses cheveux ne tombent que dix minutes plus tard. Pourquoi ? Parce qu’il utilise un shampooing qui ralentit la chute des cheveux !");
listeBlagues.push("Comment appelle-t-on les petits d’une oie ? Les noisettes.");
listeBlagues.push("Tuer son père, c’est un parricide. Tuer son frère, c’est un fratricide. Tuer son beau-frère, c’est... Un insecticide, parce qu’il tue l’époux de sa sœur !");
listeBlagues.push("Que se fait un Schtroumpf quand il tombe ? Un bleu !");
listeBlagues.push("Moi j’ai toute l’année des ampoules dans les mains. – Vous faites un travail difficile ? – Non, je suis vendeur au rayon électricité.");
listeBlagues.push("Quelle ressemblance y a-t-il entre un thermomètre et un professeur ? On tremble quand ils marquent zéro.");
listeBlagues.push("« J’ai battu un record », se vente un fou. - Ah bon, lequel, lui demandais-je? - J’ai réussi à faire en 13 jours un puzzle sur lequel il y avait écrit « de 3 à 5 ans »");
listeBlagues.push("Pourquoi les aiguilles sont-elles moins intelligentes que les épingles ? Parce qu’elles n’ont pas de tête.");
listeBlagues.push("Comment les cannibales appelaient-ils les jambes des missionnaires ? Les pattes alimentaires.");
listeBlagues.push("Quel est le comble d’un juge gourmand ? Manger des avocats !");
listeBlagues.push("Le coiffeur dit à son client : – « Cette mousse ferait pousser des cheveux sur une boule de pétanque. » – « Très bien. Mais est-ce que ça ne gênerait pas un peu le jeu ? »");
listeBlagues.push("Quelle est la différence entre une grue et toi ? La grue porte des caisses et toi tu en lâches !");
listeBlagues.push("Que dit un chien quand il cherche quelque chose et qu’il ne trouve pas ? Je suis tombé sur un os.");
listeBlagues.push("Dialogue de fou : « Oui monsieur le commissaire, mon père est maire, ma tante est soeur. J’ai un cousin qui est frère et mon frère est masseur. »");
listeBlagues.push("Que font deux brosses à dents le 14 juillet ? Un feu dentifrice !");
listeBlagues.push("Quel est le sport le plus fruité ? La boxe. Parce que quand on te met une pêche dans la poire, tu tombes dans les pommes, tu n’as pas intérêt à ramener ta fraise, tout ça pour des prunes.");
listeBlagues.push("Quel est le comble pour une femme assoiffée au milieu du désert ? Avoir l’air d’une gourde !");
listeBlagues.push("Quel est le fruit le plus féminin ? L’ananas !");
listeBlagues.push("Des oeufs sont rangés dans un frigo. Un oeuf demande à son partenaire : – « Mais pourquoi as-tu des poils ? » – « Parce-que je suis un kiwi connard ! »");
listeBlagues.push("Deux vis parlent d’un tournevis : – « Ohhh celui-là, quel beau gosse ! » – « Il nous a bien fait tourner la tête! »");
listeBlagues.push("Pourquoi n’y a t il pas de corbeilles dans les piscines ? Parce que la corbeille à papier.");
listeBlagues.push("Que dit un oignon quand il se cogne ? Aïe !");



listePhrasesSortie = new Array(0);

listePhrasesSortie.push("Hey, vous pouvez rejoindre le Discord Officiel, il vous sera possible de vous attribuer un rôle pour obtenir des notifications lorsque quelqu'un rejoint une partie !");

listePhrasesSortie.push("Veuillez quitter la salle car le quiz est terminé ou alors relancez une partie...");
listePhrasesSortie.push("Veuillez sortir du plateau en file indienne ou restez pour une autre partie...");
listePhrasesSortie.push("Prière de déguerpir avant qu'on ferme ou demander une prolongation de l'émission !");
listePhrasesSortie.push("Vous prenez vos affaires et vous foutez le camp ! A moins que vous vouliez rester pour une autre partie...");
listePhrasesSortie.push("Si vous ne déguerpissez pas immédiatement, j'appelle les flics ! A moins que vous vouliez relancer un mode de jeu ?");



listePhrasesSortie.push("On félicite encore nos candidats qui sont encore présents !");
listePhrasesSortie.push("Bravo encore à tous nos candidats encore présents");
listePhrasesSortie.push("Puisque vous êtes toujours là, pourquoi ne pas relancer une partie ?");

// Implémenter un check minimum de joueurs
//Je vois que vous êtes peu nombreux, n'hésitez pas à appeler vos amis la prochaine fois

listePhrasesSortie.push("Sécurité ! Evacuez-moi ces gens qui n'ont pas encore relancé de partie !");
listePhrasesSortie.push("Partez ou relancez une partie mais va falloir prendre une décision.");
listePhrasesSortie.push("L'émission est terminé, déguerpissez ou revenez pour une nouvelle manche !");
listePhrasesSortie.push("De 2 choses l'une, soit vous relancez une partie, soit vous vous en allez...");
listePhrasesSortie.push("Vous pouvez relancer une partie, ou vous en aller, c'est comme vous le voulez.");
listePhrasesSortie.push("Partez ou déguerpissez, le choix vous est donné.");


 

}

function notify(uuid, displayname, type)
{
	const params = new URLSearchParams();
				params.append('uuid', uuid);
				params.append('displayName', displayname);
				params.append('type', type);
				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/niko.ovh/directquiz89/notify.php',
				  data: params
				}).then(function (response) {
					 
				  })
				  .catch(function (error) {
					console.log(error);
				  });
}

function question(description, reponse, difficulty, id)
{
	this.description = description;
	this.reponse = reponse;
	this.difficulty = difficulty;
	this.id = id;
}

function membre(id, name, tokentime, points, floodCPT, floodTime, floodRespawnTime, floodNbrInfractions, version, lastGame, clientID)
{
	this.id = id;
	this.name = name;
	this.tokentime = tokentime;
	this.points = points;
	this.floodCPT = floodCPT;
	this.floodTime = floodTime;
	this.floodRespawnTime = floodRespawnTime;
	this.floodNbrInfractions = floodNbrInfractions;
	this.version = version;
	this.lastGame = lastGame;
	this.clientID = clientID
}

function itemClassement(uuid, points)
{
	this.uuid = uuid;
	this.points = points;
}

function classementEnFR()
{

	let strFinal = "";

	for (i=0; i<listeDesMembresAvecDetail.length;i++)
	{
		strFinal += listeDesMembresAvecDetail[i].name+" possède " + listeDesMembresAvecDetail[i].points + "point(s), ";
	}
	
	return strFinal;

}

function getIndexOfMemberByPseudo(liste, pseudo)
{
	for (i = 0; i < liste.length ; i++)
	{
		if (liste[i].name == pseudo) return i;
	}
	
	return -1;
}

async function nextQuestion()
{
	listeRepondantCorrect = new Array();
	cptReponseDonnee = 0;
	
	
	if (cptQuestion >= (listeQuestions.length-1))
	{
		clearInterval(chrono);
		timeoutFinQuestion = setTimeout(async function(){
			isStart = false;
			clearInterval(timerSecondes);
			
			globalChronoQuestion = 40;			
			notifyChrono(0,40);	

			var annonceBonneReponseEtait = await processLang("Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !");

			speech(annonceBonneReponseEtait);	

			io.sockets.emit('annonce',{type:4,annonce:annonceBonneReponseEtait});
			messageTitre = annonceBonneReponseEtait;
			
			isFinish = true;
			timeout = true;
				timeoutQuizFinish = setTimeout(function(){
					
					var messageFin = ((CATEGORY_ID != 8)?"Le quiz est terminé ! Merci à tous pour votre participation !":"The quiz is over ! Thanx to all for your participation !");
					
					speech(messageFin);	
					
					enPleinQuiz = false;
					isFirstFinish = true;
					io.sockets.emit('annonce',{type:4,annonce:messageFin});
					messageTitre = "The quiz is over !";
					updateScoreInDB();
					// (mis au /start) init(false);
					chronoPhSortieHasard = setInterval(lancerUnePhraseAuHasard, 120000);	
					identifiantPartie = "";					
				},6000);

		},250);
	}
	else
	{
		
		bot1OK = false;
		bot2OK = false;
		bot3OK = false;

		timeoutFinQuestion = setTimeout(async function(){
			timeout = true;
			clearInterval(timerSecondes);
			notifyChrono(0,40);

						try{
							
							var messageLaBonneReponseEtaitTempsEcoule = await processLang("Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !");
							
							speech(messageLaBonneReponseEtaitTempsEcoule);	
														
							io.sockets.emit('annonce',{type:4,annonce:messageLaBonneReponseEtaitTempsEcoule});			
							messageTitre = messageLaBonneReponseEtaitTempsEcoule;

						}
						catch(error)
						{
							console.error(error);
							socket.broadcast.emit('annonce',{type:3,annonce: ((CATEGORY_ID != 8)?"erreur détectée : ":"detected error : ") + error});
						}

			timeoutQuizContinue = setTimeout(async function(){
				cptQuestion++;
				globalChronoQuestion = 40;
				timerSecondes = setInterval(timerSec, 1000);

						try{
							
							resetAffichageClassement();
							
							var EnonceQuestionTraduit = await processLang(listeQuestions[cptQuestion].description);
							
							speech("QUESTION " + (cptQuestion + 1) + " : " + EnonceQuestionTraduit);	
							
								if (currentNumberOfBot > 0)
								{
		

										
											let nbrRepBot = nbrReponsesBot();
											var lastTimeRef = 0;
											var timeRef = 0;
											lastTimeRef = timeReflexionBot();
											
													setTimeout(function(){
											
														requestGPTByBOT("###PRESENTATOR###", EnonceQuestionTraduit, 1);
														
													}, lastTimeRef);
											
											for (i = 0; i < nbrRepBot-1; i++)
											{
												do
												{
													 timeRef = timeReflexionAdditionnelBot(i+1);
												}
												while (timeRef >= lastTimeRef-500 && timeRef <= lastTimeRef+500);
												
												lastTimeRef = timeRef;
								
													setTimeout(function(){
											
														requestGPTByBOT("###PRESENTATOR###", EnonceQuestionTraduit, 1);
														
													}, lastTimeRef);
								
											}	


											if (currentNumberOfBot >=2)
											{
												 nbrRepBot = nbrReponsesBot();
												 lastTimeRef = 0;
												 timeRef = 0;
												lastTimeRef = timeReflexionBot();
												
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", EnonceQuestionTraduit, 2);
															
														}, lastTimeRef);
												
												for (i = 0; i < nbrRepBot-1; i++)
												{
													do
													{
														 timeRef = timeReflexionAdditionnelBot(i+1);
													}
													while (timeRef >= lastTimeRef-500 && timeRef <= lastTimeRef+500);
													
													lastTimeRef = timeRef;
									
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", EnonceQuestionTraduit, 2);
															
														}, lastTimeRef);
									
												}	
											}


											if (currentNumberOfBot >=3)
											{
												 nbrRepBot = nbrReponsesBot();
												 lastTimeRef = 0;
												 timeRef = 0;
												lastTimeRef = timeReflexionBot();
												
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", EnonceQuestionTraduit, 3);
															
														}, lastTimeRef);
												
												for (i = 0; i < nbrRepBot-1; i++)
												{
													do
													{
														 timeRef = timeReflexionAdditionnelBot(i+1);
													}
													while (timeRef >= lastTimeRef-500 && timeRef <= lastTimeRef+500);
													
													lastTimeRef = timeRef;
									
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", EnonceQuestionTraduit, 3);
															
														}, lastTimeRef);
									
												}												
											}
										

								}
							
							io.sockets.emit('annonce',{type:4,annonce:((niveauDifficulte==HARD)?"🔥 ":"")+"QUESTION " + (cptQuestion + 1) + " : " + EnonceQuestionTraduit});
							messageTitre = "QUESTION " + (cptQuestion + 1) + " : " + EnonceQuestionTraduit;
							
							// conversationIA.push(new gptMessage("assistant", EnonceQuestionTraduit));
						}
						catch(error)
						{
							console.error(error);
							socket.broadcast.emit('annonce',{type:3,annonce: ((CATEGORY_ID != 8)?"erreur détectée : ":"detected error : ") + error});
						}

				timeout = false;
				
				 chronoDelaiQuestion = setTimeout(nextQuestion,40000);
				 chronoDelairPhraseHasard =  setTimeout(lancerUnePhraseAuHasard,20000);

			},7000);



		},250);		

	}
	
}

function distanceLeven(a, b)
{
    if(!a || !b) return (a || b).length;
    var m = [];
    for(var i = 0; i <= b.length; i++){
        m[i] = [i];
        if(i === 0) continue;
        for(var j = 0; j <= a.length; j++){
            m[0][j] = j;
            if(j === 0) continue;
            m[i][j] = b.charAt(i - 1) == a.charAt(j - 1) ? m[i - 1][j - 1] : Math.min(
                m[i-1][j-1] + 1,
                m[i][j-1] + 1,
                m[i-1][j] + 1
            );
        }
    }

console.log("Distance de Levenshtein entre "+ a +" et "+ b +" : "+ m[b.length][a.length]);

return m[b.length][a.length];
}


async function isMatch(msg, reponseAttendue)
{
	
	var reponseAttendueTraduite = await processLang(reponseAttendue.split("|"));
	var arrayReponse = reponseAttendueTraduite
	
	console.error("REPONSE : " + arrayReponse[0]);
	console.error("MESSAGE : " + msg);
	
	var messageSalted = "";
	var charTemp = "";
	var reponseClean = "";
	var msgClean = "";
	var indiceDeSimilarite = 999;

	// debug ---
	// arrayReponse = new Array(0);
	// arrayReponse.push("l'or");
	// ---

	for (i = 0; i < arrayReponse.length ; i++)
	{

		var reponseDecomposee = arrayReponse[i].split(" ");

		if (reponseDecomposee.length > 1 && (reponseDecomposee[0].toLowerCase().indexOf("d'") == 0 || reponseDecomposee[0].toLowerCase().indexOf("l'") == 0 || reponseDecomposee[0].toLowerCase() == "au" || reponseDecomposee[0].toLowerCase() == "en" || reponseDecomposee[0].toLowerCase() == "le" || reponseDecomposee[0].toLowerCase() == "la" || reponseDecomposee[0].toLowerCase() == "un" || reponseDecomposee[0].toLowerCase() == "une" || reponseDecomposee[0].toLowerCase() == "les" || reponseDecomposee[0].toLowerCase() == "des" || reponseDecomposee[0].toLowerCase() == "du" || reponseDecomposee[0].toLowerCase() == "the" || reponseDecomposee[0].toLowerCase() == "a" || reponseDecomposee[0].toLowerCase() == "from" || reponseDecomposee[0].toLowerCase() == "in"))
		{
			if (!(reponseDecomposee[0].toLowerCase().indexOf("d'") == 0 || reponseDecomposee[0].toLowerCase().indexOf("l'") == 0))
			{
				reponseDecomposee.splice(0,1);
				reponseClean = reponseDecomposee.join(" ");
			}
			else
			{
				reponseDecomposee[0] = reponseDecomposee[0].substring(2);
				reponseClean = reponseDecomposee.join(" ");
			}
		}
		else
		{	
			if (reponseDecomposee[0].toUpperCase().indexOf("L'") == 0 || reponseDecomposee[0].toUpperCase().indexOf("D'") == 0)
			{
				reponseClean = reponseDecomposee[0].substring(2);
			}
			else
			{
				reponseClean = arrayReponse[i];
			}
		}

		var msgDecompose = msg.split(" ");

		if (msgDecompose.length > 1 && (msgDecompose[0].toLowerCase().indexOf("d'") == 0 || msgDecompose[0].toLowerCase().indexOf("l'") == 0 || msgDecompose[0].toLowerCase() == "au" || msgDecompose[0].toLowerCase() == "en" || msgDecompose[0].toLowerCase() == "le" || msgDecompose[0].toLowerCase() == "la" || msgDecompose[0].toLowerCase() == "un" || msgDecompose[0].toLowerCase() == "une" || msgDecompose[0].toLowerCase() == "les" || msgDecompose[0].toLowerCase() == "des" || msgDecompose[0].toLowerCase() == "du"  || msgDecompose[0].toLowerCase() == "the" || msgDecompose[0].toLowerCase() == "a" || msgDecompose[0].toLowerCase() == "from" || msgDecompose[0].toLowerCase() == "in"))
		{
			
			if (!(msgDecompose[0].toLowerCase().indexOf("d'") == 0 || msgDecompose[0].toLowerCase().indexOf("l'") == 0))
			{
				msgDecompose.splice(0,1);
				msgClean = msgDecompose.join(" ");
			}
			else
			{
				msgDecompose[0] = msgDecompose[0].substring(2);
				msgClean = msgDecompose.join(" ");
			}
		}
		else
		{
			if (msgDecompose[0].toUpperCase().indexOf("L'") == 0 || msgDecompose[0].toUpperCase().indexOf("D'") == 0)
			{
				msgClean = msgDecompose[0].substring(2);
			}
			else
			{
				msgClean = msg;
			}
		
		}
		
		msgClean = msgClean.toUpperCase();
		reponseClean = reponseClean.toUpperCase();

		reponseClean = reponseClean.replace(/-/g, " ");
		msgClean = msgClean.replace(/-/g, " ");	

		if (checkIfStrictNumber(reponseClean))
		{
			
			console.log("############### 0000001 ############");
			
			indiceDeSimilarite = distanceLeven(msgClean.replace(/\D*$/g,''), reponseClean);
			if (indiceDeSimilarite == 0) return true;
		}
		else if (checkIfNumberMesure(reponseClean) && checkIfNumberMesure(msgClean))
		{
			
			console.log("############### 0000002 ############");
			
			var valeurChiffreeReponseToFind = "";
			var valeurChiffreeReponseSend = "";
			
			var decompositionValeurNommeeReponseToFind = reponseClean.split(" ");
			valeurChiffreeReponseToFind = decompositionValeurNommeeReponseToFind[0];
			decompositionValeurNommeeReponseToFind.splice(0,1);
			var nommageReponseToFind = decompositionValeurNommeeReponseToFind.join(" ");
			
			var decompositionValeurNommeeReponseSend = msgClean.split(" ");
			valeurChiffreeReponseSend = decompositionValeurNommeeReponseSend[0];
			decompositionValeurNommeeReponseSend.splice(0,1);
			var nommageReponseSend = decompositionValeurNommeeReponseSend.join(" ");		
			
			indiceDeSimilariteNommage = distanceLeven(nommageReponseToFind, nommageReponseSend);
			indiceDeSimilariteChiffrage = distanceLeven(valeurChiffreeReponseToFind, valeurChiffreeReponseSend);
	
			if (nommageReponseToFind.length >= 6)
			{
				if (indiceDeSimilariteChiffrage == 0 && indiceDeSimilariteNommage <= 3) return true;
			}
			else if (nommageReponseToFind.length < 6 && nommageReponseToFind.length > 2)
			{
				if (indiceDeSimilariteChiffrage == 0 && indiceDeSimilariteNommage <= 1) return true;
			}
			else
			{
				if (indiceDeSimilariteChiffrage == 0 && indiceDeSimilariteNommage == 0) return true;
			}
		}
		else
		{
			console.log("############### 1 ############");
			if (!checkIfNumberMesure(reponseClean))
			{
				console.log("############### 2 ############");
				indiceDeSimilarite = distanceLeven(msgClean, reponseClean);
				console.log("########### INDICE #### "+indiceDeSimilarite+" ############");
				console.log("########### CLEAN #### "+reponseClean.length+" ############");

				if ((indiceDeSimilarite <= 3 && reponseClean.length >= 8) || (indiceDeSimilarite <= 1 && reponseClean.length < 8 && reponseClean.length > 4) || (indiceDeSimilarite == 0 && reponseClean.length <= 4)) return true;
			}
		}	
				
	}
	
	return false;
	
}

function checkIfNumberMesure(str) { 
	var regex = new RegExp(/^\d{1,6} \D+$/); 
	var testResult = regex.test(str.trim()); 
	return testResult;
} 

function checkIfStrictNumber(str) { 
	var regex = new RegExp(/^\d{1,6}$/); 
	var testResult = regex.test(str.trim()); 
	return testResult;
} 

function annonceCandidats()
{
	var arrayPremiersMembresAPresenter = new Array(0);
	var dernierMembreAPresenter = "";
	
	for (i = 0; i < listeDesMembres.length-1; i++)
	{
		arrayPremiersMembresAPresenter.push(listeDesMembres[i]);
	}
	
	dernierMembreAPresenter = listeDesMembres[listeDesMembres.length-1];
	
	var messageBienvenue = ((CATEGORY_ID != 8)?"Souhaitons la bienvenue aux nouveaux joueurs " + arrayPremiersMembresAPresenter.join(", ") + " et "+ dernierMembreAPresenter +". Dés à présent, que le meilleur gagne !":"We wish welcom to the new players " + arrayPremiersMembresAPresenter.join(", ") + " and "+ dernierMembreAPresenter +". Now, That the best win !");
	
	speech(messageBienvenue);	
	
	io.sockets.emit('annonce',{type:98,annonce:messageBienvenue});
	
}

function listeCandidatsLangageNaturel()
{
	var arrayPremiersMembresAPresenter = new Array(0);
	var dernierMembreAPresenter = "";
	
	for (i = 0; i < listeDesMembres.length-1; i++)
	{
		arrayPremiersMembresAPresenter.push(listeDesMembres[i]);
	}
	
	dernierMembreAPresenter = listeDesMembres[listeDesMembres.length-1];
	
	return arrayPremiersMembresAPresenter.join(", ") + " et "+ dernierMembreAPresenter;

}

function processClearBot()
{
		if (currentNumberOfBot > 0)
		{
				for (var i = listeDesMembresAvecDetail.length-1; i >= 0; i--) {
				  if (listeDesMembresAvecDetail[i].id.indexOf("bot") > -1) {
					listeDesMembresAvecDetail.splice(i, 1);
				  }
				}
				
				for (var i = listeDesMembres.length-1; i >= 0; i--) {
				  if (listeDesMembres[i].indexOf("BOT_") > -1) {
					listeDesMembres.splice(i, 1);	
				  }
				}
				
				for (var i = allClients.length-1; i >= 0; i--) {
				  if (allClients[i] == null) {
					allClients.splice(i, 1);
				  }
				}	

				// Supprimer les références aux arrays internes
				conversationIABots.splice(0, conversationIABots.length);				

				for (let i = 0; i < 3; i++) {
				  conversationIABots.push([]);
				}

				currentNumberOfBot = 0;
				decoBot();
		}
}

function decoBot()
{
		try
		{

				var messageQuit = ((CATEGORY_ID != 8)?"Les bots viennent de nous quitter...":"Bots just leave...");
				
				speech(messageQuit);
				io.sockets.emit('annonce',{type:98,annonce: messageQuit});
					
				var messageDecoBotDiscord = ((CATEGORY_ID != 8)?":red_circle: les bots viennent de quitter la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length) + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CONFIG.eternaltwin.role:":red_circle: the bots has just leave the game "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length) + "** player"+((listeDesMembres.length>1)?"s":"")+" online | "+CONFIG.eternaltwin.role)	
					
				Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(messageDecoBotDiscord);
				
				updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length)

				if (listeDesMembres.length > 0)
					refreshListeSlotsInitial();

		}
		catch(error)
		{
			console.error("DISCONNECT ERROR : " + error);	
		}
}

async function newConnection(socket)
{
	console.log('new connection' + socket.id);
	socket.on('message', messageGet);
	socket.on('currentWriting', currentWriting);
	
	lastSocket = socket;
	socket.on('addMember', addMember);
	socket.on('getQuestionIDCurrent', getCurrentIDQuestion);
	socket.on('alive', isAlive);
	socket.on('applaudir', applaudir);
	
	notifyChrono(globalChronoQuestion, 40);

		socket.on("disconnect", async function(){	

		try
		{
				var i = allClients.indexOf(socket);
				
				console.error("CURRENT ID SOCKET DECO : "+i);
				console.error("TAILLE ARRAY SOCKETS : "+allClients.length);
				
				console.log(listeDesMembresAvecDetail[i].name + " is now offline !");
				listeDesMembresAvecDetailOffline.push(listeDesMembresAvecDetail[i]);
				listeDesMembresOffline.push(listeDesMembres[i]);
				
				var pseudoDeco = listeDesMembres[i];
				var uuidDeco = listeDesMembresAvecDetail[i].id;
				var IDDiscordDeco = await getDiscordIDByMembre(uuidDeco);
						
				var messageQuit = ((CATEGORY_ID != 8)?"vient de nous quitter...":"has just leave...");
				var messageDeco = ((CATEGORY_ID != 8)?"s'est déconnecté":"disconnected");
				
				socket.broadcast.emit('annonce',{type:2,annonce: listeDesMembres[i] + (listeDesMembresAvecDetail[i].version > 1?"#" + listeDesMembresAvecDetail[i].version:"") +  " "+messageDeco+" !"});			
				speech(pseudoDeco + " " + messageQuit);
				io.sockets.emit('annonce',{type:98,annonce:pseudoDeco + " " + messageQuit});
			
				allClients.splice(i, 1);
				listeDesMembres.splice(i, 1);
				listeDesMembresAvecDetail.splice(i, 1);
					
				var messageDecoBotDiscord = ((CATEGORY_ID != 8)?":red_circle: `"+pseudoDeco+"` "+ (IDDiscordDeco=="<@unknown>"||IDDiscordDeco=="<@>"?"":":arrow_forward:"+IDDiscordDeco) +" vient de quitter la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length) + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CONFIG.eternaltwin.role:":red_circle: `"+pseudoDeco+"` "+ (IDDiscordDeco=="<@unknown>"||IDDiscordDeco=="<@>"?"":":arrow_forward:"+IDDiscordDeco) +" has just leave the game "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length) + "** player"+((listeDesMembres.length>1)?"s":"")+" online | "+CONFIG.eternaltwin.role)	
					
				Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(messageDecoBotDiscord);
				
				updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length)

				currentWriting({uuid:uuidDeco, state:0});

				console.log(listeDesMembres.length + " joueur(s) encore connecté...");
				
				var membresActifs = false;
				
				for (j=0; j < listeDesMembresAvecDetail.length;j++)
				{
					if (listeDesMembresAvecDetail[j].id.indexOf("bot") == -1) membresActifs = true;
				}
				
				if (listeDesMembres.length > 0 && membresActifs)
					refreshListeSlotsInitial();
				else
					init(true);
		}
		catch(error)
		{
			console.error("DISCONNECT ERROR : " + error);	
		}

	});

	function applaudir(data)
	{
		var clapMoi = ((CATEGORY_ID != 8)?"Vous applaudissez":"you are applauding");
		var clapLui = ((CATEGORY_ID != 8)?"applaudit de tout son coeur":"applauds with all his heart");
		
		
		io.sockets.emit('applaudissement_send',{type:555, user:data.id, annonce: clapMoi + " ! 👏"});	
		socket.broadcast.emit('applaudissement_get',{type:555, user:data.id, annonce: data.pseudo + " "+ clapLui +" ! 👏"});
	}
	
	async function messageGet(data)
	{
		console.log(data.pseudo + " : " + data.message);
		if (data.message.indexOf('giphy.com/media/') > -1)
		{
			var message = data.message;
		}
		else
		{
			var message = data.message.replace(regexHTML,"");
		}
		
		/*
		versionPseudo = getMembreDetailByID(data.id).version;
		
		if (versionPseudo > 1)
		{
			data.pseudo = data.pseudo + "#" + versionPseudo;
		}
		*/
		
		reponseAttendue = "3598bde5-abe2-4703-a481-384c5276f2b7";
		
		var replace255 = String.fromCharCode(160);
		var regex255 = new RegExp(replace255,"gi");
		
		if (message.toUpperCase().indexOf("P0RN") > -1 || message.toUpperCase().indexOf("PORN") > -1 || message.toUpperCase().indexOf("XHAMSTER") > -1 || message.toUpperCase().indexOf("X-HAMSTER") > -1 || message.toUpperCase().indexOf("XNXX") > -1 || message.toUpperCase().indexOf("REDTUBE") > -1 || message.toUpperCase().indexOf("XXX") > -1)
		{
			if (CATEGORY_ID != 8)
				io.sockets.emit('annonce',{type:7,annonce: "Le message saisi par "+ data.pseudo + " a été bloqué, celui-ci ne respecte pas la charte du site !"});
			else
				io.sockets.emit('annonce',{type:7,annonce: "The message entered by "+ data.pseudo + " has been blocked, it does not comply with the site's rules!"});
		}
		else if (linkify.find(message).length > 0 && data.message.indexOf('giphy.com/media/') == -1)
		{
			if (CATEGORY_ID != 8)
				io.sockets.emit('annonce',{type:7,annonce: "L'URL saisie par "+ data.pseudo + " a été bloquée, merci de ne pas envoyer de liens !"});
			else
				io.sockets.emit('annonce', {type: 7, annonce: "The URL entered by " + data.pseudo + " has been blocked, please do not send links!"});
		}
		else if (linkify.find(message.replace(/ /gi, '').replace(regex255, '')).length > 0  && data.message.indexOf('giphy.com/media/') == -1)
		{
			if (CATEGORY_ID != 8)
				io.sockets.emit('annonce',{type:7,annonce: "Il semblerait qu'une URL se soit cachée dans le message de "+ data.pseudo + ", celle-ci a été bloquée par mesure de précaution !"});
			else
				io.sockets.emit('annonce',{type:7,annonce: "It seems that a URL has been hidden in the message from "+ data.pseudo + ", it has been blocked as a precautionary measure!"});
		}
		else
		{
		
		if (cptQuestion >= 0 && cptQuestion < listeQuestions.length) reponseAttendue = listeQuestions[cptQuestion].reponse;
			
		if (message.indexOf("/start") == 0)
		{

			if (listeDesMembres.length >= 2 && !isStart)
			{
					//INIT DE LA DIFFICULTY
					var startingCommande = message.split(" ");
					
					if (startingCommande.length == 1)
					{
						echellePoints = [10,5,2,2];
						niveauDifficulte = MEDIUM;
					}
					else if (startingCommande.length > 1)
					{
						if (startingCommande[1] == "hard")
						{
							echellePoints = [17,10,5,5];
							niveauDifficulte = HARD;
						}
						else if (startingCommande[1] == "easy")
						{
							echellePoints = [6,2,1,1];
							niveauDifficulte = EASY;
						}
						else 
						{
							echellePoints = [10,5,2,2];
							niveauDifficulte = MEDIUM;
						}
					}
				
				// DEBUT
				
				init(false);
				
				isStart = true;
				isFinish = false;
				clearInterval(chronoPhSortieHasard);
				listeRepondantCorrect = new Array();
				timeout = false;				

				timeoutDebutPartie = setTimeout(async function(){

					if (cptQuestion == -1)
					{
						cptQuestion++;
						
						for (i = 0; i < listeDesMembresAvecDetail.length; i++)
						{
							listeDesMembresAvecDetail[i].points = 0;
						}
						
						refreshListeSlotsInitial();
						
						var beginPartieMessage = ((CATEGORY_ID != 8)?"Une partie en mode "+ ((niveauDifficulte==MEDIUM)?"normal":((niveauDifficulte==HARD)?"difficile":"facile")) +" va commencer !":"A game in "+ ((niveauDifficulte==MEDIUM)?"medium":((niveauDifficulte==HARD)?"hard":"easy")) +" mode will begin !");
						
						speech(beginPartieMessage);	
						
						io.sockets.emit('annonce',{type:4,annonce:beginPartieMessage});
						identifiantPartie = getUUID();
						
						
						timeoutAnnonceCandidats = setTimeout(async function(){
							
							annonceCandidats();
							

							// Placer les questions par ordre de difficulté
								
								listeQuestions.sort(function (a, b) {
									return a.difficulty - b.difficulty;
								});
								
							var DelaiAvantPremiereQuestion = 3000 + (listeDesMembresAvecDetail.length * 3000); 

							timeoutAnnonceQuestion = setTimeout(async function(){
								
								enPleinQuiz = true;
								
								resetAffichageClassement();
								
								var questionTraduite = await processLang(listeQuestions[cptQuestion].description);
								
								speech("QUESTION " + (cptQuestion+1) + " : " + questionTraduite);	
													

								if (currentNumberOfBot > 0)
								{
		
										
											
										
											let nbrRepBot = nbrReponsesBot();
											var lastTimeRef = 0;
											var timeRef = 0;
											
				
												lastTimeRef = timeReflexionBot();
												
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", questionTraduite, 1);
															
														}, lastTimeRef);
												
												for (i = 0; i < nbrRepBot-1; i++)
												{
													do
													{
														 timeRef = timeReflexionAdditionnelBot(i+1);
													}
													while (timeRef >= lastTimeRef-500 && timeRef <= lastTimeRef+500);
													
													lastTimeRef = timeRef;
									
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", questionTraduite, 1);
															
														}, lastTimeRef);
									
												}	

											

											if (currentNumberOfBot >=2)
											{
												 nbrRepBot = nbrReponsesBot();
												 lastTimeRef = 0;
												 timeRef = 0;
												lastTimeRef = timeReflexionBot();
												
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", questionTraduite, 2);
															
														}, lastTimeRef);
												
												for (i = 0; i < nbrRepBot-1; i++)
												{
													do
													{
														 timeRef = timeReflexionAdditionnelBot(i+1);
													}
													while (timeRef >= lastTimeRef-500 && timeRef <= lastTimeRef+500);
													
													lastTimeRef = timeRef;
									
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", questionTraduite, 2);
															
														}, lastTimeRef);
									
												}
											}

											if (currentNumberOfBot >=3)
											{

												 nbrRepBot = nbrReponsesBot();
												 lastTimeRef = 0;
												 timeRef = 0;
												lastTimeRef = timeReflexionBot();
												
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", questionTraduite, 3);
															
														}, lastTimeRef);
												
												for (i = 0; i < nbrRepBot-1; i++)
												{
													do
													{
														 timeRef = timeReflexionAdditionnelBot(i+1);
													}
													while (timeRef >= lastTimeRef-500 && timeRef <= lastTimeRef+500);
													
													lastTimeRef = timeRef;
									
														setTimeout(function(){
												
															requestGPTByBOT("###PRESENTATOR###", questionTraduite, 3);
															
														}, lastTimeRef);
									
												}											
											}
										

								}
				
								io.sockets.emit('annonce',{type:4,annonce:((niveauDifficulte==HARD)?"🔥 ":"")+"QUESTION " + (cptQuestion+1) + " : " + questionTraduite});
								messageTitre = "QUESTION " + (cptQuestion+1) + " : " + questionTraduite;
								globalChronoQuestion = 40;							
								timerSecondes = setInterval(timerSec, 1000);
									
									
								chronoDelaiQuestion = setTimeout(nextQuestion,40000);
			
													
							},DelaiAvantPremiereQuestion);
						
						},2500);
						

						
						
					}
				},1300);


			}
			else
			{
				if (listeDesMembres.length < 2) 
				{
					var messageObligationPersonnes = ((CATEGORY_ID != 8)?"Il faut plusieurs personnes pour qu'une partie débute...":"It takes multiple people for a game to start..."); 
					
					io.sockets.emit('annonce',{type:5,annonce:messageObligationPersonnes});
					
				}
				else if (isStart) 
				{
					// io.sockets.emit('annonce',{type:98,annonce:"La partie est déjà lancée !"});
				}
			}
		}
		else if (message.indexOf("/addbot") == 0)
		{
			if (currentNumberOfBot < 3)
			{
				var databot = 
				{
						id: "bot"+(currentNumberOfBot+1),
						pseudo: "BOT_"+(currentNumberOfBot+1),
						client_id : "bot"+(currentNumberOfBot+1)
				};
				
				lastSocket = null;
				addMember(databot);
				currentNumberOfBot++;
			}
			
		}
		else if (message.indexOf("/clearbot") == 0)
		{
			processClearBot();
		}
		else if (!timeout && (!isFinish && isStart && (await isMatch(message.normalize("NFD").replace(/[\u0300-\u036f]/g, ""), reponseAttendue.normalize("NFD").replace(/[\u0300-\u036f]/g, "")) && cptQuestion >= 0)))
		{
			
				var pts = 0;

				if (listeRepondantCorrect.lastIndexOf(data.pseudo) == -1)
				{
					cptReponseDonnee++;
					
					switch (cptReponseDonnee)
					{
						case 1 : pts = echellePoints[0]; break;
						case 2 : pts = echellePoints[1]; break;
						case 3 : pts = echellePoints[2]; break;
						case 4 : pts = echellePoints[3]; break;
						default : pts = 0;					
					}

					if (pts > 0)
					{

						try
						{
							var indexToAddPoints = listeDesMembresAvecDetail.map(function(e) { return e.id; }).indexOf(data.id); //listeDesMembres.indexOf(data.pseudo);
							listeDesMembresAvecDetail[indexToAddPoints].points += pts;
							listeDesMembresAvecDetail[indexToAddPoints].lastGame = identifiantPartie;
							
							notifyPointsWin(new itemClassement(data.id, pts));
							notifyOrdreBonneReponse({uuid:data.id, ordre:cptReponseDonnee});
							
							var correctToMe = ((CATEGORY_ID != 8)?"Bonne réponse ! "+pts+" points gagnés !":"Good answer ! "+pts+" points win !");
							var correctToYou = ((CATEGORY_ID != 8)?data.pseudo + " a trouvé la bonne réponse ! "+pts+" points gagnés !":data.pseudo + " found the good answer ! "+pts+" points win !");
							
							io.sockets.emit('annonce_user',{type:3, user:data.id, annonce:correctToMe });	
							socket.broadcast.emit('annonce',{type:3, annonce:correctToYou });
							listeRepondantCorrect.push(data.pseudo);
							refreshListeSlots();
							
							// Stoppage si tout le monde ou 4 personnes ont trouvé
								
								if (listeRepondantCorrect.length == 4 || listeRepondantCorrect.length == listeDesMembres.length)
								{
									clearTimeout(chronoDelaiQuestion);
									clearTimeout(chronoDelairPhraseHasard);

									timeout = true;
									clearInterval(timerSecondes);
									notifyChrono(0,40);
								
									var allReponseOK = ((CATEGORY_ID != 8)?"Toutes les réponses ont été trouvées...":"The answers have all been found...");
								
									speech(allReponseOK);
									io.sockets.emit('annonce',{type:4,annonce:allReponseOK});
	
									timeoutNextQuestion = setTimeout(nextQuestion, 6500);
								}
							
							
							
						}
						catch(error)
						{
							console.error(error);
							socket.broadcast.emit('annonce',{type:3,annonce: ((CATEGORY_ID != 8)?"erreur détectée : ":"detected error : ") + error});
						}

					}
				}
				else // Cas réponse déjà trouvée par l'utilisateur qui écris un message
				{
					io.sockets.emit('annonce_user',{type:3, user:data.id, annonce: ((CATEGORY_ID != 8)?"Vous avez déjà trouvé la bonne réponse !":"You already found the good answer !")});
				}
		}
		else
		{

				if (message.indexOf("/speak") == 0)
				{
					speech(message.replace("/speak",""));			
				}				
				else if (message.indexOf("/kick ") == 0)
				{
					var commande = message.split(" ");
					kick(commande[1]);
				}
				else
				{
					
					var tempMessage = message;				
					var motsMessages = tempMessage.split(" ");
					var indexInsulte = -1;
					var isCensured = false;
					
					for (i = 0; i < motsMessages.length; i++)
					{
						indexInsulte = insultes.indexOf(motsMessages[i].normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase());
						
						if (indexInsulte > -1)
						{
							isCensured = true;
							var censure = "";
							for (j = 0; j < insultes[indexInsulte].length; j++)
							{
								censure += "*";
							}
							motsMessages[i] = censure;	
						}
					}
					
					data.message = motsMessages.join(" ");									
					
					socket.broadcast.emit('message', data);
					
					if ((data.message.toLowerCase().indexOf("bot_1") > -1 || data.message.toLowerCase().indexOf("bot1") > -1 || data.message.toLowerCase().indexOf("bot 1") > -1) && currentNumberOfBot >= 1)
					{
						requestGPTByBOT(data.pseudo, data.message, 1);
					}
					
					if ((data.message.toLowerCase().indexOf("bot_2") > -1 || data.message.toLowerCase().indexOf("bot2") > -1 || data.message.toLowerCase().indexOf("bot 2") > -1) && currentNumberOfBot >= 2)
					{
						requestGPTByBOT(data.pseudo, data.message, 2);
					}
					
					if ((data.message.toLowerCase().indexOf("bot_3") > -1 || data.message.toLowerCase().indexOf("bot3") > -1 || data.message.toLowerCase().indexOf("bot 3") > -1) && currentNumberOfBot >= 3)
					{
						requestGPTByBOT(data.pseudo, data.message, 3);
					}						

					let volonteVoirImage = (data.message.indexOf("Montre-moi") == 0 || data.message.indexOf("montre-moi") == 0 || data.message.indexOf("Show me") == 0 || data.message.indexOf("show me") == 0 || data.message.indexOf("Laat me") == 0 || data.message.indexOf("laat me") == 0);
					
					if ((data.message.length >= 3 && data.ia) && !volonteVoirImage)
					{
						requestGPT(data.pseudo, data.message);
					}
					
					if (data.message.length >= 25 && volonteVoirImage && data.ia)
					{
						requestDalle(data.pseudo, data.message);
					}
					else if (volonteVoirImage && data.ia && !enPleinQuiz)
					{
						var excuseMontrerImage1 = ((CATEGORY_ID != 8)?"La demande de "+data.pseudo+" est trop courte !":"The request of "+data.pseudo+" is too short !");
						
						io.sockets.emit('annonce',{type:7, annonce:excuseMontrerImage1 });						
						
					}
					else if (volonteVoirImage && data.ia && enPleinQuiz)
					{
						var excuseMontrerImage2 = ((CATEGORY_ID != 8)?data.pseudo+", les représentation visuelle ne peuvent être montrée qu'en dehors d'un quiz !":data.pseudo+", the visual representations can be show only out of a quiz !");
						
						io.sockets.emit('annonce',{type:7, annonce:excuseMontrerImage2 });
					}
					
				}				
				
			
			
		}
		
	}
		
		
	}
	
	async function addMember(data)
	{

		var clientID = data.client_id; 
		var id = data.id;
		var pseudo = data.pseudo;
		var socket_ = lastSocket;
	
		var clientDiscordID = await getDiscordIDByMembre(id);
	
		console.log("ID DISCORD = "+clientDiscordID);
	
		var pseudo = pseudo.replace(/ /g, "");
	
		console.log("Adding member : " + pseudo);
		var re = "";
		var dataAckMaxUser = {msg:"ERROR_TOOMANYUSER"};
		var dataAckExist = {msg:"ERROR_ALREADYEXIST"};
		var dataAckOK = {msg:"OK"};
		
		var indexMemberFound = listeDesMembresAvecDetail.map(function(e) { return e.id; }).indexOf(id);
		
		if (listeDesMembres.length == 8)
		{
			console.log("REFUS 8 MAX");
			socket.emit('ack', dataAckMaxUser);
			socket.disconnect(true);
		}
		else if (indexMemberFound > -1)
		{
			
			console.log("EJECTION MEMBRE EXISTE - CLIENTID="+listeDesMembresAvecDetail[indexMemberFound].clientID);
			io.sockets.emit('forcer_ejection', {client_id:listeDesMembresAvecDetail[indexMemberFound].clientID});
			
			setTimeout(async function(){
				
							console.log("ACCES AUTORISE");
							socket.emit('ack', dataAckOK);
							
							try
							{
								var idOfficielOffline = listeDesMembresAvecDetailOffline.map(function(e) { return e.id; }).indexOf(id);
							}
							catch (ex)
							{
								var idOfficielOffline = -1;
							}
							
							if (idOfficielOffline > -1)
							{
								re = "re";
								var returnMembre = listeDesMembresAvecDetailOffline[idOfficielOffline];
								returnMembre.clientID = clientID;
								returnMembre.tokentime = Math.floor(Date.now() / 1000);
								listeDesMembresOffline.splice(i, 1);
								listeDesMembresAvecDetailOffline.splice(i, 1);
								
								if (returnMembre.lastGame != identifiantPartie)
								{
									returnMembre.points = 0;
								}
								
								listeDesMembresAvecDetail.push(returnMembre);
								allClients.push(socket_);										
							}
							else
							{	

								var identicalNickname = 1;
								for (i = 0; i < listeDesMembres.length; i++)
								{
									if (listeDesMembres[i] == pseudo)
									{
										identicalNickname++;
									}
								}
						
								listeDesMembresAvecDetail.push(new membre(id, pseudo,Math.floor(Date.now() / 1000),0,0,Math.floor(Date.now()), 0, 0, identicalNickname, identifiantPartie, clientID));
								allClients.push(socket_);
							}
							
							listeDesMembres.push(pseudo);
							notifyChrono(globalChronoQuestion,40);
							
							getMembreDetailByID();
							
							var estConnectedNow = ((CATEGORY_ID != 8)?" s'est " + re + "connecté !":" is " + re + "conneced !"); 
							
							countMember = listeDesMembres.length;
							socket.broadcast.emit('annonce',{type:1,annonce:pseudo + (getMembreDetailByID(id).version > 1?"#" + getMembreDetailByID(id).version:"") + estConnectedNow});
							
							if (countMember > 1)
							{
								if (CATEGORY_ID != 8)
								{
									speech( (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo ));
									io.sockets.emit('annonce',{type:98,annonce: (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo )  });
								}
								else
								{
									speech((re == 're' ? pseudo + ' is back with us' : 'Let\'s welcome ' + pseudo)); 
									io.sockets.emit('annonce',{type:98,annonce:(re == 're' ? pseudo + ' is back with us' : 'Let\'s welcome ' + pseudo)});
								}
							}
							else
							{
								var phraseAccueil = (await phraseAttenteAuHasard()).replace("###PSD40###",pseudo);
								phraseAccueil = await processLang(phraseAccueil);
								
								speech(phraseAccueil);
								io.sockets.emit('annonce',{type:98,annonce: phraseAccueil});
								
								setTimeout(async function(){
									
								var phrase0 = await processLang(listePhrasesInutiles[0]);	
									
									speech(phrase0);
									io.sockets.emit('annonce',{type:98,annonce:phrase0});
									
								},5800);
								
							}							
							
							if (CATEGORY_ID != 8)
								Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(":green_circle: `"+pseudo+"` "+ (clientDiscordID=="<@unknown>"||clientDiscordID=="<@>"?"":":arrow_forward:"+clientDiscordID) +" vient de rejoindre une partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + listeDesMembres.length + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CONFIG.eternaltwin.role);
							else
								Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(":green_circle: `"+pseudo+"` "+ (clientDiscordID=="<@unknown>"||clientDiscordID=="<@>"?"":":arrow_forward:"+clientDiscordID) +" just join a game "+ CATEGORY +" on https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + listeDesMembres.length + "** player"+((listeDesMembres.length>1)?"s":"")+" online | "+CONFIG.eternaltwin.role);
							
							updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length);
							refreshListeSlotsInitial();	

							setTimeout(function(){
				
									io.sockets.emit('refresh_status_writing',{id:currentWritingList});
								
							},800);
							
							timeoutRefreshTopMessage = setTimeout(function(){io.sockets.emit('refreshTopMessage',{nickname:pseudo, msg:messageTitre});},2000);
						
				
			},1500);

		}
		else
		{	
			// [CODE DUPLIQUE __^]
			console.log("ACCES AUTORISE");
			socket.emit('ack', dataAckOK);
			
			try
			{
				var idOfficielOffline = listeDesMembresAvecDetailOffline.map(function(e) { return e.id; }).indexOf(id);
			}
			catch (ex)
			{
				var idOfficielOffline = -1;
			}
			
			if (idOfficielOffline > -1)
			{
				re = "re";
				var returnMembre = listeDesMembresAvecDetailOffline[idOfficielOffline];
				returnMembre.clientID = clientID;
				returnMembre.tokentime = Math.floor(Date.now() / 1000)
				listeDesMembresOffline.splice(i, 1);
				listeDesMembresAvecDetailOffline.splice(i, 1);
				
				if (returnMembre.lastGame != identifiantPartie)
				{
					returnMembre.points = 0;
				}
				
				listeDesMembresAvecDetail.push(returnMembre);
				allClients.push(socket_);
			}
			else
			{	

				var identicalNickname = 1;
				for (i = 0; i < listeDesMembres.length; i++)
				{
					if (listeDesMembres[i] == pseudo)
					{
						identicalNickname++;
					}
				}
		
				listeDesMembresAvecDetail.push(new membre(id, pseudo,Math.floor(Date.now() / 1000),0,0,Math.floor(Date.now()), 0, 0, identicalNickname, identifiantPartie, clientID));
				allClients.push(socket_);
			}
			
			listeDesMembres.push(pseudo);
			notifyChrono(globalChronoQuestion,40);
			
			getMembreDetailByID();
			
			countMember = listeDesMembres.length;
			socket.broadcast.emit('annonce',{type:1,annonce:pseudo + (getMembreDetailByID(id).version > 1?"#" + getMembreDetailByID(id).version:"") + " s'est " + re + "connecté !"});
			
			if (countMember > 1)
			{
				if (CATEGORY_ID != 8)
				{	
					speech( (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo ));
					io.sockets.emit('annonce',{type:98,annonce: (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo )  });
				}
				else
				{
					speech( (re == "re"? pseudo + " is back":"Welcom to " + pseudo ));
					io.sockets.emit('annonce',{type:98,annonce: (re == "re"? pseudo + " is back":"Welcom to " + pseudo )  });
				}
			}
			else
			{
				var phraseAccueil = (await phraseAttenteAuHasard()).replace("###PSD40###",pseudo);
				speech( phraseAccueil);
				io.sockets.emit('annonce',{type:98,annonce: phraseAccueil});
				
				var accueilDiscordMsg = await processLang(listePhrasesInutiles[0]);
				
				setTimeout(function(){	
						speech(accueilDiscordMsg);				
						io.sockets.emit('annonce',{type:98,annonce:accueilDiscordMsg});									
				},5800);
			}
			
			if (CATEGORY_ID != 8)
				Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(":green_circle: `"+pseudo+"` "+ (clientDiscordID=="<@unknown>"||clientDiscordID=="<@>"?"":":arrow_forward:"+clientDiscordID) +" vient de rejoindre une partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + listeDesMembres.length + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CONFIG.eternaltwin.role);
			else
				Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(":green_circle: `"+pseudo+"` "+ (clientDiscordID=="<@unknown>"||clientDiscordID=="<@>"?"":":arrow_forward:"+clientDiscordID) +" just join a game "+ CATEGORY +" on https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + listeDesMembres.length + "** player"+((listeDesMembres.length>1)?"s":"")+" online | "+CONFIG.eternaltwin.role);

			
			updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length)
			refreshListeSlotsInitial();	
			
			setTimeout(function(){
				
				io.sockets.emit('refresh_status_writing',{id:currentWritingList});
				
			},800);
			
			
			timeoutRefreshTopMessage = setTimeout(function(){io.sockets.emit('refreshTopMessage',{nickname:pseudo, msg:messageTitre});},2000);
		
		}
		
	}
	
	function getMembreDetailByID(uuid)
	{
		for (i = 0; i < listeDesMembresAvecDetail.length; i++)
		{
			if (listeDesMembresAvecDetail[i].id == uuid)
			{
				return listeDesMembresAvecDetail[i];
			}
		}
		
		return null;
	}
	
	// ici étatit les fonctions de slot
	
	function getCurrentIDQuestion(data)
	{
		
		if (enPleinQuiz)
		{
			if (cptQuestion > -1)
			{
				if (listeQuestions[cptQuestion] != undefined)
				{
					io.sockets.emit('suspect_id_question',{idQuestion:listeQuestions[cptQuestion].id, libelleQuestion:listeQuestions[cptQuestion].description ,uuid:data.uuid});
				}
				else
				{
					io.sockets.emit('suspect_id_question',{idQuestion:-2, libelleQuestion:"[ERREUR]" ,uuid:data.uuid});
				}
			}
			else
			{
				io.sockets.emit('suspect_id_question',{idQuestion:-2, libelleQuestion:"[ERREUR]" ,uuid:data.uuid});
			}
		}
		else
		{
			io.sockets.emit('suspect_id_question',{idQuestion:-3, libelleQuestion:"[QUIZ_NOT_CURRENT]" ,uuid:data.uuid});
		}
			
	}
	
	function isAlive(data)
	{	
		listeDesMembresRestant.push(data.pseudo);	
		console.log(data.pseudo + " est encore en ligne...");	
	}

}

	function purgeMember(i)
	{
		// Ne fonctionne pas bien (kick)
		/*
		if (listeDesMembres[i] !== undefined)
		{
			
			if (listeDesMembresAvecDetailOffline.indexOf(listeDesMembresAvecDetail[i]) == -1)
			{
				listeDesMembresAvecDetailOffline.push(listeDesMembresAvecDetail[i]);
				listeDesMembresOffline.push(listeDesMembres[i]);
				
				var pseudoDeco = listeDesMembres[i];
			
			// // Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_PATBOT")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_PATBOT")).send(":red_circle: `"+pseudoDeco+"` vient d'être kické de la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length - 1) + "** joueur"+(((listeDesMembres.length - 1)>1)?"s":"")+" en ligne | "+CredentialsDiscord.getKey("ROLE_DIRECTQUIZ_PATBOT"));
			// Client.guilds.cache.get(CONFIG.eternaltwin.guild_id).channels.cache.get(CONFIG.eternaltwin.channel_id).send(":red_circle: `"+pseudoDeco+"` vient d'être kické de la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length - 1) + "** joueur"+(((listeDesMembres.length - 1)>1)?"s":"")+" en ligne | "+CONFIG.eternaltwin.role);
			updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length-1)			
		
			allClients.splice(i, 1);
			listeDesMembres.splice(i, 1);
			listeDesMembresAvecDetail.splice(i, 1);
					
			io.sockets.emit('kick',{id:listeDesMembresAvecDetail[i].id});		
					
			console.log(allClients.length + " joueur(s) encore connecté...");
				
			if (allClients.length > 0)
				refreshListeSlots();
			else
				init(true);	

			}		
			
		}
		*/
	}

	function kick(pseudo)
	{
		var arrayPseudo = pseudo.split("#");

		if (arrayPseudo.length > 1)
		{	
			for (i = 0; i < listeDesMembres.length; i++)
			{
				
					if (listeDesMembresAvecDetail[i].name.toUpperCase() == arrayPseudo[0].toUpperCase() && listeDesMembresAvecDetail[i].version+"" == arrayPseudo[1])
					{
						io.sockets.emit('annonce',{type:5,annonce:listeDesMembres[i] + (listeDesMembresAvecDetail[i].version > 1?"#"+listeDesMembresAvecDetail[i].version:"") + ((CATEGORY_ID != 8)?" a été expulsé de la partie...":" has been kicked from the game...")});
						//listeDesMembresAvecDetail[i].points = 0;
						io.sockets.emit('kick',{id:listeDesMembresAvecDetail[i].id});
						
						purgeMember(i);
					}

			}
		}
		else
		{
			for (i = 0; i < listeDesMembres.length; i++)
			{
				
					if (listeDesMembresAvecDetail[i].name.toUpperCase() == arrayPseudo[0].toUpperCase())
					{
						io.sockets.emit('annonce',{type:5,annonce:listeDesMembres[i] + ((CATEGORY_ID != 8)?" a été expulsé de la partie...":" has been kicked from the game...")});
						//listeDesMembresAvecDetail[i].points = 0;
						io.sockets.emit('kick',{id:listeDesMembresAvecDetail[i].id});

						purgeMember(i);
					}

			}
		}
	}

	async function notifyChrono(restant_, total_)
	{
	
		if (restant_ == 6)
		{			
				var phrase5Secondes = await phrase5secondesAuHasard();
				speech(phrase5Secondes);	
				io.sockets.emit('annonce',{type:98,annonce:phrase5Secondes});
		}
		
		io.sockets.emit('chrono',{total:total_,restant:restant_});
	}
	
	function resetAffichageClassement()
	{
		var data = "reset";
		io.sockets.emit('reset_affichage_classement',data);
	}
	
	function notifyPointsWin(data)
	{
		io.sockets.emit('notify_points',data);
	}
	
	function notifyOrdreBonneReponse(data)
	{
		io.sockets.emit('notify_ordre',data);
	}
	
	function currentWriting(data)
	{
		var i = currentWritingList.indexOf(data.uuid)
		
		if (data.state == 1)
		{
			if (i == -1) currentWritingList.push(data.uuid)
		}
		else
		{
			if (i != -1) currentWritingList.splice(i, 1);	
		}

		if (lastEmprunteWritingCurrent != JSON.stringify(currentWritingList))
		{
			lastEmprunteWritingCurrent = JSON.stringify(currentWritingList);
			io.sockets.emit('refresh_status_writing',{id:currentWritingList});
		}

	}
	
	function updateScoreInDB()
	{
		for (i = 0; i < listeDesMembresAvecDetail.length; i++)
		{
			
			try 
			{
				const params = new URLSearchParams();
				params.append('uuid', listeDesMembresAvecDetail[i].id);
				params.append('points', listeDesMembresAvecDetail[i].points);
				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/directquiz.niko.ovh/dev/ajax/score101.php',
				  data: params
				}).then(function (response) {
						console.log("Membre \""+ listeDesMembresAvecDetail[i].name + "\" --> Score enregistré avec succès dans la DB !");
				  })
				  .catch(function (error) {
					console.log(error);
				  });
			}
			catch (error)
			{
				console.error(error);
			}
		}
	}
	
async function getDiscordIDByMembre(uuidMembre) {
 
 try {
    const params = new URLSearchParams();
    params.append('uuid', uuidMembre);

    const response = await axios.post('https://www.directquiz.org/niko.ovh/directquiz89/getDiscordID.php', params);
	return response.data;
  } catch (error) {
    console.log(error);
  }
  
}

	function timeReflexionBot()
	{
		var min = 6000;
		var max = 15000;
		var randomNumber = Math.random() * ((max+1) - min) + min;
		return Math.floor(randomNumber);
	}

	function nbrReponsesBot()
	{
		var min = 1;
		var max = 3;
		var randomNumber = Math.random() * ((max+1) - min) + min;
		return Math.floor(randomNumber);
	}
	
	function timeReflexionAdditionnelBot(etape)
	{
		var add = 4000*etape;
		
		var min = 7000+add;
		var max = 16000+add;
		var randomNumber = Math.random() * ((max+1) - min) + min;
		return Math.floor(randomNumber);
	}
	
	function botDonneBonneReponse()
	{
		var max = 3;

		switch (niveauDifficulte)
		{
			case "EASY" : max = 3; break; 
			case "MEDIUM" : max = 5; break;
			case "HARD" : max = 7; break;
		}
		
		var min = 1;
		
		var randomNumber = Math.random() * ((max+1) - min) + min;
		return (Math.floor(randomNumber) == 1);
	}

	function refreshListeSlots()
	{
		var listePoints = new Array(0);
		for (i = 0; i < listeDesMembres.length; i++)
		{
			listePoints.push(new itemClassement(listeDesMembresAvecDetail[i].id, listeDesMembresAvecDetail[i].points)); 
		}
		
		io.sockets.emit('slotsClassement', listePoints);
	}
	
	function refreshListeSlotsInitial()
	{
		var listeSlots = new Array(0);
		for (i = 0; i < listeDesMembres.length; i++)
		{
			listeSlots.push(listeDesMembresAvecDetail[i]); 
		}
			
		io.sockets.emit('slots', listeSlots);	
	}
