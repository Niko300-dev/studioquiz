const process = require("process");
const toml = require("toml");
const findUp = require("find-up");
const fs = require("fs");


function getLocalConfigSync() {
  const cwd = process.cwd();
  const configPath = findUp.sync("directquiz.toml", {cwd});
  if (typeof configPath !== "string") {
    throw new Error("Config not found: make sure you have a `directquiz.toml` file");
  }
  const configToml = fs.readFileSync(configPath, {encoding: "utf-8"});
  return parseConfig(configToml);
}

function parseConfig(input) {
  const raw = toml.parse(input);
  // TODO: Check the config is valid
  return raw;
}

module.exports = {getLocalConfigSync, parseConfig};
