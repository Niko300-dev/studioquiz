<?php
header('Content-type: text/html; charset=UTF-8');
include('config.php');
	
	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

		$query = "CALL DQ_GetNotifications();";
		$result = $mysqli->query($query);

		/* Tableau associatif */
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			echo $row['UUID'].'|'.str_replace('|','',str_replace('#','',$row['DisplayName'])).'#';	
		}

		/* Libération des résultats */
		$result->free();

		/* Fermeture de la connexion */
		$mysqli->close();
	
	
	$mysqli = null;
	
?>