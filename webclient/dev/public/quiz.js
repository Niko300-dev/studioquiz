var socket;
var pseudoClient = "UNKNOWN";
var audioBonneReponse = new Audio('https://directquiz.niko.ovh/dev/bip_bonne_reponse.mp3');
var audioLettre = new Audio('https://directquiz.niko.ovh/dev/bip_lettre.mp3');
var audioTic = new Audio('https://directquiz.niko.ovh/dev/tic.mp3');
var audioTac = new Audio('https://directquiz.niko.ovh/dev/tac.mp3');
var audioFin  = new Audio('https://directquiz.niko.ovh/dev/elapsed.mp3');
var audioFlood  = new Audio('https://directquiz.niko.ovh/dev/bip_flood.mp3');

var audioMsg  = new Audio('https://directquiz.niko.ovh/dev/bip_msg.mp3');

var audioApplause  = new Array(0);

var currentApplause = -1;

var audioConnected  = new Audio('https://directquiz.niko.ovh/dev/connected.mp3');
var audioDisconnected  = new Audio('https://directquiz.niko.ovh/dev/disconnected.mp3');

var audioBlabla  = new Audio('https://directquiz.niko.ovh/dev/blabla.mp3');
const regexHTML = /(<([^>]+)>)/ig;

var actualChar = 0;
var currentText = "";

var currentQuestionOrTimeOut = "";
var intervaleChangephrase = null;
var annonceSpeaked = "";
var socket = null;

/*
phrases5secondesRestantes = new Array(0);

phrases5secondesRestantes.push("Il ne reste plus que 5 secondes...");
phrases5secondesRestantes.push("Attention, seulement 5 secondes restantes !");
phrases5secondesRestantes.push("Depechez-vous, le temps est bientôt écoulé !");
phrases5secondesRestantes.push("5 secondes et le temps sera écoulé...");
phrases5secondesRestantes.push("Encore 5 secondes.");
phrases5secondesRestantes.push("Prudence, plus que 5 secondes !");
phrases5secondesRestantes.push("Plus que 5 secondes au chrono.");
phrases5secondesRestantes.push("Le temps est limité, 5 secondes restantes !");
phrases5secondesRestantes.push("Prudence, le temps restant n'est plus que de 5 secondes !");
phrases5secondesRestantes.push("Faites vite car il ne reste que 5 secondes !");
phrases5secondesRestantes.push("Le chrono s'arrète dans 5 secondes !");
phrases5secondesRestantes.push("5 secondes !");

function phrase5secondesAuHasard()
{
	
	return phrases5secondesRestantes[getRandomInt(0,phrases5secondesRestantes.length)];
	
}
*/



for (i=0;i<24;i++)
{
	audioApplause.push(new Audio('https://directquiz.niko.ovh/dev/applause'+(i+1)+'.mp3'))
}

function nextApplause()
{	
	if (currentApplause == 23) currentApplause = 0;
	else currentApplause++;
	
	return currentApplause;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

String.prototype.normalizeAcc = function(){
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
     
    return str;
}

function connectToServer(pseudonyme, iduser, salon)
{
	pseudonyme = pseudonyme.replace(/ /g,"");
	
	if (salon == 1)
		socket = io.connect('www.directquiz.org:3000', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b7", transports: ["websocket"]});
	else if (salon == 2)
		socket = io.connect('www.directquiz.org:3001', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b8", transports: ["websocket"]});
	else if (salon == 3)
		socket = io.connect('www.directquiz.org:3002', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b9", transports: ["websocket"]});
	else if (salon == 4)
		socket = io.connect('www.directquiz.org:3003', {query: "token=3598bde5-abe2-4703-a481-384c5276f2ba", transports: ["websocket"]});
	else if (salon == 5)
		socket = io.connect('www.directquiz.org:3004', {query: "token=3598bde5-abe2-4703-a481-384c5276f2bb", transports: ["websocket"]});
	else if (salon == 99)	
		socket = io.connect('www.directquiz.org:3050', {query: "token=3598bde5-abe2-4703-a481-384c5276f2bc", transports: ["websocket"]});
	
	//var socket = io('https://nodejs.niko.ovh/', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b7"});

	//socket = io.connect('https://nodejs.niko.ovh/', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b7", transport : ['websocket']});	
	//else if (salon == 2)
	//	socket = io.connect('https://nodejs2.niko.ovh/', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b7"});
	//else if (salon == 3)
	//	socket = io.connect('directquiz.kozow.com:8080', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b7"});
		
	socket.on('message', newMessage);
	socket.on('annonce_vocale', newAnnonceVocale);
	socket.on('annonce', newAnnonce);
	socket.on('annonce_user', newAnnonceUser);
	socket.on('slots', newListeSlots);
	// socket.on('checkDisconnect', checkDisconnect);		
	socket.on('ack', acknowledge);
	socket.on('refreshTopMessage', refreshTopMessage);
	socket.on('chrono', updateChrono);
	socket.on('annonce_flood_user', floodGet);
	
	socket.on('applaudissement_send', applaudissement_send);
	socket.on('applaudissement_get', applaudissement_get);



			
	socket.on('kick', kick);		
			
	var dataMember = {
		    id: iduser,
			pseudo: pseudonyme,
				};			
	
	socket.emit('addMember',dataMember);

	pseudoClient = pseudonyme;
}

function kick(data)
{
	if (IDConnected == data.id)
	{
		window.location = "https://www.directquiz.org/index.php?type=kick";
	}
}

function displayTexteProgressif() {
		actualChar = 0;
	    intervaleChangephrase = setInterval('changeMessage()', 90);
}

function changeMessage() {
	     if (actualChar < currentQuestionOrTimeOut.length) {
	         currentText += currentQuestionOrTimeOut.charAt(actualChar);	      
	         actualChar++;
			 if (!isMuted) audioLettre.play();
	     }
	     $('#annonceQuestion').text(currentText);
		 
		 if (actualChar == currentQuestionOrTimeOut.length)
		 {
			 clearInterval(intervaleChangephrase);			 
		 }
}
	 
	 
function write(data)
{
	
	newMessageClient(data);
	
	if (data.message.indexOf("GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]") == -1)
	{	
		if (data.message.indexOf("/gif") == - 1 && data.message != "/mute"  && data.message != "/unmute" && data.message != "/cls" && data.message.indexOf("/setvoice") == - 1) socket.emit('message',data);	
	}	
	
	$("#msgToSend").val("");
	//console.log(data.msg);
	
}

function acknowledge(data)
{
	//alert (data.msg);
	
	if (data.msg == "ERROR_TOMANYUSER")
	{
		alert("Le salon est plein : 8 personnes maximum !")
	}
	else if (data.msg == "ERROR_ALREADYEXIST")
	{
		alert("Ce pseudo est déjà utilisé !")
	}
	else if (data.msg == "OK")
	{
		$("#currentConnected").text(pseudoClient);
		$("#connectFrame").hide();
		$("#sender").show();
	}
	
}

function refreshTopMessage(data)
{
	if (data.nickname == pseudoClient) $("#annonceQuestion").html(data.msg);
}

function floodGet(data)
{
	if (data.user == pseudoClient)
	{
		$("#chatContent").append("<div class='annonce"+data.type+"'>" + data.annonce + "</div>");
		$("#chatContent").scrollTop(99999);
		audioFlood.play();
	}	
}

function updateChrono(data)
{
	// data.restant
	// data.total
				if (parseInt(data.restant,10) < 40)
				{
					if (parseInt(data.restant,10) == 0)
					{
						audioFin.play();
					}
					else
					{
						if (!isMuted)
						{
							if (parseInt(data.restant,10)%2 == 0) audioTic.play();
							else audioTac.play();
							
							// if (parseInt(data.restant,10) == 5) presentateurParle(phrase5secondesAuHasard());
						}
						
					}
				}
				
	$("#chrono").attr("src","https://directquiz.niko.ovh/dev/img/C"+data.restant+".png");
}

function nettoyerMessageDeToutesSesInsultes(message)
{
	insultes = new Array(0);
	
 insultes.push("CUL");
 insultes.push("BITE");
 insultes.push("CHIER");
 insultes.push("PUTAIN");
 insultes.push("MERDE"); 
 insultes.push("MERDEUX");
 insultes.push("CONNARD");
 insultes.push("CONNASSE");
 insultes.push("CON");
 insultes.push("CONS");
 insultes.push("FILS DE PUTE");
 insultes.push("SALOPE");
 insultes.push("ENCULES");
 insultes.push("ENCULE");
 insultes.push("ENCULER");
 insultes.push("TA MERE LA PUTE");
 insultes.push("FILS DE CHIEN");
 insultes.push("FOUTRE");
 insultes.push("PUTE");
 insultes.push("PUTTE");
 insultes.push("SALOPE");
 insultes.push("ABRUTI");
 insultes.push("SAPERLIPOPETTE");
 insultes.push("MORVEUX");
 insultes.push("CRETIN");
 insultes.push("IMBECILE");
 insultes.push("FUCK");
 insultes.push("MOTHER FUCKER");
 insultes.push("COUILLES");
 insultes.push("COUILLE");
 insultes.push("ZIZI");
 insultes.push("ZEZETTE");
 insultes.push("KIKINE");

					var tempMessage = message;				
					var motsMessages = tempMessage.split(" ");
					var indexInsulte = -1;
					
					for (i = 0; i < motsMessages.length; i++)
					{
						indexInsulte = insultes.indexOf(motsMessages[i].normalizeAcc().replace(/[\u0300-\u036f]/g, "").toUpperCase());
						
						if (indexInsulte > -1)
						{		
							var censure = "";
							for (j = 0; j < insultes[indexInsulte].length; j++)
							{
								censure += "*";
							}
							motsMessages[i] = censure;	
						}
					}
					
					return motsMessages.join(" ");	
}

function newAnnonceUser(data)
{

	if (data.user == IDConnected)
	{

		if (data.type == 3)
		{
			audioBonneReponse.play();
		}	

		$("#chatContent").append("<div class='annonce"+data.type+"'>" + data.annonce + "</div>");
		$("#chatContent").scrollTop(99999);
	}
}

function applaudissement_send(data)
{
	if (data.user == IDConnected)
	{
		
		$("#chatContent").append("<div class='annonce"+data.type+"'>" + data.annonce + "</div>");
		$("#chatContent").scrollTop(99999);
		
		audioApplause[nextApplause()].play();
		
		$("#listeMembre").addClass('shake-slow');
		
		setTimeout(function(){
			
			$("#listeMembre").removeClass('shake-slow');
			
		},3900);
	}
}

function applaudissement_get(data)
{
	$("#chatContent").append("<div class='annonce"+data.type+"'>" + data.annonce + "</div>");
	$("#chatContent").scrollTop(99999);
	
		audioApplause[nextApplause()].play();
	
		$("#listeMembre").addClass('shake-slow');
		
		setTimeout(function(){
			
			$("#listeMembre").removeClass('shake-slow');
			
		},3900);
}


function newAnnonceVocale(data)
{
	
	duree = data.secondes;

		setTimeout(function(){
			animParler();
			setTimeout(function(){
			animStop();
		},duree*1000);
		},1850);
	
	   
											

	
	if (!isMuted)
	{
		setTimeout(function(){
			
			var annonceAudio = new Audio('https://directquiz.org/speech/' + data.annonce);
			annonceAudio.play();
			
		},1500);
	}
	
}

// BALISE VIDEO

function animParler() {
    // Lit la vidéo
    lecteur.play();
}

function animStop() {
    // Arrête la vidéo
    // On met en pause
    lecteur.pause();
    // Et on se remet au départ
    lecteur.currentTime = 0;
}

// -- BALISE VIDEO



function newAnnonce(data)
{
	var annonce = "";
	
	if (data.type != 111)
		annonce = decodeURIComponent(data.annonce.replace(regexHTML,""));
	else
		annonce = data.annonce;
	
	annonce = annonce.replace("お茶が好き","Oka ga suki");
	
	
	var dureeMS = 4000; //(data.annonce.length * 100) - 850;
	
	if (data.type == 4)
	{
		currentText = "";
		currentQuestionOrTimeOut = annonce;
		$("#annonceQuestion").removeClass();
		
		if (annonce.indexOf("La bonne réponse était ") > -1)
		{
			$("#annonceQuestion").addClass("annonceReponse");
		}
		else
		{
			$("#annonceQuestion").addClass("annonceClassique");
		}

		displayTexteProgressif();			
		/*
		$("#avatarAnimateur").html('<video id="animation-presentateur" muted preload="auto" autoplay="true" loop width="288" height="216">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.webm"'+
											'type="video/webm">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4"'+
											'type="video/mp4">'+
											'</video>');
											*/
											//animParler();
		
		/*
		if (annonce.length <= 10)
		{
			dureeMS = 900;
		}
		*/
		
		if (annonce.indexOf("La bonne réponse était") > -1)
		{
			annonceSpeaked = annonce.toLowerCase();
		}
		else
		{
			annonceSpeaked = annonce;
		}
		
		if (!isMuted) speakInSex(annonceSpeaked.replace(/_/g," "), sexeVoix);
		
		
	}
	else if (data.type == 3)
	{
		audioBonneReponse.play();
	}
	else if (data.type == 5)
	{
		if (annonce.lastIndexOf(pseudoClient+" ") > -1)
		{
			location.reload();
		}
	}
	else if (data.type == 99)
	{
		
		if (annonce.indexOf("La bonne réponse était") > -1)
		{
			annonceSpeaked = annonce.toLowerCase();
		}
		else
		{
			annonceSpeaked = annonce;
		}
		
		if (!isMuted) speakInSex(annonceSpeaked.replace(/_/g," "), sexeVoix);
		/*
		$("#avatarAnimateur").html('<video id="animation-presentateur" muted preload="auto" autoplay="true" loop width="288" height="216">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.webm"'+
											'type="video/webm">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4"'+
											'type="video/mp4">'+
											'</video>');
											*/
											
											//animParler();
		
	}
	else if (data.type == 98)
	{
		$("#chatContent").append("<div class='annonce"+data.type+"'> <img src='images/megaphone.png' alt='megaphone' /> " + annonce + "</div>");
		$("#chatContent").scrollTop(99999);
		
		speakInSex("","Male");
		
		//animParler();
		
	}
	
	if (data.type != 98)
	{
		$("#chatContent").append("<div class='annonce"+data.type+"'>" + annonce + "</div>");
		$("#chatContent").scrollTop(99999);
		
		if (data.type == 1) // connexion
		{
			audioConnected.play()
		}
		else if (data.type == 2) // déconnexion
		{
			audioDisconnected.play()
		}
	}
}

function speakInSex(texte,sexe)
{
	//if (!isMuted) audioBlabla.play();
}

function presentateurParle(annonceParlee)
{		
		$("#chatContent").append("<div class='annonce98'> <img src='images/megaphone.png' alt='megaphone' /> " + annonceParlee + "</div>");
		$("#chatContent").scrollTop(99999);
}

function newMessage(data)
{
	
	audioMsg.play();
	
	var message = "";
	
	if (data.message.indexOf('giphy.com/media/') > -1)
	{
		 message = data.message;
	}
	else
	{
		 message = data.message.replace(regexHTML,"");
	}	
	
	$("#chatContent").append("<div class='message'> <img src='images/avatar/"+data.id+".jpg?"+mili()+"' class='microavatar' /> <strong>" + (data.pseudo.split("#")[1] == "1"?data.pseudo.replace("#1",""):data.pseudo) + "</strong> : " + message + "</div>");
	$("#chatContent").scrollTop(99999);	
}

function mili()
{
	
	var min = 500;
	var max = 1024;
		
 return Math.floor(Math.random() * (max - min + 1)) + min;
}

function newMessageClient(data)
{
	
		if (data.message.indexOf("GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]") > -1)
		{
			var GIF = data.message.split(/\|/);
			$("#chatContent").append("<div class='message'> <img src='images/avatar/"+data.id+".jpg?"+mili()+"' class='microavatar' /> <strong>" + data.pseudo + "</strong> : " + "<img src='"+GIF[1]+"' />" + "</div>");
			
			var dataGIF = {
				id: IDConnected,
				pseudo: pseudoConnected,
				message: "<img src='"+GIF[1]+"' />"
			};

			socket.emit('message',dataGIF);
		}
		else
		{	
				var message = nettoyerMessageDeToutesSesInsultes(data.message.replace(regexHTML,""));	
				
				if (message == "/cls") $("#chatContent").html("");
				
				if (message.indexOf("/setvoice") == 0)
				{
					if (message.indexOf(" femme") > -1)
					{
						//setFemme();
						//if (!isMuted) presentateurParle("Bonjour ! Je vous parle en voix de femme !");
					}
					else if (message.indexOf(" homme") > -1)
					{
						//setHomme();
						//if (!isMuted) presentateurParle("Bonjour ! Je vous parle en voix d'homme !");
					}
				}
				else if (message.indexOf("/mute") == 0)
				{
					mute();
					presentateurParle("Silence activé...");
				}
				else if (message.indexOf("/unmute") == 0)
				{
					unmute();
					presentateurParle("Silence désactivé...");
				}
				else if (message.indexOf("/gif") == 0)
				{
					var query = message.replace("/gif",""); 
					if (query != "") searchGIF(query);	
				}
				else
				{
					if (message.indexOf("/start") == -1)
					{
						$("#chatContent").append("<div class='message'><img src='images/avatar/"+IDConnected+".jpg?"+mili()+"' class='microavatar' /> <strong>" + $(".slot[data-user-id="+IDConnected+"]").children(".pseudonyme").eq(0).text() + "</strong> : " + message + "</div>");
						audioMsg.play();
					}
					
				}
		}
	
	$("#chatContent").scrollTop(99999);	
}

function newListeSlots(data)
{
	$(".slot").each(function(){
		
		$(this).html("&nbsp;");
		
	});
	
	for (i = 0; i < data.length && i < 8; i++)
	{
		$("#slot"+(i+1)).html("<img src='images/avatar/0.jpg' class='avatar'/><img src='images/avatar/level_1.png' uuid='"+data[i].id+"' class='avatar-level' /> <span class='pseudonyme'>" + (data[i].version == 1?data[i].name:data[i].name+"#"+data[i].version) + "</span> <span class='points'>" + data[i].points + " pts</span>");
		
		if (data[i].points == 0) $("#slot"+(i+1)).children(".points").eq(0).css("background-color","#c33f3f");
		else $("#slot"+(i+1)).children(".points").eq(0).css("background-color","#3c8845");
		
		$("#slot"+(i+1)).attr("data-user-id",data[i].id);

	}
	
	refreshAvatars();
}

/*
function checkDisconnect(data)
{
	var member = {
		pseudo : pseudoClient
	}
	
		socket.emit('alive',member);
}
*/