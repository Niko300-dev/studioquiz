<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/countmem.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/getBestScoreByUserName101.php");

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
/* enqueue script for parent theme stylesheeet */        
function childtheme_parent_styles() {
 
 // enqueue style
wp_enqueue_style( 'parent', get_template_directory_uri().'/style.css' ); 
wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'parent-style' ) ); 
}

function getAvatarToPutArray()
{
	$result = "";
	
	for ($i = 0; $i < getCountMembers(); $i++)
	{
		$result .= "tableauDesAvatars.push('".um_get_user_avatar_url($user_id = ($i+1), $size = '32' )."'); ";
	}
	
	return $result;
	
}

function child_footer_extra_content() {


global $current_user;
get_currentuserinfo();

 echo '	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
	<script src="../../../dev/js/speech.js"></script>
	<script	src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <script src="../../../dev/js/jquery.tightgrid.js"></script>
	
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		
	<script>

		var isMuted = false;
		var sexeVoix = "French Canadian Male";
	
		function mute()
		{
			isMuted = true;
		}
		
		function unmute()
		{
			isMuted = false;
		}
		
		function setHomme()
		{
			sexeVoix = "French Canadian Male";
			$("#avatarAnimateur").css("border","3px solid darkblue");
			
		}
		
		function setFemme()
		{
			sexeVoix = "French Canadian Female"
			$("#avatarAnimateur").css("border","3px solid pink");
		}
		
		function getRecordByName(currentUserName)
		{
			$.post("../../../dev/ajax/getBestScoreByUserName101.php",{name:currentUserName}).done(function(data){
			
				return data;
			
			});
			
		}
	
		function setRecordsMembres()
		{

		}
	
		var tableauDesAvatars = new Array(0);
		var pseudoConnected = "";	
		
		$(function(){
				//'.$current_user->user_id.'///
				'.getAvatarToPutArray().'
				
				$(".um-members-pagi-dropdown").change(function(){
					
					setRecordsMembres();
					
				});
				
				
				
				if (!(window.location.href.lastIndexOf("jouer") == -1 || "'.$current_user->user_login.'" == ""))
				{
					
					$("#connected-jouer-btn").html("<input type=\'button\' style=\'width:400px; height:100px; font-size: 1.9em; border-radius:10px 10px 10px 10px;\' value=\'Jouer sur Salon #1\' data-user=\''.$current_user->user_login.'\' data-user-id=\''.wp_get_current_user()->ID.'\' id=\'kd8e6g52f6d85e2d6f2\' />");
					
					$("#kd8e6g52f6d85e2d6f2").click(function(){

						f5d6e85g6f12d56f2e41e($("#kd8e6g52f6d85e2d6f2").attr("data-user"), $("#kd8e6g52f6d85e2d6f2").attr("data-user-id"), 1);
						$("#salon_name").text("salon #1");
					});
					
					// SALON 2
					
					$("#connected-jouer-btn-salon2").html("<input type=\'button\' style=\'width:400px; height:100px; font-size: 1.9em; border-radius:10px 10px 10px 10px;\' value=\'Jouer sur Salon #2 (Beta !) \' data-user=\''.$current_user->user_login.'\' data-user-id=\''.wp_get_current_user()->ID.'\' id=\'kd8e6g52f6d85e2d6f3\' />");
					
					$("#kd8e6g52f6d85e2d6f3").click(function(){

						f5d6e85g6f12d56f2e41e($("#kd8e6g52f6d85e2d6f3").attr("data-user"), $("#kd8e6g52f6d85e2d6f3").attr("data-user-id"), 2);
						$("#salon_name").text("salon #2 | Beta dev");
					});

					// SALON 3
					
					$("#connected-jouer-btn-salon3").html("<input type=\'button\' style=\'width:400px; height:100px; font-size: 1.9em; border-radius:10px 10px 10px 10px;\' value=\'Jouer sur Salon #3 (Alpha !) \' data-user=\''.$current_user->user_login.'\' data-user-id=\''.wp_get_current_user()->ID.'\' id=\'kd8e6g52f6d85e2d6f4\' />");
					
					$("#kd8e6g52f6d85e2d6f4").click(function(){

						f5d6e85g6f12d56f2e41e($("#kd8e6g52f6d85e2d6f4").attr("data-user"), $("#kd8e6g52f6d85e2d6f4").attr("data-user-id"), 3);
						$("#salon_name").text("salon #3 | Alpha dev");
					});

					
							var img = null;
					
							for (i = 0; i <= 40; i++)
							{
								
								img = new Image();
								img.src="../../../dev/img/C"+i+".png";
								
							}	
					
				}
				else if (!(window.location.href.lastIndexOf("members") == -1))
				{
					console.log("in member list");
					setRecordsMembres();
					$("#calendar").remove();
				}
				else
				{
					$("#calendar").remove();
				}
			
			//displayTexteProgressif();
		
		
		
		});
		
		function searchGIF(terme)
		{

				$("#resultat").show();
	
				var api = "https://api.giphy.com/v1/gifs/search?";
				var apiKey = "&api_key=4O10kcOpby6A1I8CB6aOtXrWedK70uty";
				var query = "&q="+terme;
				var limit = "&limit=24";

				var url = api + apiKey + query + limit;

				$.get(url).done(function(data){

					//result = JSON.parse(data);
					
					var result = "";
					
					for (i = 0; i < data.data.length ;i++)
					{
							result += "<div class=\'item\'><img class=\'item-gif\' src=\'"+data.data[i].images.fixed_height_small.url+"\' /></div>";
					}
					$("#resultat").html("");
					$("#resultat").html(result);
					
							//$(".pictures").tjGallery("clear");
							
							/*
							$(".pictures").tjGallery({
								selector: ".item",
								margin: 10
							});
							*/
					
					$(".item-gif").click(function(){

					var data = {
						pseudo: pseudoConnected,
						message: "GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]|" + $(this).attr("src")
							};
							
						write(data);
						
						$("#resultat").hide();
						
					});
					
					$("#resultat").click(function(){
						$("#resultat").html("");
						$("#resultat").hide();
						
					});
					
					

				
				});

		}
		
		function getAvatarsArray()
		{
			
			return tableauDesAvatars;
			
		}
		
		function refreshAvatars()
		{
			$(".slot").each(function(){
				
				$(this).children(".avatar").eq(0).attr("src",tableauDesAvatars[parseInt($(this).attr("data-user-id"), 10)-1]);
				
			});
		}
		
		function messageEnter(e) {

			if (e.keyCode == 13 && $("#msgToSend").val().trim().replace(/ /g,"") != "") {

					var data = {
						pseudo: pseudoConnected,
						message: $("#msgToSend").val()
							};
					write(data);

					return false;
		  }
				 
		}
		
		function f5d6e85g6f12d56f2e41e(pseudo, iduser, sa)
		{
					
					if (pseudo.trim() != "")
					{
						connectToServer(pseudo.trim(), iduser, sa);
						pseudoConnected = pseudo.trim();
					}
					else
					{
						alert("Veuillez vous connecter !")
					}
		}-
	
	</script>	
<div id="my-toast-location" style="position: absolute; top; 10px; left: 10px;"></div>
<div id="sender">

<div class="container-fluid" id="plateau">

<p>&gt; Connecté en tant que <span id="currentConnected"></span> sur <strong><span id="salon_name"></span></strong></p>


<div class="row">
<div class="col-12 col-sm-12 col-md-11 col-lg-5 col-xl-5">
	
	<div class="row">

		<div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6">
			<img src="../../../dev/img/animateur.jpg" id="avatarAnimateur" alt="Animateur TV" />
			
			<br/>
			<br/>


		</div>

		<div style="padding: 50px;" class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6">
			<img id="chrono" src="../../../dev/img/C40.png" />
		</div>
		
	</div>
	
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<table id="listeMembre">

					<tr><td class="slot" id="slot1">&nbsp;</td><td class="slot" id="slot5">&nbsp;</td></tr>
					<tr><td class="slot" id="slot2">&nbsp;</td><td class="slot" id="slot6">&nbsp;</td></tr>
					<tr><td class="slot" id="slot3">&nbsp;</td><td class="slot" id="slot7">&nbsp;</td></tr>
					<tr><td class="slot" id="slot4">&nbsp;</td><td class="slot" id="slot8">&nbsp;</td></tr>

				</table>
			</div>
		</div>

</div>

<div class="col-12 col-sm-12 col-md-11 col-lg-5 col-xl-5">

<div id="annonceQuestion">
&nbsp;
</div>
<div id="chatContent">
</div>	
<input type="text" id="msgToSend" style="border-radius: 0px 0px 10px 10px;
			border : 3px solid darkblue;
			border-top-width : 0;
			background-color : black;
			color: white;
			height : 25px;
			width : 100%;
			font-size : 1.1em;
			padding-left: 25px;" onkeypress="return messageEnter(event)" placeholder="Ecrivez votre message ici" /> <input type="button" style="display:none;" value="Envoyer !" id="btnToSend" />	
<p style="font-size: 0.65em;">
<img class="icone-info" src="../../../dev/img/info.png" /> Pour lancer une partie : <strong>/start</strong><br/>
<img class="icone-info" src="../../../dev/img/info.png" /> Pour lacher un GIF animé, lancer une recherche : <strong>/gif</strong> suivi d\'un espace, suivi de votre recherche puis cliquez sur le GIF à envoyer !<br/>
<img class="icone-info" src="../../../dev/img/info.png" /> Pour désactiver/réactiver la voix du présentateur et des sons pour vous :  <strong>/mute</strong> | <strong>/unmute</strong> <br/>
<img class="icone-info" src="../../../dev/img/info.png" /> Pour entendre une voix de femme / d\'homme pour vous : <strong>/setvoice femme</strong> | <strong>/setvoice homme</strong>

</p>

<div id="resultat" class="pictures">
</div>

</div>

</div>




</div> <!-- plateau -->
</div> <!-- sender -->
<div id="connectFrame">
<center>
<div id="calendar" style="width:880px; height:470px;">

	<iframe id="framecalendar" style="overflow:hidden; border-width:0;" src="https://niko.ovh/calendate/example/" width="880px" height="470px"></iframe>

</div>
</center>

<center><div id="connected-jouer-btn"></div><!--[ En maintenance - De retour bientôt :) ]--></center><br/>
	<br/>

<div style="visibility:hidden; max-height:0, max-width:0;">


</div>

</div>
<br/><br/>

<script src="https://directquiz.niko.ovh/dev/public/quiz.js"></script>';
//<br/><img class="avatar" src=\''.um_get_user_avatar_url($user_id = wp_get_current_user()->ID, $size = '32' ).'\' />

//echo '<center> EN MAINTENANCE </center>';
}

add_action( 'wp_enqueue_scripts', 'childtheme_parent_styles');
add_action( 'parent_footer', 'child_footer_extra_content', 20 );
?>